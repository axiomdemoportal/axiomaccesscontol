package com.mollatech.internal.access.handler.db;

import java.util.Iterator;
import java.util.Set;
import org.hibernate.Session;
import org.hibernate.cfg.AnnotationConfiguration;

public class SessionFactoryUtil extends SessionFactory {
 private static org.hibernate.SessionFactory sessionFactoryapprovalsettings;
    public static final int approvalsettings = 44;
    private org.hibernate.Session m_session = null;
    private int m_iType = -1;

    public SessionFactoryUtil(int iType) {
        m_iType = iType;
        try {
            AnnotationConfiguration annotationConfig = new AnnotationConfiguration();
            Set set = stat_Properties.keySet();
            Iterator itr = set.iterator();
            while (itr.hasNext()) {
                String key = (String) itr.next();
                String value = (String) stat_Properties.get(key);
                annotationConfig.setProperty(key, value);
            }
         if (iType == approvalsettings) {
                annotationConfig.addResource("com/mollatech/internal/access/handler/db/Approvalsettings.hbm.xml");
                annotationConfig.addPackage("com/mollatech/internal/access/handler/db/");
                annotationConfig.addAnnotatedClass(Approvalsettings.class);
                if (sessionFactoryapprovalsettings == null) {
                    sessionFactoryapprovalsettings = annotationConfig.buildSessionFactory();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Session openSession() {
       if (m_iType == approvalsettings) {
           m_session = sessionFactoryapprovalsettings.openSession();
       } 
        return m_session;
    }
    public void close() {
//        if (sessionFactory != null) {
//            sessionFactory.close();
//        }
//        sessionFactory = null;
    }
}
