package com.mollatech.internal.access.handler.db;
import com.mollatech.axiom.connector.user.SourceDetails;
import com.mollatech.internal.access.handler.AccessControlHandler;
import java.util.Properties;

public class SessionFactory {

    public static Properties stat_Properties = null;
    static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SessionFactory.class.getName());
    
    
    public SessionFactory() {
        try {
            if (stat_Properties == null) {
                SourceDetails srcDetailsObj = AccessControlHandler.config.getSourceDetails();
                Properties p = new Properties();
                String database = srcDetailsObj.getDatabaseName();
                if (srcDetailsObj.getDatabaseType().equalsIgnoreCase("mysql")) {
                    p.put("hibernate.connection.driver_class", "com.microsoft.sqlserver.jdbc.SQLServerDriver");
                    p.put("hibernate.dialect", "org.hibernate.dialect.SQLServerDialect");
                } else {
                    p.put("hibernate.connection.driver_class", "oracle.jdbc.OracleDriver");
                    p.put("hibernate.dialect", "org.hibernate.dialect.OracleDialect");
                }
                p.put("hibernate.connection.datasource", "java:/comp/env/jdbc/mainDB");
                p.put("hibernate.show_sql", "true");
                p.put("hibernate.transaction.factory_class", "org.hibernate.transaction.JDBCTransactionFactory");
                p.put("hibernate.current_session_context_class", "thread");
                p.put("hibernate.show_sql", "false");
                p.put("log4j.logger.org.hibernate", "server");
                stat_Properties = p;
            }
        } catch (Exception e) {
            log.error(e);
        }
    }


//    public SessionFactory() {
//
//        try {
//            if (stat_Properties == null) {                
//                SourceDetails srcDetailsObj = AxiomUser.config.getSourceDetails();
//                Properties p = new Properties();
//                                
//                p.put("hibernate.connection.url", "jdbc:mysql://"
//                        + srcDetailsObj.getHostname() + ":"
//                        + srcDetailsObj.getPortno() + "/"
//                        + srcDetailsObj.getDatabaseName()
//                        + "?autoReconnect=true");
//                p.put("hibernate.connection.username", 
//                        srcDetailsObj.getUserName());
//
//                String passwd = srcDetailsObj.getPassword();
//                
//                p.put("hibernate.connection.password",passwd);
//                // }
//                p.put("hibernate.connection.driver_class", srcDetailsObj.getDatabaseType());
//                p.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
//                p.put("hibernate.transaction.factory_class", "org.hibernate.transaction.JDBCTransactionFactory");
//                p.put("hibernate.current_session_context_class", "thread");
//                p.put("hibernate.show_sql", "false");
//                p.put("log4j.logger.org.hibernate", "error");
//                stat_Properties = p;
//            }
//
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
//    }
}
