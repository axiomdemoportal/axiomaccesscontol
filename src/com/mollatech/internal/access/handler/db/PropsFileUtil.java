package com.mollatech.internal.access.handler.db;

import java.io.*;
import java.util.*;

public class PropsFileUtil {

    public PropsFileUtil() {
    }

    public Properties properties = new Properties();

    public boolean LoadFile(String FilePath) {
        try {
            FileInputStream f = new FileInputStream(FilePath);
            properties.load(f);
            f.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public String GetProperty(String Key) {
        try {
            String value = (String) properties.getProperty(Key);
            return value;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean AddProperties(HashMap map, String fileName) {
        try {
            Set set = map.keySet();
            Iterator itr = set.iterator();
            while (itr.hasNext()) {
                String key = (String) itr.next();
                String value = (String) map.get(key);
                properties.setProperty(key, value);
            }
            FileOutputStream fop = new FileOutputStream(fileName);
            properties.store(fop, "DO NOT TAMPER OR DELETE OR EDIT THIS FILE.");
            fop.close();
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    public boolean ReplaceProperties(HashMap map, String fileName) {
        try {
            Set set = map.keySet();
            Iterator itr = set.iterator();
            while (itr.hasNext()) {
                String key = (String) itr.next();
                String value = (String) map.get(key);
                properties.setProperty(key, value);
            }
            FileOutputStream fop = new FileOutputStream(fileName);
            properties.store(fop, "DO NOT TAMPER OR DELETE OR EDIT THIS FILE.");
            fop.close();
            return true;

        } catch (Exception e) {
            return false;
        }

    }

    public boolean AddProperties(Properties map, String fileName) {
        try {
            Set set = map.keySet();
            Iterator itr = set.iterator();
            while (itr.hasNext()) {
                String key = (String) itr.next();
                String value = (String) map.get(key);
                properties.setProperty(key, value);
            }
            FileOutputStream fop = new FileOutputStream(fileName);
            properties.store(fop, "DO NOT TAMPER OR DELETE OR EDIT THIS FILE.");
            fop.close();
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    public static byte[] serializeToObject(Object objectToSerialize) {
        ObjectOutputStream oos = null;
        byte[] data = null;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(bos);
            oos.writeObject(objectToSerialize);
            oos.flush();
            oos.close();
            bos.close();
            data = bos.toByteArray();
            return data;
        } catch (IOException ex) {
            ex.printStackTrace();

        } finally {
            try {
                oos.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return data;
    }
}
