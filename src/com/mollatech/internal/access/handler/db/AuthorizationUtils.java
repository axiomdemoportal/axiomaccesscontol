/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.internal.access.handler.db;

import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.crypto.HASH;
import static com.mollatech.internal.access.handler.db.PropsFileUtil.serializeToObject;
import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author nilesh
 */
public class AuthorizationUtils {

    private SessionFactoryUtil m_su;
    private Session m_session;
    public static final int AUTORIZATION_PENDING_STATUS = 2;
    public static final int AUTORIZATION_APPROVE_STATUS = 1;
    public static final int AUTORIZATION_REJECT_STATUS = -2;
    public static final int AUTORIZATION_EXPIRE_STATUS = -4;

    public AuthorizationUtils(SessionFactoryUtil su, Session session) {
        m_su = su;
        m_session = session;
    }

    public int addSetting(String channelId, String operatorId, int status, Date expireDate, Object approval) {
        Approvalsettings app = new Approvalsettings();
        app.setStatus(status);
           UUID uuid = UUID.randomUUID();
        String hashedID= HASH.getStringFromSHA1(""+new Date().getTime()+Thread.currentThread().getId()+uuid.toString());
        byte[] settings = AxiomProtect.ProtectDataBytes(serializeToObject(approval));
        app.setApprovalSettingEntry(settings);
        app.setApprovalId(hashedID);
        app.setChannelId(channelId);
        app.setCreatedOn(new Date());
        app.setMarkerOperatorid(operatorId);
        if(expireDate != null){
        app.setExpireOn(expireDate);
        }else{
            app.setExpireOn(new Date());
        }
         app.setUpdatedOn(new Date());

        Transaction tx = null;
        try {
            tx = m_session.beginTransaction();
            m_session.save(app);
            tx.commit();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();

        }
        return -1;
      
    }

    public Approvalsettings[] getALLPendingRequest(String ChannelId, int status) {

        try {
            Criteria criteria = m_session.createCriteria(Approvalsettings.class);
            criteria.add(Restrictions.eq("channelId", ChannelId));
            criteria.add(Restrictions.eq("status", status));
            criteria.addOrder(Order.asc("createdOn"));

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                Approvalsettings[] clogs = new Approvalsettings[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    clogs[i] = (Approvalsettings) list.get(i);
                    //                clogs[i].setMessage(AxiomProtect.AccessData(clogs[i].getMessage()));
                }
                return clogs;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public Approvalsettings getApprovalSettingByApprovalId(String ChannelId, int approvalId) {

        try {
            Criteria criteria = m_session.createCriteria(Approvalsettings.class);
            criteria.add(Restrictions.eq("channelId", ChannelId));
            criteria.add(Restrictions.eq("approvalId", approvalId));
//            criteria.addOrder(Order.asc("createdOn"));

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
//                Approvalsettings[] clogs = new Approvalsettings[list.size()];
//                for (int i = 0; i < list.size(); i++) {
                Approvalsettings clogs = (Approvalsettings) list.get(0);
                //                clogs[i].setMessage(AxiomProtect.AccessData(clogs[i].getMessage()));
//                }
                return clogs;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public int removeAuthorizationRequest(String channelId, String approveroperatorId, int approvalId) {

        try {
            Criteria criteria = m_session.createCriteria(Approvalsettings.class);
            criteria.add(Restrictions.eq("channelId", channelId));
            criteria.add(Restrictions.eq("approvalId", approvalId));
            Approvalsettings tObj = null;

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -1;
            } else {
                tObj = (Approvalsettings) list.get(0);

                Transaction tx = null;
                try {
                    tObj.setStatus(AUTORIZATION_APPROVE_STATUS);
                    tObj.setApproverOperatorid(approveroperatorId);
                    tObj.setUpdatedOn(new Date());
                    tx = m_session.beginTransaction();
                    m_session.save(tObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -2;
    }

    public Approvalsettings[] getExpiredRequest(String ChannelId, Date start, int status) {

        try {
            Calendar current = Calendar.getInstance();
            current.setTime(start);

            Date startDate1 = current.getTime();
            Criteria criteria = m_session.createCriteria(Approvalsettings.class);
            criteria.add(Restrictions.lt("expireOn", startDate1));
            criteria.add(Restrictions.eq("channelId", ChannelId));
            criteria.add(Restrictions.eq("status", status));

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                Approvalsettings[] clogs = new Approvalsettings[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    clogs[i] = (Approvalsettings) list.get(i);
                }

                return clogs;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

//    public Approvalsettings[] getExpiredRequest(String ChannelId, Date start) {
//
//        try {
//            Calendar current = Calendar.getInstance();
//            current.setTime(start);
////                current.set(Calendar.AM_PM, Calendar.AM);
////                current.set(Calendar.HOUR, 24);
////                current.set(Calendar.MINUTE, 0);
////                current.set(Calendar.SECOND, 0);
////                current.set(Calendar.MILLISECOND, 0);
//            //current.add(Calendar.DATE, -1);//minus a date
//            Date startDate1 = current.getTime();
//            Criteria criteria = m_session.createCriteria(Approvalsettings.class);
////            criteria.add(Restrictions.lt("expireOn",start ) );
//            criteria.add(Restrictions.lt("expireOn", startDate1));
//            criteria.add(Restrictions.eq("channelId", ChannelId));
//
//            List list = criteria.list();
//            if (list == null || list.isEmpty() == true) {
//                return null;
//            } else {
//                Approvalsettings[] clogs = new Approvalsettings[list.size()];
//                for (int i = 0; i < list.size(); i++) {
//                    clogs[i] = (Approvalsettings) list.get(i);
//                }
//
//                return clogs;
//            }
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//        return null;
//    }
    public Approvalsettings[] getALLArchiveRequest(String ChannelId, String operatorId) {

        try {
            Criteria criteria = m_session.createCriteria(Approvalsettings.class);
            criteria.add(Restrictions.eq("channelId", ChannelId));
            criteria.add(Restrictions.eq("markerOperatorid", operatorId));
//            criteria.add(Restrictions.eq("status", status));
            Criterion completeCondition
                    = Restrictions.disjunction().add(Restrictions.like("status", AUTORIZATION_APPROVE_STATUS))
                    .add(Restrictions.like("status", AUTORIZATION_REJECT_STATUS))
                    .add(Restrictions.like("status",AUTORIZATION_EXPIRE_STATUS));
            criteria.add(completeCondition);
            criteria.addOrder(Order.asc("createdOn"));

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                Approvalsettings[] clogs = new Approvalsettings[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    clogs[i] = (Approvalsettings) list.get(i);
                    //                clogs[i].setMessage(AxiomProtect.AccessData(clogs[i].getMessage()));
                }
                return clogs;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public int rejectAuthorizationRequest(String channelId, String ApproveroperatorId, int approvalId) {

        try {
            Criteria criteria = m_session.createCriteria(Approvalsettings.class);
            criteria.add(Restrictions.eq("channelId", channelId));
            criteria.add(Restrictions.eq("approvalId", approvalId));
            Approvalsettings tObj = null;

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -1;
            } else {
                tObj = (Approvalsettings) list.get(0);
                Transaction tx = null;
                try {
                    tObj.setStatus(AUTORIZATION_REJECT_STATUS);
                    tObj.setApproverOperatorid(ApproveroperatorId);
                    tObj.setUpdatedOn(new Date());
                    tx = m_session.beginTransaction();
                    m_session.save(tObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -2;
    }

    public Approvalsettings[] getALLRequsterPendingRequest(String ChannelId, String operatorId, int status) {

        try {
            Criteria criteria = m_session.createCriteria(Approvalsettings.class);
            criteria.add(Restrictions.eq("channelId", ChannelId));
            criteria.add(Restrictions.eq("markerOperatorid", operatorId));
            criteria.add(Restrictions.eq("status", status));
            criteria.addOrder(Order.asc("createdOn"));

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                Approvalsettings[] clogs = new Approvalsettings[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    clogs[i] = (Approvalsettings) list.get(i);
                    //                clogs[i].setMessage(AxiomProtect.AccessData(clogs[i].getMessage()));
                }
                return clogs;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public int expiredAuthorizationRequest(String channelId, int approvalId) {

        try {
            Criteria criteria = m_session.createCriteria(Approvalsettings.class);
            criteria.add(Restrictions.eq("channelId", channelId));
            criteria.add(Restrictions.eq("approvalId", approvalId));
            Approvalsettings tObj = null;

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -1;
            } else {
                tObj = (Approvalsettings) list.get(0);
                Transaction tx = null;
                try {
                    tObj.setStatus(AUTORIZATION_EXPIRE_STATUS);
                    tObj.setUpdatedOn(new Date());
                    tx = m_session.beginTransaction();
                    m_session.save(tObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -2;
    }

    public Approvalsettings[] getALLArchiveRequestAuthorizer(String ChannelId, String operatorId) {

        try {
            Criteria criteria = m_session.createCriteria(Approvalsettings.class);
            criteria.add(Restrictions.eq("channelId", ChannelId));
            criteria.add(Restrictions.eq("approverOperatorid", operatorId));
//            criteria.add(Restrictions.eq("status", status));
            Criterion completeCondition
                    = Restrictions.disjunction().add(Restrictions.like("status",AUTORIZATION_APPROVE_STATUS))
                    .add(Restrictions.like("status",AUTORIZATION_REJECT_STATUS));
//                    .add(Restrictions.like("status", AuthorizationManagement.AUTORIZATION_EXPIRE_STATUS));
            criteria.add(completeCondition);
            criteria.addOrder(Order.asc("createdOn"));

            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                Approvalsettings[] clogs = new Approvalsettings[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    clogs[i] = (Approvalsettings) list.get(i);
                    //                clogs[i].setMessage(AxiomProtect.AccessData(clogs[i].getMessage()));
                }
                return clogs;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    
     public static Object deserializeFromObject(ByteArrayInputStream binaryObject) {
        ObjectInputStream ins;
        try {
            ins = new ObjectInputStream(binaryObject);
            Object object = (Object) ins.readObject();
            ins.close();
            return object;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
