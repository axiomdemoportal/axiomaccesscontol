/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.internal.access.handler;

import com.mollatech.axiom.connector.access.controller.AxiomAccesControlInterface;
import com.mollatech.axiom.connector.communication.AXIOMStatus;
import com.mollatech.axiom.connector.user.AxiomConfigProvider;
import com.mollatech.axiom.connector.access.controller.AccessMatrixSettings;
import com.mollatech.axiom.connector.access.controller.ApprovalSetting;
import com.mollatech.axiom.nucleus.crypto.AxiomProtect;
import com.mollatech.axiom.nucleus.crypto.PropsFileUtil;
import com.mollatech.internal.access.handler.db.Approvalsettings;
import com.mollatech.internal.access.handler.db.AuthorizationUtils;
import com.mollatech.internal.access.handler.db.SessionFactoryUtil;
import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import org.hibernate.Session;

/**
 *
 * @author nilesh
 */
public class AccessControlHandler implements AxiomAccesControlInterface {

    public static final int ASSIGN_OTP_TOKEN = 1;
    public static final int ASSIGN_HW_OTP_TOKEN = 2;
    public static final int CHANGE_OTP_TOKEN_TYPE = 3;
    public static final int CHANGE_OTP_TOKEN_STATUS = 4;
    public static final int ASSIGNTOKEN_SENDREGCODE = 110;
    public static final int ASSIGN_HW_PKI_TOKEN = 5;
    public static final int ASSIGN_CERTIFICATE = 6;
    public static final int SEND_CERTIFICATE = 7;
    public static final int RENEW_CERTIFICATE = 8;
    public static final int REVOKE_CERTIFICATE = 9;

    public static final int PKI_TOKEN_STATUS = 92;
    public static final int PKI_SEND_PFX = 93;
    public static final int PKI_SEND_PFX_PASSWORD = 94;
    public static final int PKI_SEND_REGCODE = 95;

    //channel
    public static final int CHANNEL_ADD = 10;
    public static final int CHANNEL_EDIT = 11;
    public static final int CHANNEL_STATUS = 12;
    public static final int CHANNEL_REMOTE_ACCESS = 13;

    //operators
    public static final int OPERATOR_ADD = 14;
    public static final int OPERATOR_EDIT = 15;
    public static final int OPERATOR_STATUS = 16;
    public static final int OPERATOR_ROLE = 17;
    public static final int OPERATOR_RESEND_PASSWORD = 18;
    public static final int OPERATOR_SEND_RANDOM_PASSWORD = 19;
    public static final int OPERATOR_GRANT_ACCESS = 84;
    public static final int OPERATOR_ACCESS_TYPE = 112;

    //units
    public static final int UNIT_ADD = 20;
    public static final int UNIT_EDIT = 21;
    public static final int UNIT_SHIFT_OPERATORS = 22;
    public static final int UNIT_ASSIGN_TOKEN = 23;
    public static final int UNIT_SHIFT_TOKEN_IN_UNIT = 24;
    public static final int UNIT_REMOVE = 25;
    public static final int UNIT_STATUS = 26;

    //Template
    public static final int TEMPLATE_ADD_MOBILE = 27;
    public static final int TEMPLATE_ADD_EMAIL = 28;
    public static final int TEMPLATE_EDIT_EMAIL = 29;
    public static final int TEMPLATE_EDIT_MOBILE = 90;
    public static final int TEMPLATE_STATUS = 30;
    public static final int TEMPLATE_DELETE = 31;

    //Users
    public static final int USER_ADD = 32;
    public static final int USER_EDIT = 33;
    public static final int USER_CHANGEDETAILS = -112;
    public static final int REPLACE_TOKEN = -113;
    public static final int USER_STATUS = 34;
    public static final int USER_DELETE = 35;
    public static final int USER_UNLOCK_PASSWORD = 36;
    public static final int USER_RESEND_PASSWORD = 37;
    public static final int USER_RESET_PASSWORD = 38;
    public static final int USER_SET_SEND_PASSWORD = 39;
    public static final int USER_ERASE_MOBILE_TRUST = 40;

    //Challenge Response
    public static final int CHALLENGE_RESPONSE_STATUS = 41;
    public static final int CHALLENGE_RESPONSE_DELETE = 42;
    public static final int CHALLENGE_RESPONSE_ADDQUESTION = 98;
    public static final int CHALLENGE_RESPONSE_EDITQUESTION = 99;

    //Trusted Device
    public static final int TRUSTED_DEVICE_STATUS = 43;
    public static final int TRUSTED_DEVICE_DELETE = 44;

    //Geo Tracking
    public static final int GEOTRACKING_EDIT = 45;

    //System Message
    public static final int SYSTEMESSAGE_ADD = 46;
    public static final int SYSTEMESSAGE_DELETE = 47;
    public static final int SYSTEMESSAGE_STATUS = 48;

    //Image 
    public static final int IMAGE_DELETE = 49;
    public static final int IMAGE_UNLOCK = 50;
    //SNMP 
    public static final int SNMP_ADD = 51;
    public static final int SNMP_EDIT = 52;
    public static final int SNMP_STATUS = 53;
    public static final int SNMP_DELETE = 54;
    //web watch 
    public static final int WEBWATCH_ADD = 107;
    public static final int WEBWATCH_CHANGESTATUS = 108;
    public static final int WEBWATCH_DELETE = 109;

    public static final int WEBRESOURCE_ADD = 110;
    public static final int WEBRESOURCE_CHANGESTATUS = 111;

    //gateways
    public static final int PREFERENCE_ONE = 1;   //primary
    public static final int PREFERENCE_TWO = 2;
    //sms
    public static final int SMS_ADD = 55;
    public static final int SMS_EDIT = 56;
    //ussd
    public static final int USSD_ADD = 57;
    public static final int EDIT_USSD = 58;
    //voice
    public static final int VOICE_ADD = 59;
    public static final int EDIT_VOICE = 60;
    //email
    public static final int EMAIL_ADD = 61;
    public static final int EDIT_EMAIL = 62;
    //Add billing manager
    public static final int BILLING_MANAGER_ADD = 63;
    public static final int EDIT_BILLING_MANAGER = 64;
    //Add Push setting
    public static final int ADD_ANDROID_PUSH_SETTING = 65;
    public static final int EDIT_ANDROID_PUSH_SETTING = 66;
    public static final int ADD_IPHONE_PUSH_SETTING = 67;
    public static final int EDIT_IPHONE_PUSH_SETTING = 68;
    //Add Push setting
    public static final int ADD_USER_SOURCE_SETTING = 69;
    public static final int EDIT_USER_SOURCE_SETTING = 70;
    //Add Password Policy setting
    public static final int ADD_PASSWORD_POLICY_SETTING = 71;
    public static final int EDIT_PASSWORD_POLICY_SETTING = 72;
    //Add Channel Profile setting
    public static final int ADD_CHANNLE_PROFILE_SETTING = 73;
    public static final int EDIT_CHANNLE_PROFILE_SETTING = 74;
    //Add Channel Profile setting
    public static final int ADD_TOKEN_SETTING = 75;
    public static final int EDIT_TOKEN_SETTING = 76;
    //Add Certificate setting
    public static final int ADD_CERTIFICATE_SETTING = 77;
    public static final int EDIT_CERTIFICATE_SETTING = 78;
    //Add Mobile Trust setting
    public static final int ADD_MOBILE_TRUST_SETTING = 79;
    public static final int EDIT_MOBILE_TRUST_SETTING = 80;
    //Add Radius setting
    public static final int ADD_RADIUS_SETTING = 81;
    public static final int EDIT_RADIUS_SETTING = 82;
    //Epin
    public static final int ADD_EPIN_SETTING = 83;

    //user group
    public static final int USERGROUP_ADD = 85;
    public static final int USERGROUP_STATUS = 86;
    public static final int USERGROUP_SHIFT_USERS = 87;
    public static final int USERGROUP_DELETE = 88;
    public static final int USERGROUP_EDIT = 89;

    //two way auth
    public static final int TWO_WAY_AUTH_STATUS = 91;
    public static final int TWO_WAY_AUTH_REGCODE = 96;
    //system specific
    public static final int SYSTEM_CHANGEPASSWORD = 97;
    public static final int SYSTEM_ADD_SCHEDULER = 100;
    public static final int SYSTEM_EDIT_SCHEDULER = 101;
    public static final int SYSTEM_STATUS_SCHEDULER = 102;
    public static final int SYSTEM_DELETE_SCHEDULER = 103;
    public static final int SYSTEM_AUDIT_INTEGRITY = 104;
    public static final int HW_OTP_UPLOAD = 105;
    public static final int OTP_TOKEN_REPLACE = 106;

//    public static final int OTP_TOKEN_REPLACE = 106;
    public static final String strASSIGN_OTP_TOKEN = "ASSIGN_OTP_TOKEN";
//    public static final int CHANGE_OTP_TOKEN_STATUS = 2;
//    public static final int ADD_USER = 5;
    public static final int CHANGE_USER_STATUS = 6;
    public static final int EDIT_USER_DETAILS = 7;
    public static final int DELETE_USER_DETAILS = 8;
    public static final int UNLOCK_USER_PASSWORD = 9;
    public static final int RESEND_USER_PASSWORD = 10;
    public static final int SET_RESEND_USER_PASSWORD = 11;
    public static final int RESET_USER_PASSWORD = 12;
    public static final int CHANGE_USERGROUP = 111;
//    public static final int ASSIGN_CERTIFICATE = 13;
//    public static final int SEND_CERTIFICATE = 14;
//    public static final int RENEW_CERTIFICATE = 15;
//    public static final int REVOKE_CERTIFICATE = 16;
//    public static final int ASSIGN_HW_PKI_TOKEN = 17;
    public static final int CHANGE_HW_PKI_TOKEN_STATUS = 18;
    public static final int SEND_PFX_FILE = 19;
    public static final int SEND_PFX_PASSWORD = 20;
    public static final int SEND_PKI_TOKEN_REG_CODE = 21;
    public static final int SEND_OTP_TOKEN_REG_CODE = 22;
    public static final int RETURN_AUTORIZATION_RESULT = 5;
    public static final int AUTORIZATION_PENDING_STATUS = 2;
    public static final int AUTORIZATION_APPROVE_STATUS = 1;
    public static final int AUTORIZATION_REJECT_STATUS = -2;
    public static final int AUTORIZATION_EXPIRE_STATUS = -4;
    public static final int REQUESTER = 1;
    public static final int AUTHORIZER = 2;

    public static final int MAKERCHECKER_DISABLE = 0;
    public static final int MAKERCHECKER_ENABLE = 1;

    public static final int TOKEN_STATUS_ACTIVE = 1;
    public static final int TOKEN_STATUS_LOCKEd = -1;
    public static final int TOKEN_STATUS_ASSIGNED = 0;
    public static final int TOKEN_STATUS_UNASSIGNED = -10;
    public static final int TOKEN_STATUS_FREE = -10;
    public static final int TOKEN_STATUS_SUSPENDED = -2;
    public static final int TOKEN_STATUS_ALL = 2;
    public static final int TOKEN_STATUS_LOST = -5;

    public AccessControlHandler() {
    }
    private Connection cn;
    private PreparedStatement pst, pst1;
    private ResultSet rs;
    public static AxiomConfigProvider config = null;
    AXIOMStatus aStatus = new AXIOMStatus();
    int error = -1;
    String strError = "Failed";
    int success = 0;
    String strSuccess = "Success";
    int pending = 1;
    String strPending = "Pending";
    int duplicate = 11;
    String strDuplicate;

    @Override
    public AXIOMStatus Load(AxiomConfigProvider axiomConfigProvider) {
        AXIOMStatus ax = new AXIOMStatus();

        try {
            config = axiomConfigProvider;
            Class.forName(config.getSourceDetails().getDatabaseType());
            ax.iStatus = 0;
            ax.strStatus = "success";
            return ax;
        } catch (Exception ex) {
            ax.strStatus = ex.getMessage();
        }
        ax.iStatus = -1;
        return ax;
    }

    @Override
    public AXIOMStatus Unload() {
        AXIOMStatus ax = new AXIOMStatus();
        ax.iStatus = 0;
        ax.strStatus = "success";
        return ax;
    }

    @Override
    public AXIOMStatus CheckAcces(Map m, String url, String operatorId, int operatorType,
            int reqExpiryDuration, int authStatus, Object obj, String hwOtpXmlFilePath, String hwOtpPasswordFilePath) {

            if (m == null || url == null || obj == null) {
            return null;
        }
        AccessMatrixSettings accessObj = (AccessMatrixSettings) obj;
        boolean b = accessObj.ListUsers;

        aStatus.iStatus = success;
        aStatus.strStatus = strSuccess;

        String sep = System.getProperty("file.separator");
        String usrhome = System.getProperty("catalina.home");
        if (usrhome == null) {
            usrhome = System.getenv("catalina.home");
        }
        Properties navSettings = null;
        if (usrhome != null) {
            usrhome += sep + "axiomv2-settings" + sep + "AccessControl.conf";
            PropsFileUtil p1 = new PropsFileUtil();
            if (p1.LoadFile(usrhome) == true) {
                navSettings = p1.properties;
            }
        }
        // assign Tokens
        if (url.contains(navSettings.getProperty("OTPToken.assignToken"))) {
            String[] _category = (String[]) m.get("_category");
            String[] _subcategory = (String[]) m.get("_subcategory");
            String[] _serialnumber = (String[]) m.get("_serialnumber");
            String[] _userid = (String[]) m.get("_userid");

            int iCategory = -1;
            if (_category != null) {
                iCategory = Integer.parseInt(_category[0]);
            }
            int isubcategory = -1;
            if (_category != null) {
                isubcategory = Integer.parseInt(_subcategory[0]);
            }
            if (iCategory == 3 && isubcategory == 1) {//SMS Token
                if (accessObj.addoobAssignToken == true) {
                    // if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = ASSIGN_OTP_TOKEN;
                    approval.itemid = "Assign " + "OOB" + " : " + "SMS Token";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = "" + 0;
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);//chSettingObj.authorizationDuration= need to add later
                    Date dexpiredOn = pexpiredOn.getTime();
                    /////
//                    if (operatorType == REQUESTER) {
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId))) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
//                    }
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                        // }

                    }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }
            if (iCategory == 3 && isubcategory == 2) {//Voice Token
                if (accessObj.addoobAssignToken == true) {
                    // if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = ASSIGN_OTP_TOKEN;
                    approval.itemid = "Assign " + "OOB" + " : " + "Voice Token";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = "" + 0;
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);//chSettingObj.authorizationDuration= need to add later
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId))) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }
                    }
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                        //}

                    }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            if (iCategory == 3 && isubcategory == 3) {//USSD Token
                if (accessObj.addoobAssignToken == true) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    //if (operatorType == REQUESTER) {
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = ASSIGN_OTP_TOKEN;
                    approval.itemid = "Assign " + "OOB" + " : " + "USSD Token";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = "" + 0;
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);//chSettingObj.authorizationDuration= need to add later
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId))) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

                    // }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            if (iCategory == 3 && isubcategory == 4) {//EMAIl Token
                if (accessObj.addoobAssignToken == true) {
                    // if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = ASSIGN_OTP_TOKEN;
                    approval.itemid = "Assign " + "OOB" + " : " + "EMAIL Token";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = "" + 0;
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);//chSettingObj.authorizationDuration= need to add later
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId))) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                        // }

                    }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            if (iCategory == 1 && isubcategory == 1) {//SW Web Token
                if (accessObj.addsoftAssignToken == true) {
                    //   if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = ASSIGN_OTP_TOKEN;
                    approval.itemid = "Assign " + "SW" + " : " + "Web Token";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = "" + 0;
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);//chSettingObj.authorizationDuration= need to add later
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId))) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

                    // }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            if (iCategory == 1 && isubcategory == 2) {//SW Mobile Token
                if (accessObj.addsoftAssignToken == true) {
                    //if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = ASSIGN_OTP_TOKEN;
                    approval.itemid = "Assign " + "SW" + " : " + "Mobile Token";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = "" + 0;
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);//chSettingObj.authorizationDuration= need to add later
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId))) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

                    //}
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            if (iCategory == 1 && isubcategory == 3) {//SW PC Token
                if (accessObj.addsoftAssignToken == true) {
                    // if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = ASSIGN_OTP_TOKEN;
                    approval.itemid = "Assign " + "SW" + " : " + "PC Token";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = "" + 0;
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);//chSettingObj.authorizationDuration= need to add later
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approval.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId))) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

                    // }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            } else// hw token without category and subcategory
            if (iCategory == 2 && isubcategory == 1) {
                if (accessObj.addhardAssignToken == true) {
                    //  if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = ASSIGN_OTP_TOKEN;
                    approval.itemid = "Assign " + "HW" + " : " + "OTP Token";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenSerialNo = _serialnumber[0];
                    approval.tokenStatus = "" + 0;
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);//chSettingObj.authorizationDuration= need to add later
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId))) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

                    // }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }
        }
        //  change  OTP         
        if (url.contains(navSettings.getProperty("OTPToken.changeTokenType"))) {
            String[] _category = (String[]) m.get("_category");
            String[] _subcategory = (String[]) m.get("_subcategory");
            String[] _userid = (String[]) m.get("_userid");
            // String[] _status= (String[]) m.get("_status");

            int iCategory = -1;
            if (_category != null) {
                iCategory = Integer.parseInt(_category[0]);
            }
            int isubcategory = -1;
            if (_category != null) {
                isubcategory = Integer.parseInt(_subcategory[0]);
            }
            if (_category[0].equals("3") && _subcategory[0].equals("1")) {
                if (accessObj.addoobChangetokenType == true) {
//                    if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_TYPE;
                    approval.itemid = "Change Token Type from" + "n" + "TO" + "";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    //  approval.tokenStatus =_status[0];

                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId))) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            if (_category[0].equals("3") && _subcategory[0].equals("2")) {
                if (accessObj.addoobChangetokenType == true) {
                    //  if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_TYPE;
                    approval.itemid = "Change Token Type";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    //  approval.tokenStatus =_status[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId))) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

                   // } else {
                    //  }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            if (_category[0].equals("3") && _subcategory[0].equals("3")) {
                if (accessObj.addoobChangetokenType == true) {
                    //  if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_TYPE;
                    approval.itemid = "Change Token Type";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    // approval.tokenStatus =_status[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId))) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                        //  }

                    }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            if (_category[0].equals("3") && _subcategory[0].equals("4")) {
                if (accessObj.addoobChangetokenType == true) {
                    // if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_TYPE;
                    approval.itemid = "Change Token Type";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    //approval.tokenStatus =_status[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId))) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

                    //  }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

//            if (_category[0].equals("1") && _subcategory[0].equals("1")) {
//                if (accessObj.editSWWEBToken == true) {
//                    if (authStatus == MAKERCHECKER_DISABLE) {
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//                    }
//                    if (operatorType == REQUESTER) {
//                        String[] _fromApprove = (String[]) m.get("_fromApprove");
//                        if (_fromApprove != null) {
//                            if (_fromApprove[0].equals("yes")) {
//                                aStatus.iStatus = success;
//                                aStatus.strStatus = strSuccess;
//                                return aStatus;
//                            }
//                        }
//                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                        Session sTemplate = suTemplate.openSession();
//                        AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                        ApprovalSetting approval = new ApprovalSetting();
//                        approval.action = CHANGE_OTP_TOKEN_TYPE;
//                        approval.itemid = "Change Token Type";
//                        approval.makerid = operatorId;
//                        approval.userId = _userid[0];
//                        approval.tokenCategory = iCategory;
//                        approval.tokenSubCategory = isubcategory;
//                        // approval.tokenStatus =_status[0];
//                        Calendar pexpiredOn = Calendar.getInstance();
//                        pexpiredOn.setTime(new Date());
//                        pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                        Date dexpiredOn = pexpiredOn.getTime();
//                        int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                        if (res == 0) {
//                            aStatus.iStatus = pending;
//                            aStatus.strStatus = strPending;
//                            return aStatus;
//                        } else {
//                            aStatus.iStatus = error;
//                            aStatus.strStatus = strError;
//                            return aStatus;
//                        }
//
//                    }
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
//            if (_category[0].equals("1") && _subcategory[0].equals("2")) {
//                if (accessObj.editSWMOBILEToken == true) {
//                    if (operatorType == REQUESTER) {
//                        if (authStatus == MAKERCHECKER_DISABLE) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                        String[] _fromApprove = (String[]) m.get("_fromApprove");
//                        if (_fromApprove != null) {
//                            if (_fromApprove[0].equals("yes")) {
//                                aStatus.iStatus = success;
//                                aStatus.strStatus = strSuccess;
//                                return aStatus;
//                            }
//                        }
//                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                        Session sTemplate = suTemplate.openSession();
//                        AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                        ApprovalSetting approval = new ApprovalSetting();
//                        approval.action = CHANGE_OTP_TOKEN_TYPE;
//                        approval.itemid = "Change Token Type";
//                        approval.makerid = operatorId;
//                        approval.userId = _userid[0];
//                        approval.tokenCategory = iCategory;
//                        // approval.tokenStatus =_status[0];
//                        approval.tokenSubCategory = isubcategory;
//                        Calendar pexpiredOn = Calendar.getInstance();
//                        pexpiredOn.setTime(new Date());
//                        pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                        Date dexpiredOn = pexpiredOn.getTime();
//                        int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                        if (res == 0) {
//                            aStatus.iStatus = pending;
//                            aStatus.strStatus = strPending;
//                            return aStatus;
//                        } else {
//                            aStatus.iStatus = error;
//                            aStatus.strStatus = strError;
//                            return aStatus;
//                        }
//
//                    }
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
//            if (_category[0].equals("1") && _subcategory[0].equals("3")) {
//                if (accessObj.editSWPCToken == true) {
//                    if (authStatus == MAKERCHECKER_DISABLE) {
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//                    }
//                    if (operatorType == REQUESTER) {
//                        String[] _fromApprove = (String[]) m.get("_fromApprove");
//                        if (_fromApprove != null) {
//                            if (_fromApprove[0].equals("yes")) {
//                                aStatus.iStatus = success;
//                                aStatus.strStatus = strSuccess;
//                                return aStatus;
//                            }
//                        }
//                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                        Session sTemplate = suTemplate.openSession();
//                        AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                        ApprovalSetting approval = new ApprovalSetting();
//                        approval.action = CHANGE_OTP_TOKEN_TYPE;
//                        approval.itemid = "Change Token Type";
//                        approval.makerid = operatorId;
//                        approval.userId = _userid[0];
//                        approval.tokenCategory = iCategory;
//                        approval.tokenSubCategory = isubcategory;
//                        Calendar pexpiredOn = Calendar.getInstance();
//                        //  approval.tokenStatus =_status[0];
//                        pexpiredOn.setTime(new Date());
//                        pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                        Date dexpiredOn = pexpiredOn.getTime();
//                        int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                        if (res == 0) {
//                            aStatus.iStatus = pending;
//                            aStatus.strStatus = strPending;
//                            return aStatus;
//                        } else {
//                            aStatus.iStatus = error;
//                            aStatus.strStatus = strError;
//                            return aStatus;
//                        }
//
//                    }
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
//            if (_category[0].equals("2") && _subcategory[0].equals("1")) {
//                if (accessObj.editSWPCToken == true) {
//                    if (operatorType == REQUESTER) {
//                        if (authStatus == MAKERCHECKER_DISABLE) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                        String[] _fromApprove = (String[]) m.get("_fromApprove");
//                        if (_fromApprove != null) {
//                            if (_fromApprove[0].equals("yes")) {
//                                aStatus.iStatus = success;
//                                aStatus.strStatus = strSuccess;
//                                return aStatus;
//                            }
//                        }
//                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                        Session sTemplate = suTemplate.openSession();
//                        AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                        ApprovalSetting approval = new ApprovalSetting();
//                        approval.action = CHANGE_OTP_TOKEN_TYPE;
//                        approval.itemid = "Change Token Type";
//                        approval.makerid = operatorId;
//                        approval.userId = _userid[0];
//                        approval.tokenCategory = iCategory;
//                        approval.tokenSubCategory = isubcategory;
//                        // approval.tokenStatus =_status[0];
//                        Calendar pexpiredOn = Calendar.getInstance();
//
//                        pexpiredOn.setTime(new Date());
//                        pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                        Date dexpiredOn = pexpiredOn.getTime();
//                        int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                        if (res == 0) {
//                            aStatus.iStatus = pending;
//                            aStatus.strStatus = strPending;
//                            return aStatus;
//                        } else {
//                            aStatus.iStatus = error;
//                            aStatus.strStatus = strError;
//                            return aStatus;
//                        }
//
//                    } else {
//                        aStatus.iStatus = error;
//                        aStatus.strStatus = strError;
//                        return aStatus;
//                    }
//                }
//            }
        }

        if (url.contains(("changeRoleStatus"))) {
            if (accessObj.editaccessMatrix == true) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {

                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }

        if (url.contains(("addRole"))) {
            if (accessObj.addaccessMatrix == true) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {

                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }
        if (url.contains(("removeAccessRole"))) {
            if (accessObj.removeaccessMatrix == true) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {

                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }
        if (url.contains(("EditRoles"))) {
            if (accessObj.editaccessMatrix == true) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {

                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }

        if (url.contains(navSettings.getProperty("OTPToken.resynctoken"))) {
            if (accessObj.addHWresyncToken == true) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {

                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }

        if (url.contains(navSettings.getProperty("PKIToken.losttoken"))) {
            String[] _category = (String[]) m.get("_category");
            String[] _subcategory = (String[]) m.get("_subcategory");
            String[] _userid = (String[]) m.get("_userid");
            String[] _status = (String[]) m.get("_status");
            int iCategory = -1;
            if (_category != null) {
                iCategory = Integer.parseInt(_category[0]);
            }
            int isubcategory = -1;
            if (_subcategory != null) {
                isubcategory = Integer.parseInt(_subcategory[0]);
            }
            if (iCategory == 2 && isubcategory == 1) {
                if (accessObj.addHWMarkAsLost == true) {
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_STATUS;
                    approval.itemid = "Change Token Status";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = _status[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId))) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }
        }

        if (url.contains(navSettings.getProperty("OTPToken.verifyOTP"))) {

            String[] _category = (String[]) m.get("_usertypeIDS");
            String[] _subcategory = (String[]) m.get("_subcategory");
            String[] _userid = (String[]) m.get("_userid");
            String[] _status = (String[]) m.get("_status");
            int iCategory = -1;
            if (_category != null) {
                iCategory = Integer.parseInt(_category[0]);
            }
            int isubcategory = -1;
            if (_category != null) {
                isubcategory = Integer.parseInt(_subcategory[0]);
            }
            int istatus = -1;
            if (_status != null) {
                istatus = Integer.parseInt(_status[0]);
            }
            if (_category[0].equals("2")) {
                if (accessObj.editHWOTPToken == true) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {

                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            if (_category[0].equals("1")) {
                if (accessObj.editSWMOBILEToken == true) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {

                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

            }

            if (_category[0].equals("3")) {
                if (accessObj.editOOBSMSToken == true) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {

                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

            }

//            if (_category[0].equals("3")) {
//                if (accessObj.editOOBVOICEToken == true) {
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//                    }
//                    else {
//                        aStatus.iStatus = error;
//                        aStatus.strStatus = strError;
//                        return aStatus;
//                    }
//
////                } 
//            if (_category[0].equals("3") && _subcategory[0].equals("2")) {
//                if (accessObj.editOOBSMSToken == true) {
////                    if (operatorType == REQUESTER) {
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//                    }
//                 else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
//            if (_category[0].equals("3") && _subcategory[0].equals("3")) {
//                if (accessObj.editOOBUSSDToken == true) {
//
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//                    }
//               else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
//              if (_category[0].equals("1") && _subcategory[0].equals("1") ) {
//                   if (accessObj.editSWWEBToken == true) {
//
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//                    }
//               else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//              }
//            if (_category[0].equals("3") && _subcategory[0].equals("4")) {
//                if (accessObj.editOOBEMAILToken == true) {
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//                    }
//                   
//                 else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }

        if (url.contains(navSettings.getProperty("OTPToken.replaceToken"))) {

            String[] _userIDReplaceHW = (String[]) m.get("_userIDReplaceHW");
            String[] _category = (String[]) m.get("_categoryReplaceHW");
            String[] _oldTokenStatus = (String[]) m.get("_oldTokenStatus");
            String[] _oldTokenSerialNo = (String[]) m.get("_oldTokenSerialNo");
            String[] _newTokenSerialNo = (String[]) m.get("_newTokenSerialNo");
            int iCategory = -1;
            if (_category != null) {
                iCategory = Integer.parseInt(_category[0]);
            }
            int istatus = -1;
            if (_oldTokenStatus != null) {
                istatus = Integer.parseInt(_oldTokenStatus[0]);
            }
            if (accessObj.addHWReplaceToken == true) {
//                //    if (operatorType == REQUESTER) {
                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                Session sTemplate = suTemplate.openSession();
                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                ApprovalSetting approval = new ApprovalSetting();
                approval.action = REPLACE_TOKEN;
                approval.itemid = "Replace Hardware Token ";
                approval.makerid = operatorId;
                approval.userId = _userIDReplaceHW[0];
                approval.tokenCategory = iCategory;
                approval.tokenStatus = _oldTokenStatus[0];
                approval.oldtokenSerialno = _oldTokenSerialNo[0];
                approval.newtokenSerialno = _newTokenSerialNo[0];
                Calendar pexpiredOn = Calendar.getInstance();
                pexpiredOn.setTime(new Date());
                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                Date dexpiredOn = pexpiredOn.getTime();
                Approvalsettings[] arrRequest = null;
                Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                if(approvalobj != null){
                for (int i = 0; i < approvalobj.length; i++) {
                    byte[] obje = approvalobj[i].getApprovalSettingEntry();
                    byte[] f = AxiomProtect.AccessDataBytes(obje);
                    ByteArrayInputStream bais = new ByteArrayInputStream(f);
                    Object object = oUtil.deserializeFromObject(bais);
                    ApprovalSetting approvalSetting = null;
                    if (object instanceof ApprovalSetting) {
                        approvalSetting = (ApprovalSetting) object;
                    }
                    if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                            && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                            && (approval.userId.equals(approvalSetting.userId)) && approval.tokenStatus.equals(approvalSetting.tokenStatus)) {
                        aStatus.iStatus = duplicate;
                        aStatus.strStatus = strDuplicate;
                        return aStatus;
                    }
                }}
                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                if (res == 0) {
                    aStatus.iStatus = pending;
                    aStatus.strStatus = strPending;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

               // } else {
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }

        // change status        
        if (url.contains(navSettings.getProperty("OTPToken.changeTokenStatus"))) {
            String[] _category = (String[]) m.get("_category");
            String[] _subcategory = (String[]) m.get("_subcategory");
            String[] _userid = (String[]) m.get("_userid");
            String[] _status = (String[]) m.get("_status");
            int iCategory = -1;
            if (_category != null) {
                iCategory = Integer.parseInt(_category[0]);
            }
            int isubcategory = -1;
            if (_category != null) {
                isubcategory = Integer.parseInt(_subcategory[0]);
            }
            int isubcategory1 = -1;
            if (_subcategory != null) {
                if (_subcategory.length == 2) {
                    isubcategory1 = Integer.parseInt(_subcategory[1]);
                }
            }
            int istatus = -1;
            if (_status != null) {
                istatus = Integer.parseInt(_status[0]);
            }
            if (_category[0].equals("2") && _subcategory[0].equals("1") && istatus == TOKEN_STATUS_ACTIVE) {
                if (accessObj.addhardMarkAsActive == true) {
//                //    if (operatorType == REQUESTER) {
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_STATUS;
                    approval.itemid = "Change Token Status";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = _status[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId)) && approval.tokenStatus.equals(approvalSetting.tokenStatus)) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

               // } else {
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

            }
            if (_category[0].equals("3") && _subcategory[0].equals("1") && istatus == TOKEN_STATUS_UNASSIGNED) {
                if (accessObj.addoobUnAssignToken == true) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    // if (operatorType == REQUESTER) {
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_STATUS;
                    approval.itemid = "Change Token Status";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = _status[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId)) && approval.tokenStatus.equals(approvalSetting.tokenStatus)) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;

             //   } else {
                }
            }

            if (_category[0].equals("3") && _subcategory[0].equals("2") && istatus == TOKEN_STATUS_UNASSIGNED) {
                if (accessObj.addoobUnAssignToken == true) {
                    //    if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_STATUS;
                    approval.itemid = "Change Token Status";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = _status[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId)) && approval.tokenStatus.equals(approvalSetting.tokenStatus)) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

                    // }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            if (_category[0].equals("3") && _subcategory[0].equals("3") && istatus == TOKEN_STATUS_UNASSIGNED) {
                if (accessObj.addoobUnAssignToken == true) {
                    //  if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_STATUS;
                    approval.itemid = "Change Token Status";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = _status[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId)) && approval.tokenStatus.equals(approvalSetting.tokenStatus)) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                        //  }

                    }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            if (_category[0].equals("3") && _subcategory[0].equals("4") && istatus == TOKEN_STATUS_UNASSIGNED) {
                if (accessObj.addoobUnAssignToken == true) {
                    // if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_STATUS;
                    approval.itemid = "Change Token Status";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = _status[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId)) && approval.tokenStatus.equals(approvalSetting.tokenStatus)) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                        //   }

                    }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            if (_category[0].equals("1") && _subcategory[0].equals("1") && istatus == TOKEN_STATUS_UNASSIGNED) {
                if (accessObj.addsoftUnAssignToken == true) {

                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_STATUS;
                    approval.itemid = "Change Token Status";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = _status[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId)) && approval.tokenStatus.equals(approvalSetting.tokenStatus)) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            if (_category[0].equals("1") && _subcategory[0].equals("2") && istatus == TOKEN_STATUS_UNASSIGNED) {
                //  if (operatorType == REQUESTER) {
                if (accessObj.addsoftUnAssignToken == true) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_STATUS;
                    approval.itemid = "Change Token Status";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = _status[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId)) && approval.tokenStatus.equals(approvalSetting.tokenStatus)) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            if (_category[0].equals("1") && _subcategory[0].equals("3") && istatus == TOKEN_STATUS_UNASSIGNED) {
                if (accessObj.addsoftUnAssignToken == true) {

                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_STATUS;
                    approval.itemid = "Change Token Status";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = _status[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId)) && approval.tokenStatus.equals(approvalSetting.tokenStatus)) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            if (_category[0].equals("2") && _subcategory[0].equals("1") && istatus == TOKEN_STATUS_UNASSIGNED) {
                if (accessObj.addHwUnAssignToken == true) {

                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_STATUS;
                    approval.itemid = "Change Token Status";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = _status[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId)) && approval.tokenStatus.equals(approvalSetting.tokenStatus)) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

         //TO change suspend hw token
            if (_category[0].equals("2") && _subcategory[0].equals("1") && istatus == TOKEN_STATUS_UNASSIGNED) {
                if (accessObj.addHwUnAssignToken == true) {

                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_STATUS;
                    approval.itemid = "Change Token Status";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = _status[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId)) && approval.tokenStatus.equals(approvalSetting.tokenStatus)) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            if (_category[0].equals("2") && istatus == TOKEN_STATUS_SUSPENDED) {
                if (accessObj.addHWMarkAsSuspended == true) {
//                  //  if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_STATUS;
                    approval.itemid = "Change Token Status";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = _status[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId)) && approval.tokenStatus.equals(approvalSetting.tokenStatus)) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                        //     }

                    }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            if (_category[0].equals("3") && _subcategory[0].equals("2") && istatus == TOKEN_STATUS_ACTIVE) {
                if (accessObj.addoobMarkAsActive == true) {
                    //   if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_STATUS;
                    approval.itemid = "Change Token Status";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = _status[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId)) && approval.tokenStatus.equals(approvalSetting.tokenStatus)) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

                    //  }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            if (_category[0].equals("3") && _subcategory[0].equals("2") && istatus == TOKEN_STATUS_ACTIVE) {
                if (accessObj.addoobMarkAsActive == true) {
                    //   if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_STATUS;
                    approval.itemid = "Change Token Status";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = _status[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId)) && approval.tokenStatus.equals(approvalSetting.tokenStatus)) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

                    //  }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            if (_category[0].equals("3") && _subcategory[0].equals("1") && istatus == TOKEN_STATUS_ACTIVE) {
                if (accessObj.addoobMarkAsActive == true) {
//                  //  if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_STATUS;
                    approval.itemid = "Change Token Status";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = _status[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId)) && approval.tokenStatus.equals(approvalSetting.tokenStatus)) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

                    //   }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            if (_category[0].equals("3") && _subcategory[0].equals("4") && istatus == TOKEN_STATUS_ACTIVE) {
                if (accessObj.addoobMarkAsActive == true) {
//                  //  if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_STATUS;
                    approval.itemid = "Change Token Status";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = _status[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                        // }

                    }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }
            // sw token

            if (_category[0].equals("1") && _subcategory[0].equals("1") && istatus == TOKEN_STATUS_ACTIVE) {
                if (accessObj.addsoftMarkAsActive == true) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_STATUS;
                    approval.itemid = "Change Token Status";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = _status[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    {
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId)) && approval.tokenStatus.equals(approvalSetting.tokenStatus)) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            if (_category[0].equals("1") && _subcategory[0].equals("2") && istatus == TOKEN_STATUS_ACTIVE) {
                if (accessObj.addsoftMarkAsActive == true) {
//                    if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].contains("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_STATUS;
                    approval.itemid = "Change Token Status";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = _status[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId)) && approval.tokenStatus.equals(approvalSetting.tokenStatus)) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

                    // }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            if (_category[0].equals("1") && _subcategory[0].equals("3") && istatus == TOKEN_STATUS_ACTIVE) {
                if (accessObj.addsoftMarkAsActive == true) {
//                    if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_STATUS;
                    approval.itemid = "Change Token Status";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = _status[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId)) && approval.tokenStatus.equals(approvalSetting.tokenStatus)) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

//                    }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }
            //Software Token Suspend Option

            if ((iCategory == 1 && isubcategory == 1 && isubcategory1 == 1) || (iCategory == 1 && isubcategory == 2 && isubcategory1 == 2) || (iCategory == 1 && isubcategory == 3 && isubcategory1 == 3) || (iCategory == 1 && isubcategory == 4 && isubcategory1 == 4) && istatus == TOKEN_STATUS_SUSPENDED) {
                if (accessObj.addsoftMarkSuspende == true) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_STATUS;
                    approval.itemid = "Change Token Status";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = _status[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId)) && approval.tokenStatus.equals(approvalSetting.tokenStatus)) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            //Hw token Suspend Option
//            if ((iCategory == 1 &&  isubcategory == 1 && isubcategory1 == 1 && istatus==TOKEN_STATUS_SUSPENDED)){
//            if (accessObj.addHWMarkAsSuspended == true) {
//                if (authStatus == MAKERCHECKER_DISABLE) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                        String[] _fromApprove = (String[]) m.get("_fromApprove");
//                        if (_fromApprove != null) {
//                            if (_fromApprove[0].equals("yes")) {
//                                aStatus.iStatus = success;
//                                aStatus.strStatus = strSuccess;
//                                return aStatus;
//                            }
//                        }
//                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                        Session sTemplate = suTemplate.openSession();
//                        AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                        ApprovalSetting approval = new ApprovalSetting();
//                        approval.action = CHANGE_OTP_TOKEN_STATUS;
//                        approval.itemid = "Change Token Status";
//                        approval.makerid = operatorId;
//                        approval.userId = _userid[0];
//                        approval.tokenCategory = iCategory;
//                        approval.tokenSubCategory = isubcategory;
//                        approval.tokenStatus = _status[0];
//                        Calendar pexpiredOn = Calendar.getInstance();
//                        pexpiredOn.setTime(new Date());
//                        pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                        Date dexpiredOn = pexpiredOn.getTime();
//                        int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                        if (res == 0) {
//                            aStatus.iStatus = pending;
//                            aStatus.strStatus = strPending;
//                            return aStatus;
//                        } else {
//                            aStatus.iStatus = error;
//                            aStatus.strStatus = strError;
//                            return aStatus;
//                        }
//            } else {
//                aStatus.iStatus = error;
//                aStatus.strStatus = strError;
//                return aStatus;
//            }
//            }
            //oob TOken Suspend
            if ((iCategory == 3 && isubcategory == 1 && istatus == TOKEN_STATUS_SUSPENDED) || (iCategory == 3 && isubcategory == 2 && istatus == TOKEN_STATUS_SUSPENDED) || (iCategory == 3 && isubcategory == 3 && istatus == TOKEN_STATUS_SUSPENDED) || (iCategory == 3 && isubcategory == 4 && istatus == TOKEN_STATUS_SUSPENDED)) {
                if (accessObj.addoobMarkAsSuspended == true) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = CHANGE_OTP_TOKEN_STATUS;
                    approval.itemid = "Change Token Status";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenSubCategory = isubcategory;
                    approval.tokenStatus = _status[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId)) && approval.tokenStatus.equals(approvalSetting.tokenStatus)) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            // hw token replace token                                                                   
        }
        if (url.contains(navSettings.getProperty("OTPToken.replaceToken"))) {

            if (accessObj.editSWWEBToken == true) {
                //  if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
                String[] _userid = (String[]) m.get("_userIDReplaceHW");
                String[] _oldTokenSerialNo = (String[]) m.get("_oldTokenSerialNo");
                String stroldTokenSerialNo = null;
                if (_oldTokenSerialNo != null) {
                    stroldTokenSerialNo = _oldTokenSerialNo[0];
                }
                String[] _newTokenSerialNo = (String[]) m.get("_newTokenSerialNo");
                String strnewTokenSerialNo = null;
                if (_newTokenSerialNo != null) {
                    strnewTokenSerialNo = _newTokenSerialNo[0];
                }
                String[] _oldTokenStatus = (String[]) m.get("_oldTokenStatus");
                String[] _categoryReplaceHW = (String[]) m.get("_categoryReplaceHW");
                String[] _subcategoryReplaceHW = (String[]) m.get("_subcategoryReplaceHW");
                if (_categoryReplaceHW[0].equals("2") && _subcategoryReplaceHW[0].equals("1")) {
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = OTP_TOKEN_REPLACE;
                    approval.itemid = "Replace OTP Token";
                    approval.makerid = operatorId;
                    approval.tokenSerialNo = strnewTokenSerialNo;
                    approval.userId = _userid[0];
                    approval.tokenCategory = Integer.parseInt(_categoryReplaceHW[0]);
                    approval.tokenSubCategory = Integer.parseInt(_subcategoryReplaceHW[0]);
                    approval.tokenSerialNoOld = stroldTokenSerialNo;
                    approval.tokenStatus = _oldTokenStatus[0];
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId)) && approval.tokenStatus.equals(approvalSetting.tokenStatus)) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
                // }
            }

        }
        //end OTP                    

        //PKI Tokens       
        // Assign Token
        if (url.contains(navSettings.getProperty("PKIToken.assignToken"))) {

            String[] _serialnumber = (String[]) m.get("_ser");
            String[] _category = (String[]) m.get("_category");
            String[] _userid = (String[]) m.get("_userid");
            String[] _status = (String[]) m.get("_status");
            int iCategory = -1;
            if (_category != null) {
                iCategory = Integer.parseInt(_category[0]);
            }

            if (_category[0].equals("1")) {
                if (accessObj.addSWPKIToken == true) {
                    //  if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    if (operatorType == REQUESTER) {
                        String[] _fromApprove = (String[]) m.get("_fromApprove");
                        if (_fromApprove != null) {
                            if (_fromApprove[0].equals("yes")) {
                                aStatus.iStatus = success;
                                aStatus.strStatus = strSuccess;
                                return aStatus;
                            }
                        }

                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                        Session sTemplate = suTemplate.openSession();
                        AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                        ApprovalSetting approval = new ApprovalSetting();
                        approval.action = ASSIGN_HW_PKI_TOKEN;
                        approval.itemid = "Change Token Type";
                        approval.makerid = operatorId;
                        approval.userId = _userid[0];
                        approval.tokenCategory = iCategory;

                        approval.tokenStatus = _status[0];
//                    approval.tokenSubCategory = isubcategory;
                        Calendar pexpiredOn = Calendar.getInstance();
                        pexpiredOn.setTime(new Date());
                        pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                        Date dexpiredOn = pexpiredOn.getTime();
                        Approvalsettings[] arrRequest = null;
                        Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                        if(approvalobj != null){
                        for (int i = 0; i < approvalobj.length; i++) {
                            byte[] obje = approvalobj[i].getApprovalSettingEntry();
                            byte[] f = AxiomProtect.AccessDataBytes(obje);
                            ByteArrayInputStream bais = new ByteArrayInputStream(f);
                            Object object = oUtil.deserializeFromObject(bais);
                            ApprovalSetting approvalSetting = null;
                            if (object instanceof ApprovalSetting) {
                                approvalSetting = (ApprovalSetting) object;
                            }
                            if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                    && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                    && (approval.userId.equals(approvalSetting.userId)) && approval.tokenStatus.equals(approvalSetting.tokenStatus)) {
                                aStatus.iStatus = duplicate;
                                aStatus.strStatus = strDuplicate;
                                return aStatus;
                            }
                        }}
                        int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                        if (res == 0) {
                            aStatus.iStatus = pending;
                            aStatus.strStatus = strPending;
                            return aStatus;
                        } else {
                            aStatus.iStatus = error;
                            aStatus.strStatus = strError;
                            return aStatus;
                        }
                        // }
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }
                }
            }

            if (_category[0].equals("2")) {
                if (accessObj.addHWPKIToken == true) {
//                    if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
//                        String[] _fromApprove = (String[]) m.get("_fromApprove");
//                        if (_fromApprove != null) {
//                            if (_fromApprove[0].equals("yes")) {
//                                aStatus.iStatus = success;
//                                aStatus.strStatus = strSuccess;
//                                return aStatus;
//                            }
//                        }
//                        String str_serialnumber = null;
//                        if (_serialnumber != null) {
//                            str_serialnumber = _serialnumber[0];
//                        }
//                        SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                        Session sTemplate = suTemplate.openSession();
//                        AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                        ApprovalSetting approval = new ApprovalSetting();
//                        approval.action = ASSIGN_HW_PKI_TOKEN;
//                        approval.itemid = "Assign HW PKI Token";
//                        approval.makerid = operatorId;
//                        approval.userId = _userid[0];
//                        approval.tokenCategory = iCategory;
//                        approval.tokenSerialNo = str_serialnumber;
//                        approval.tokenStatus = _status[0];
//                        Calendar pexpiredOn = Calendar.getInstance();
//
//                        pexpiredOn.setTime(new Date());
//                        pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                        Date dexpiredOn = pexpiredOn.getTime();
//                        int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                        if (res == 0) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

                // }
            }
        }

        ///////////////////////////////////////////////////////////
        // change status in HW and SW PKI
        if (url.contains(navSettings.getProperty("PKIToken.sendpfxfile"))) {

            if (accessObj.editHWPKIToken == true) {

//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }
//                    String[] _userid = (String[]) m.get("_userid");
//                    String[] _category = (String[]) m.get("_category");
//                    String[] _status = (String[]) m.get("_status");
////                if (operatorType == REQUESTER) {
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = PKI_SEND_PFX;
//                    approval.itemid = "Send PFX file";
//                    approval.makerid = operatorId;
//                    approval.userId = _userid[0];
//                    approval.tokenCategory = Integer.parseInt(_category[0]);
//                    approval.tokenStatus = _status[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                    } else {
//                        aStatus.iStatus = error;
//                        aStatus.strStatus = strError;
//                        return aStatus;
//                    }
                //  }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("PKIToken.sendpfxpassword"))) {

            if (accessObj.editHWPKIToken == true) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                String[] _fromApprove = (String[]) m.get("_fromApprove");
//                if (_fromApprove != null) {
//                    if (_fromApprove[0].equals("yes")) {
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//                    }
//                }
//                String[] _userid = (String[]) m.get("_userid");
//                String[] _category = (String[]) m.get("_category");
//                String[] _status = (String[]) m.get("_status");
////                if (operatorType == REQUESTER) {
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = PKI_SEND_PFX_PASSWORD;
//                approval.itemid = "Send PFX Password";
//                approval.makerid = operatorId;
//                approval.userId = _userid[0];
//                approval.tokenStatus = _status[0];
//                approval.tokenCategory = Integer.parseInt(_category[0]);
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("PKIToken.sendRegistrationCode"))) {
            String[] _category = (String[]) m.get("_category");
            String[] _userid = (String[]) m.get("_userid");
            String[] _subcategory = (String[]) m.get("_subcategory");
            String[] _status = (String[]) m.get("_status");
            int iCategory = -1;
            if (_category != null) {
                iCategory = Integer.parseInt(_category[0]);
            }

            if (_category[0].equals("1") && _category[0].equals("1")) {

                if (accessObj.editSWWEBToken == true) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;

                }
            }

            if (_category[0].equals("1") && _category[0].equals("2")) {
                if (accessObj.editSWMOBILEToken == true) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

            }
            if (_category[0].equals("2")) {
                if (accessObj.editHWPKIToken == true) {

                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

            }

            if (_category[0].equals("1")) {
                if (accessObj.editSWPKIToken == true) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
//                    if (operatorType == REQUESTER) {
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = ASSIGN_HW_PKI_TOKEN;
                    approval.itemid = "Change Token Type";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenStatus = _status[0];
//                        approval.tokenSubCategory = isubcategory;
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }
        }
        if (url.contains(navSettings.getProperty("OTPToken.sendRegistrationCode"))) {
            String[] _category = (String[]) m.get("_category");
            String[] _userid = (String[]) m.get("_userid");
            String[] _sendby = (String[]) m.get("_type");
            int iSubcategroy = 0;
            String[] _subcategory = (String[]) m.get("_subcategory");
            int iCategory = -1;
            if (_category != null) {
                iCategory = Integer.parseInt(_category[0]);
            }
            if (_subcategory != null) {
                iSubcategroy = Integer.parseInt(_subcategory[0]);
            }
            if (accessObj.addsoftResendActCode == true) {
                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }

//                    if (operatorType == REQUESTER) {
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                Session sTemplate = suTemplate.openSession();
                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                ApprovalSetting approval = new ApprovalSetting();
                approval.action = ASSIGNTOKEN_SENDREGCODE;
                approval.itemid = "Send Registration Code";
                approval.makerid = operatorId;
                approval.userId = _userid[0];
                approval.tokenCategory = Integer.parseInt(_category[0]);
                approval.allowAlert = _sendby[0];
                Calendar pexpiredOn = Calendar.getInstance();
                pexpiredOn.setTime(new Date());
                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                Date dexpiredOn = pexpiredOn.getTime();
                Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.tokenCategory == approvalSetting.tokenCategory) && (approval.tokenSubCategory == approvalSetting.tokenSubCategory)
                                && (approval.userId.equals(approvalSetting.userId)) && (approval.allowAlert.equals(approvalSetting.allowAlert))) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                
                
                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                if (res == 0) {
                    aStatus.iStatus = pending;
                    aStatus.strStatus = strPending;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;

            }

//            if (iCategory==1 && iSubcategroy==1) {
//                if (accessObj.addsoftResendActCode==true) {
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = ASSIGNTOKEN_SENDREGCODE;
//                    approval.itemid = "Send Registration Code";
//                    approval.makerid = operatorId;
//                    approval.userId = _userid[0];
//                    approval.tokenCategory = Integer.parseInt(_category[0]);
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
//                        aStatus.iStatus = pending;
//                        aStatus.strStatus = strPending;
//                        return aStatus;
//                    } else {
//                        aStatus.iStatus = error;
//                        aStatus.strStatus = strError;
//                        return aStatus;
//                    }
//
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("PKIToken.changeTokenStatus"))) {
            String[] _category = (String[]) m.get("_category");
            String[] _userid = (String[]) m.get("_userid");
            String[] _status = (String[]) m.get("_status");
            String[] _unitId = (String[]) m.get("_unitId");
            int iCategory = -1;
            if (_category != null) {
                iCategory = Integer.parseInt(_category[0]);
            }

            if (_category[0].equals("2")) {
                if (accessObj.editHWPKIToken == true) {
                    //   if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
//                    if (operatorType == REQUESTER) {
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = PKI_TOKEN_STATUS;
                    approval.itemid = "Change Token Status";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenStatus = _status[0];
//                        approval.tokenSubCategory = isubcategory;
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

            if (_category[0].equals("1")) {
                if (accessObj.editSWPKIToken == true) {
                    //   if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
//                    if (operatorType == REQUESTER) {
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = ASSIGN_HW_PKI_TOKEN;
                    approval.itemid = "Change Token Type";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenStatus = _status[0];
//                        approval.tokenSubCategory = isubcategory;
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                        //   }

                    }
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }
        }
        // Remove PKI Tokens
        if (url.contains(navSettings.getProperty("PKIToken.RemoveStatus"))) {
            String[] _category = (String[]) m.get("_category");
//            String[] _category = (String[]) m.get("_category");
            String[] _userid = (String[]) m.get("_userid");
            String[] _status = (String[]) m.get("_status");
            String[] _unitId = (String[]) m.get("_unitId");
            int iCategory = -1;
            if (_category != null) {
                iCategory = Integer.parseInt(_category[0]);
            }
            if (_category[0].equals("2")) {
                if (accessObj.editHWPKIToken == true) {
                    //   if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
//                    if (operatorType == REQUESTER) {
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = PKI_TOKEN_STATUS;
                    approval.itemid = "Change Token Type";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenStatus = _status[0];
//                        approval.tokenSubCategory = isubcategory;
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

            }

            if (_category[0].equals("1")) {
                if (accessObj.editSWPKIToken == true) {
                    //   if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
//                  
                    String[] _fromApprove = (String[]) m.get("_fromApprove");
                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    }
                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                    Session sTemplate = suTemplate.openSession();
                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                    ApprovalSetting approval = new ApprovalSetting();
                    approval.action = ASSIGN_HW_PKI_TOKEN;
                    approval.itemid = "Change Token Type";
                    approval.makerid = operatorId;
                    approval.userId = _userid[0];
                    approval.tokenCategory = iCategory;
                    approval.tokenStatus = _status[0];
//                        approval.tokenSubCategory = isubcategory;
                    Calendar pexpiredOn = Calendar.getInstance();
                    pexpiredOn.setTime(new Date());
                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                    Date dexpiredOn = pexpiredOn.getTime();
                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                    if (res == 0) {
                        aStatus.iStatus = pending;
                        aStatus.strStatus = strPending;
                        return aStatus;
                    } else {
                        aStatus.iStatus = error;
                        aStatus.strStatus = strError;
                        return aStatus;
                    }

                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
                //}
            }

        }
        if (url.contains(navSettings.getProperty("Certificate.addCertificate"))) {

            if (accessObj.addCerticate == true) {
//              //  if (operatorType == REQUESTER) {

                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _userid = (String[]) m.get("_userid");
////                if (operatorType == REQUESTER) {
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = ASSIGN_CERTIFICATE;
//                    approval.itemid = "Assign Certificate";
//                    approval.makerid = operatorId;
//                    approval.userId = _userid[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;

            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }

        if (url.contains(navSettings.getProperty("Certificate.changeCertificate"))) {

            if (accessObj.editCerticate == true) {
//              //  if (operatorType == REQUESTER) {

                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _userid = (String[]) m.get("_userSENDCERT");
//                    String[] _emailSENDCERT = (String[]) m.get("_emailSENDCERT");
////                if (operatorType == REQUESTER) {
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = SEND_CERTIFICATE;
//                    approval.itemid = "Send Certificate to " + _emailSENDCERT;
//                    approval.makerid = operatorId;
//                    approval.userId = _userid[0];
//                    approval.sendCertTo = _emailSENDCERT[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = pending;
                aStatus.strStatus = strPending;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }
        if (url.contains(navSettings.getProperty("Certificate.renewCertificate"))) {

            if (accessObj.editCerticate == true) {
//            //    if (operatorType == REQUESTER) {

                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _userid = (String[]) m.get("_userid");
////                String[] _emailSENDCERT = (String[]) m.get("_emailSENDCERT");
////                if (operatorType == REQUESTER) {
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = RENEW_CERTIFICATE;
//                    approval.itemid = "Renew Certificate";
//                    approval.makerid = operatorId;
//                    approval.userId = _userid[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }
        if (url.contains(navSettings.getProperty("Certificate.revokeCertificate"))) {

            if (accessObj.removeCerticate == true) {
                // if (operatorType == REQUESTER) {

                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _userid = (String[]) m.get("_userid");
//                    String[] _reason = (String[]) m.get("_reason");
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = REVOKE_CERTIFICATE;
//                    approval.itemid = "Revoke Certificate";
//                    approval.makerid = operatorId;
//                    approval.userId = _userid[0];
//                    String strReason = null;
//                    if (_reason != null) {
//                        strReason = _reason[0];
//                    }
//                    approval.cerRevokeReason = strReason;
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }

        //end from amol
        //channels
        if (url.contains(navSettings.getProperty("channels.list"))) {
            if (accessObj.listChannel == true) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("channels.add"))) {
            if (accessObj.addChannel == true) {
                //  if (operatorType == REQUESTER) {

                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//               // if (operatorType == REQUESTER) {
                String[] _fromApprove = (String[]) m.get("_fromApprove");

                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                } else {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;

                }
//                String[] _ch_name = (String[]) m.get("_ch_name");
//                String[] _ch_virtual_path = (String[]) m.get("_ch_virtual_path");
//                String[] _ch_status = (String[]) m.get("_ch_status");
//                String[] _op_name = (String[]) m.get("_op_name");
//                String[] _op_email = (String[]) m.get("_op_email");
//                String[] _op_phone = (String[]) m.get("_op_phone");
//                String strChannelName = null;
//                String _ch_virtual_pathS = null;
//                String _op_nameS = null;
//                String _op_emailS = null;
//                String _op_phoneS = null;
//                if (_ch_name != null) {
//                    strChannelName = _ch_name[0];
//                }
//                if (_ch_virtual_path != null) {
//                    _ch_virtual_pathS = _ch_virtual_path[0];
//                }
//                if (_op_name != null) {
//                    _op_nameS = _op_name[0];
//                }
//                if (_op_email != null) {
//                    _op_emailS = _op_email[0];
//                }
//                if (_op_phone != null) {
//                    _op_phoneS = _op_phone[0];
//                }
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = CHANNEL_ADD;
//                approval.itemid = "Add Channel";
//                approval.makerid = operatorId;
//
//                approval.chanel_name = strChannelName;
//                approval.chanel_operator_email = _op_emailS;
//                approval.chanel_operator_name = _op_nameS;
//                approval.chanel_operator_phone = _op_phoneS;
//                approval.chanel_status = _ch_status[0];
//                approval.chanel_virtual_path = _ch_virtual_pathS;
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;

            }
        }

        //pramodcheck
        if (url.contains(navSettings.getProperty("channels.edit"))) {

            if (accessObj.editChannel == true) {
//                if (operatorType == REQUESTER) {

                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    } else {
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//
//                    }
//                String[] _ch_name = (String[]) m.get("_ch_nameE");
//                String[] _ch_status = (String[]) m.get("_ch_statusE");
//                String[] _ch_virtual_path = (String[]) m.get("_ch_virtual_pathE");
//                String[] _channelid = (String[]) m.get("_channelidE");
//                String[] _rem_expirymin = (String[]) m.get("_rem_expiryminE");
//                String[] _rem_loginid = (String[]) m.get("_rem_loginidE");
//                String[] _rem_password = (String[]) m.get("_rem_passwordE");
//                String[] _rem_passwordC = (String[]) m.get("_rem_passwordEC");
//                String _ch_nameS = null;
//                String _ch_virtual_pathS = null;
//                String _channelidS = null;
//                String _rem_expiryminS = null;
//                String _rem_loginidS = null;
//                String _rem_passwordS = null;
//                String _rem_passwordCS = null;
//                if (_ch_name != null) {
//                    _ch_nameS = _ch_name[0];
//                }
//                if (_ch_virtual_path != null) {
//                    _ch_virtual_pathS = _ch_virtual_path[0];
//                }
//                if (_channelid != null) {
//                    _channelidS = _channelid[0];
//                }
//                if (_rem_expirymin != null) {
//                    _rem_expiryminS = _rem_expirymin[0];
//                }
//                if (_rem_loginid != null) {
//                    _rem_loginidS = _rem_loginid[0];
//                }
//                if (_rem_password != null) {
//                    _rem_passwordS = _rem_password[0];
//                }
//                if (_rem_passwordC != null) {
//                    _rem_passwordCS = _rem_passwordC[0];
//                }
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = CHANNEL_EDIT;
//                approval.itemid = "Edit Channel";
//                approval.makerid = operatorId;
//                approval.chanel_name = _ch_nameS;
//                approval.chanel_status = _ch_status[0];
//                approval.chanel_virtual_path = _ch_virtual_pathS;
//                approval.chanel_channelid = _channelidS;
//                approval.chanel_expirymin = _rem_expiryminS;
//                approval.chanel_remote_loginid = _rem_loginidS;
//                approval.chanel_remote_password = _rem_passwordS;
//                approval.chanel_remote_password_confirm = _rem_passwordCS;
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        if (url.contains(navSettings.getProperty("channels.status"))) {
            String[] _ch_status = (String[]) m.get("_ch_status");
            String[] _channelid = (String[]) m.get("_channelid");

            if (accessObj.editChannel == true) {
//                if (operatorType == REQUESTER) {
//
//                    if (authStatus == MAKERCHECKER_DISABLE) {
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//                    }
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                        } else {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;

            }

//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = CHANNEL_STATUS;
//                approval.itemid = "Channel Status";
//                approval.makerid = operatorId;
//                approval.chanel_status = _ch_status[0];
//                approval.chanel_channelid = _channelid[0];
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
        }
        // }
        if (url.contains(navSettings.getProperty("channels.remoteaccess"))) {
            String[] _ch_status = (String[]) m.get("_ch_rem_status");
            String[] _channelid = (String[]) m.get("_channelid");
            if (accessObj.editChannel == true) {
//                if (operatorType == REQUESTER) {

//                    if (authStatus == MAKERCHECKER_DISABLE) {
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//                    }
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                        } else {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;

            }

//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = CHANNEL_REMOTE_ACCESS;
//                approval.itemid = "Channel Remote Access";
//                approval.makerid = operatorId;
//                approval.chanel_status = _ch_status[0];
//                approval.chanel_channelid = _channelid[0];
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        //operators
        if (url.contains(navSettings.getProperty("operators.list")) || url.contains(navSettings.getProperty("operators.list1"))) {
            if (accessObj.listOperator == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("operators.add"))) {
            if (accessObj.addOperator == true) {
//                if (operatorType == REQUESTER) {

                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                        } else {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//                String[] _op_name = (String[]) m.get("_oprname");
//                String[] _op_phone = (String[]) m.get("_oprphone");
//                String[] _op_email = (String[]) m.get("_opremail");
//                String[] _op_role = (String[]) m.get("_oprrole");
//                String[] _units = (String[]) m.get("_units");
//                String[] _operatorType = (String[]) m.get("_operatorType");
//                String _op_nameS = null;
//                String _op_phoneS = null;
//                String _op_emailS = null;
//                if (_op_name != null) {
//                    _op_nameS = _op_name[0];
//                }
//                if (_op_name != null) {
//                    _op_phoneS = _op_phone[0];
//                }
//                if (_op_email != null) {
//                    _op_emailS = _op_email[0];
//                }
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = OPERATOR_ADD;
//                approval.itemid = "Opeartor Add";
//                approval.makerid = operatorId;
//                approval.operator_name = _op_nameS;
//                approval.operator_phone = _op_phoneS;
//                approval.operator_email = _op_emailS;
//                approval.operator_role = _op_role[0];
//                approval.operator_units = _units[0];
//                approval.operator_type = _operatorType[0];
//
//                Calendar pexpiredOn = Calendar.getInstance();
//
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("operators.editoperator"))) {

            if (accessObj.editOperator == true) {
//                if (operatorType == REQUESTER) {

                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                        } else {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;

            }
//                String[] _op_name = (String[]) m.get("_oprnameE");
//                String[] _op_phone = (String[]) m.get("_oprphoneE");
//                String[] _op_email = (String[]) m.get("_opremailE");
//                String[] _op_role = (String[]) m.get("_oprroleidE");
//                String[] _oprstatusE = (String[]) m.get("_oprstatusE");
//                String[] _units = (String[]) m.get("_unitsEDIT");
//                String[] _operatorType = (String[]) m.get("_operatorTypeE");
//                String[] _opridE = (String[]) m.get("_opridE");
//
//                String _op_nameS = null;
//                String _op_phoneS = null;
//                String _op_emailS = null;
//                if (_op_name != null) {
//                    _op_nameS = _op_name[0];
//                }
//                if (_op_name != null) {
//                    _op_phoneS = _op_phone[0];
//                }
//                if (_op_email != null) {
//                    _op_emailS = _op_email[0];
//                }
//
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = OPERATOR_EDIT;
//                approval.itemid = "Opeartor Edit";
//                approval.makerid = operatorId;
//                approval.operator_name = _op_nameS;
//                approval.operator_phone = _op_phoneS;
//                approval.operator_email = _op_emailS;
//                approval.operator_status = _oprstatusE[0];
//                approval.operator_role = _op_role[0];
//                approval.operator_units = _units[0];
//                approval.operator_type = _operatorType[0];
//                approval.operator_id = _opridE[0];
//
//                Calendar pexpiredOn = Calendar.getInstance();
//
//                pexpiredOn.setTime(
//                        new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("operators.status"))) {

            if (accessObj.editOperator == true) {
//                if (operatorType == REQUESTER) {

//                    if (authStatus == MAKERCHECKER_DISABLE) {
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//                    }
//                    String[] _op_status = (String[]) m.get("_op_status");
//                    String[] _operId = (String[]) m.get("_oprid");
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {

                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;

            }
//                    } else {
//
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//                    }
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = OPERATOR_STATUS;
//                approval.itemid = "Opeartors Status";
//                approval.makerid = operatorId;
//                approval.operator_id = _operId[0];
//                approval.operator_status = _op_status[0];
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

            //  }
//            } else {
//                aStatus.iStatus = error;
//                aStatus.strStatus = strError;
//                return aStatus;
//            }
        }
        if (url.contains(navSettings.getProperty("operators.role"))) {
            if (accessObj.editOperator == true) {
                //  if (operatorType == REQUESTER) {

                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                        } else {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//                String[] _operId = (String[]) m.get("_oprid");
//                String[] _roleId = (String[]) m.get("_roleId");
//                String[] _roleName = (String[]) m.get("_roleName");
//                String _roleNameS = null;
//                if (_roleName != null) {
//                    _roleNameS = _roleName[0];
//                }
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = OPERATOR_ROLE;
//                approval.itemid = "Operator Role";
//                approval.makerid = operatorId;
//                approval.operator_id = _operId[0];
//                approval.operator_role = _roleId[0];
//                approval.operator_role_name = _roleNameS;
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("operators.accesstype"))) {
            if (accessObj.editOperator == true) {
//                if (operatorType == REQUESTER) {

                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                        } else {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//
//                        }
            } else {

                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//                String[] _operId = (String[]) m.get("_oprid");
//                String[] _op_type = (String[]) m.get("_op_type");
//
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = OPERATOR_ACCESS_TYPE;
//                approval.itemid = "Operator Access Type";
//                approval.makerid = operatorId;
//                approval.operator_id = _operId[0];
//                approval.operator_type = _op_type[0];
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("operators.status"))) {
            String[] _operId = (String[]) m.get("_oprid");
            String[] _op_status = (String[]) m.get("_op_status");
            if (accessObj.editOperator == true) {
//                if (operatorType == REQUESTER) {

//                    if (authStatus == MAKERCHECKER_DISABLE) {
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//                    }
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                        } else {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;

            }
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = OPERATOR_STATUS;
//                approval.itemid = "Operator Status";
//                approval.makerid = operatorId;
//                approval.operator_id = _operId[0];
//                approval.operator_status = _op_status[0];
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
            // }
        }
        if (url.contains(navSettings.getProperty("operators.resendpassword"))) {

            if (accessObj.editOperator == true) {
//                if (operatorType == REQUESTER) {

//                    if (authStatus == MAKERCHECKER_DISABLE) {
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//                    }
//                    String[] _operId = (String[]) m.get("_oprid");
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                        } else {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//
//                        }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;

            }
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = OPERATOR_RESEND_PASSWORD;
//                approval.itemid = "Resend Passoword";
//                approval.makerid = operatorId;
//                approval.operator_id = _operId[0];
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res
//                        == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

//                }
//            } else {
//                aStatus.iStatus = error;
//                aStatus.strStatus = strError;
//                return aStatus;
//            }
        }
        if (url.contains(navSettings.getProperty("operators.sendrandompassword"))) {

            if (accessObj.editOperator == true) {
//                if (operatorType == REQUESTER) {

                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                        } else {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
            } else {

                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//                String[] _operId = (String[]) m.get("_oprid");
//                String[] operator_send_password = (String[]) m.get("_send");
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = OPERATOR_SEND_RANDOM_PASSWORD;
//                approval.itemid = "Send Random Password";
//                approval.makerid = operatorId;
//                approval.operator_id = _operId[0];
//                approval.operator_send_password = Boolean.parseBoolean(operator_send_password[0]);
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("operators.grantAccess"))) {

            if (accessObj.editOperator == true) {
//                if (operatorType == REQUESTER) {

                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                        } else {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//
//                        }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//                String[] _operId = (String[]) m.get("_oprAccessId");
//                String[] _accessStartDate = (String[]) m.get("_accessStartDate");
//                String[] _accessEndDate = (String[]) m.get("_accessEndDate");
//
//                String _accessStartDateS = null;
//                String _accessEndDateS = null;
//                if (_accessStartDate != null) {
//                    _accessStartDateS = _accessStartDate[0];
//                }
//                if (_accessEndDate != null) {
//                    _accessEndDateS = _accessEndDate[0];
//                }
//
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = OPERATOR_GRANT_ACCESS;
//                approval.itemid = "Grant Access";
//                approval.makerid = operatorId;
//                approval.operator_id = _operId[0];
//                approval.operator_access_startdate = _accessStartDateS;
//                approval.operator_access_enddate = _accessEndDateS;
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("operators.checkOperatorAudit"))) {
            if (accessObj.editOperator == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        //units
        if (url.contains(navSettings.getProperty("units.changeUnitsStatus"))) {

            if (accessObj.editUnits == true) {
//                if (operatorType == REQUESTER) {

                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                        } else {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//                String[] _unitId = (String[]) m.get("_unitId");
//                String[] _status = (String[]) m.get("_status");
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = UNIT_STATUS;
//                approval.itemid = "Change Units Status";
//                approval.makerid = operatorId;
//                approval.units_id = _unitId[0];
//                approval.units_status = _status[0];
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("units.editUnit"))) {
            if (accessObj.editUnits == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                        } else {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//                String[] _unit_name = (String[]) m.get("_unitNameE");
//                String[] _unitId = (String[]) m.get("_unitId");
//                String[] _oldunitNameE = (String[]) m.get("_oldunitNameE");
//                String[] _thresholdLimitO = (String[]) m.get("_thresholdLimitO");
//                String[] _thresholdLimit = (String[]) m.get("_thresholdLimitE");
//
//                String _unit_nameS = null;
//                String _oldunitNameES = null;
//                String _thresholdLimitOS = null;
//                String _thresholdLimitS = null;
//                if (_unit_name != null) {
//                    _unit_nameS = _unit_name[0];
//                }
//                if (_oldunitNameE != null) {
//                    _oldunitNameES = _oldunitNameE[0];
//                }
//                if (_thresholdLimitO != null) {
//                    _thresholdLimitOS = _thresholdLimitO[0];
//                }
//                if (_thresholdLimit != null) {
//                    _thresholdLimitS = _thresholdLimit[0];
//                }
//
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = UNIT_EDIT;
//                approval.itemid = "Edit Unit";
//                approval.makerid = operatorId;
//                approval.units_id = _unitId[0];
//                approval.units_name = _unit_nameS;
//                approval.units_name_old = _oldunitNameES;
//                approval.units_threshold_limit = _thresholdLimitS;
//                approval.units_threshold_limit_old = _thresholdLimitOS;
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("units.shiftUnitsOperators"))) {

            if (accessObj.editUnits == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                        } else {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//                String[] _unitId = (String[]) m.get("_unitIdS");
//                String[] _oldunitNameE = (String[]) m.get("_oldunitNameS");
//                String[] _unitS = (String[]) m.get("_unitS");
//
//                String _oldunitNameES = null;
//                String _unitSS = null;
//                if (_oldunitNameE != null) {
//                    _oldunitNameES = _oldunitNameE[0];
//                }
//                if (_unitS != null) {
//                    _unitSS = _unitS[0];
//                }
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = UNIT_SHIFT_OPERATORS;
//                approval.itemid = "Shift Operators Of Units";
//                approval.makerid = operatorId;
//                approval.units_id = _unitSS;
//                approval.units_id_old = _unitId[0];
//                approval.units_name_old = _oldunitNameES;
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("units.assignTokenToUnit"))) {
            String[] _unitId = (String[]) m.get("_unitIdA");
            String[] _oldunitNameE = (String[]) m.get("_oldunitNameA");
            if (accessObj.editUnits == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = UNIT_ASSIGN_TOKEN;
//                    approval.itemid = "Assign Token To Unit";
//                    approval.makerid = operatorId;
//                    approval.units_id = _unitId[0];
//                    approval.units_name = _oldunitNameE[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("units.shiftTokensInUnits"))) {

            if (accessObj.editUnits == true) {
//                if (operatorType == REQUESTER) {

                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                    String[] _unitId = (String[]) m.get("_unitIdT");
//                    String[] _oldunitNameE = (String[]) m.get("_oldunitNameT");
//                    String[] new_unitID = (String[]) m.get("_unitIdNameT");
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = UNIT_SHIFT_TOKEN_IN_UNIT;
//                    approval.itemid = "Unit Shift Tokens";
//                    approval.makerid = operatorId;
//                    approval.units_id_old = _unitId[0];
//                    approval.units_id = new_unitID[0];
//                    approval.units_name_old = _oldunitNameE[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }

        if (url.contains(navSettings.getProperty("units.removeUnit"))) {

            if (accessObj.deleteUnits == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                        } else {
//
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//
//                        }
            } else {

                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//                String[] new_unitID = (String[]) m.get("_tid");
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = UNIT_REMOVE;
//                approval.itemid = "Remove Unit";
//                approval.makerid = operatorId;
//                approval.units_id = new_unitID[0];
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("units.checkUnits"))) {
            if (accessObj.listUnits == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("units.addUnit"))) {
            if (accessObj.addUnits == true) {
//                if (operatorType == REQUESTER) {

                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                        } else {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//                String[] _unitName = (String[]) m.get("_unitName");
//                String[] _status = (String[]) m.get("_unitStatus");
//                String[] _thresholdLimit = (String[]) m.get("_thresholdLimit");
//
//                String _unitNameS = null;
//                String _thresholdLimitS = null;
//                if (_unitName != null) {
//                    _unitNameS = _unitName[0];
//                }
//                if (_thresholdLimit != null) {
//                    _thresholdLimitS = _thresholdLimit[0];
//                }
//
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = UNIT_ADD;
//                approval.itemid = "Add Unit";
//                approval.makerid = operatorId;
//                approval.units_status = _status[0];
//                approval.units_threshold_limit = _thresholdLimitS;
//                approval.units_name = _unitNameS;
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        //user Group
        if (url.contains(navSettings.getProperty("usergroup.addgroup"))) {
            if (accessObj.addUserGroup == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                        } else {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//                String[] _unitName = (String[]) m.get("_unitName");
//                String[] _unitStatus = (String[]) m.get("_unitStatus");
//                String[] _selectedType = (String[]) m.get("_selectedType");
//                String[] _CountryName = (String[]) m.get("_CountryName");
//                String[] _StateName = (String[]) m.get("_StateName");
//                String[] _cityName = (String[]) m.get("_cityName");
//                String[] _PinCode = (String[]) m.get("_PinCode");
//
//                String _unitNameS = null;
//                String _CountryNameS = null;
//                String _StateNameS = null;
//                String _cityNameS = null;
//                String _PinCodeS = null;
//
//                if (_unitName != null) {
//                    _unitNameS = _unitName[0];
//                }
//                if (_CountryName != null) {
//                    _CountryNameS = _CountryName[0];
//                }
//                if (_StateName != null) {
//                    _StateNameS = _StateName[0];
//                }
//                if (_cityName != null) {
//                    _cityNameS = _cityName[0];
//                }
//                if (_PinCode != null) {
//                    _PinCodeS = _PinCode[0];
//                }
//
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = USERGROUP_ADD;
//                approval.itemid = "Add User Group";
//                approval.makerid = operatorId;
//                approval.units_name = _unitNameS;
//                approval.units_status = _unitStatus[0];
//                approval.selectedType = _selectedType[0];
//                approval.CountryName = _CountryNameS;
//                approval.StateName = _StateNameS;
//                approval.cityName = _cityNameS;
//                approval.PinCode = _PinCodeS;
//
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("usergroup.status"))) {

            if (accessObj.editUserGroup == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                        } else {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//                String[] _status = (String[]) m.get("_status");
//                String[] _unitId = (String[]) m.get("_unitId");
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = USERGROUP_STATUS;
//                approval.itemid = "User Group Status";
//                approval.makerid = operatorId;
//                approval.units_id = _unitId[0];
//                approval.units_status = _status[0];
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("usergroup.shiftgroupusers"))) {

            if (accessObj.editUserGroup == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                        } else {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//                String[] _unitIdS = (String[]) m.get("_unitIdS");
//                String[] _oldGroupNameS = (String[]) m.get("_oldGroupNameS");
//                String[] _unitS = (String[]) m.get("_unitS");
//
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = USERGROUP_SHIFT_USERS;
//                approval.itemid = "Shift Group users";
//                approval.makerid = operatorId;
//                approval.units_id = _unitS[0];
//                approval.units_id_old = _unitIdS[0];
//                approval.units_name = _oldGroupNameS[0];
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }

        if (url.contains(navSettings.getProperty("usergroup.removegroup"))) {

            if (accessObj.removeUserGroup == true) {
//                if (operatorType == REQUESTER) {

                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                        } else {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//                String[] groupname = (String[]) m.get("groupname");
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = USERGROUP_DELETE;
//                approval.itemid = "User Group Remove";
//                approval.makerid = operatorId;
//                approval.units_name = groupname[0];
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//
//                }
//            } else {
//                aStatus.iStatus = error;
//                aStatus.strStatus = strError;
//                return aStatus;
//            }
        }
        if (url.contains(navSettings.getProperty("usergroup.editgroup"))) {

            if (accessObj.editUserGroup == true) {
                if (operatorType == REQUESTER) {
                    if (authStatus == MAKERCHECKER_DISABLE) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }

                    String[] _fromApprove = (String[]) m.get("_fromApprove");

                    if (_fromApprove != null) {
                        if (_fromApprove[0].equals("yes")) {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        } else {
                            aStatus.iStatus = success;
                            aStatus.strStatus = strSuccess;
                            return aStatus;
                        }
                    } else {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
//                String[] _unitName = (String[]) m.get("_unitNameE");
//                String[] _unitStatus = (String[]) m.get("_unitStatusE");
//                String[] _selectedType = (String[]) m.get("_selectedTypeE");
//                String[] _CountryName = (String[]) m.get("_CountryNameE");
//                String[] _StateName = (String[]) m.get("_StateName");
//                String[] _cityName = (String[]) m.get("_cityNameE");
//                String[] _PinCode = (String[]) m.get("_PinCodeE");
//                String _unitNameS = null;
//                String _CountryNameS = null;
//                String _StateNameS = null;
//                String _cityNameS = null;
//                String _PinCodeS = null;
//
//                if (_unitName != null) {
//                    _unitNameS = _unitName[0];
//                }
//                if (_CountryName != null) {
//                    _CountryNameS = _CountryName[0];
//                }
//                if (_StateName != null) {
//                    _StateNameS = _StateName[0];
//                }
//                if (_cityName != null) {
//                    _cityNameS = _cityName[0];
//                }
//                if (_PinCode != null) {
//                    _PinCodeS = _PinCode[0];
//                }
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = USERGROUP_EDIT;
//                approval.itemid = "User Group Edit";
//                approval.makerid = operatorId;
//                approval.units_name = _unitNameS;
//                approval.units_status = _unitStatus[0];
//                approval.selectedType = _selectedType[0];
//                approval.CountryName = _CountryNameS;
//                approval.StateName = _StateNameS;
//                approval.cityName = _cityNameS;
//                approval.PinCode = _PinCodeS;
//
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        if (url.contains(navSettings.getProperty("usergroup.makeitdefualt"))) {

            if (accessObj.editUserGroup == true) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;

            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }

        if (url.contains(navSettings.getProperty("users.changeusergroup"))) {
            if (accessObj.editUser == true) {
                String[] _groupId = (String[]) m.get("_groupidE");
                String[] _userid = (String[]) m.get("_userIdE");
                String[] _groupName = (String[]) m.get("_groupname");
                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }

                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                Session sTemplate = suTemplate.openSession();
                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                ApprovalSetting approval = new ApprovalSetting();
                approval.action = CHANGE_USERGROUP;
                approval.itemid = "Change User Group";
                approval.makerid = operatorId;
                approval.userId = _userid[0];
                approval.user_groupId = _groupId[0];
                approval.user_groupname = _groupName[0];
                Calendar pexpiredOn = Calendar.getInstance();
                pexpiredOn.setTime(new Date());
                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                Date dexpiredOn = pexpiredOn.getTime();
                Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.userId.equals(approvalSetting.userId))) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                
                
                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                if (res == 0) {
                    aStatus.iStatus = pending;
                    aStatus.strStatus = strPending;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }

        //templates
        if (url.contains(navSettings.getProperty("templates.TemplateEmailList")) || url.contains(navSettings.getProperty("templates.TemplateMobileList"))) {
            if (accessObj.listTemplate == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("templates.savetemplates"))) {

            if (accessObj.addTemplate == true) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                String[] _fromApprove = (String[]) m.get("_fromApprove");
//                if (_fromApprove != null) {
//                    if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                    } else {
//                        aStatus.iStatus = error;
//                        aStatus.strStatus = strError;
//                        return aStatus;
//                    }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//                String[] _types = (String[]) m.get("_types");
//                String _templatename[] = null;
//                String _templatebody[] = null;
//                String _templatesubject[] = null;
//                String _taggedWith[] = null;
//                String _templatevaribles[] = null;
//                int _templatetype = -1;
//                if (_types != null) {
//                    _templatetype = Integer.valueOf(_types[0]);
//                }
//                if (_templatetype == 1) {
//                    _templatename = (String[]) m.get("_templatename");
//                    _templatebody = (String[]) m.get("_templatebody");
//                    _templatesubject = (String[]) m.get("_templatesubject");
//                    _taggedWith = _templatename;
//                    _templatevaribles = (String[]) m.get("_templatevariables");
//                } else {
//                    _templatename = (String[]) m.get("_templatenameS");
//                    _templatebody = (String[]) m.get("emailCotents");
//                    _templatesubject = (String[]) m.get("_templatesubjectS");
//                    _taggedWith = _templatename;
//                    _templatevaribles = (String[]) m.get("_templatevariablesS");
//                }
//                String _templatenameS = null;
//                String _templatebodyS = null;
//                String _templatesubjectS = null;
//                String _taggedWithS = null;
//                String _templatevariblesS = null;
//                if (_templatename != null) {
//                    _templatenameS = _templatename[0];
//                }
//                if (_templatebody != null) {
//                    _templatebodyS = _templatebody[0];
//                }
//                if (_templatesubject != null) {
//                    _templatesubjectS = _templatesubject[0];
//                }
//                if (_taggedWith != null) {
//                    _taggedWithS = _taggedWith[0];
//                }
//                if (_templatevaribles != null) {
//                    _templatevariblesS = _templatevaribles[0];
//                }
//
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                if (_templatetype == 1) {
//                    approval.action = TEMPLATE_ADD_MOBILE;
//                } else {
//                    approval.action = TEMPLATE_ADD_EMAIL;
//                }
//                approval.itemid = "Add Template";
//                approval.makerid = operatorId;
//                approval.template_name = _templatenameS;
//                approval.template_body = _templatebodyS;
//                if (_templatesubject != null) {
//                    approval.template_subject = _templatesubjectS;
//                }
//                approval.template_tagged_With = _taggedWithS;
//                approval.template_varibles = _templatevariblesS;
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

//            } else {
//                aStatus.iStatus = error;
//                aStatus.strStatus = strError;
//                return aStatus;
//            }
        }
        if (url.contains(navSettings.getProperty("templates.TemplateEmailEdit"))) {
            if (accessObj.editTemplate == true) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                String[] _tid = (String[]) m.get("idEmailTemplateId");
//                String[] _templatename = (String[]) m.get("_templateE_name");
//                String[] _templatesubject = (String[]) m.get("_templateE_subject");
//                String[] _templatebody = (String[]) m.get("emailContents");
//                String[] _templateVariables = (String[]) m.get("_templateE_variable");
//                String _templatenameS = null;
//                String _templatebodyS = null;
//                String _templatesubjectS = null;
//                String _taggedWithS = null;
//                String _templatevariblesS = null;
//                if (_templatename != null) {
//                    _templatenameS = _templatename[0];
//                }
//                if (_templatebody != null) {
//                    _templatebodyS = _templatebody[0];
//                }
//                if (_templatesubject != null) {
//                    _templatesubjectS = _templatesubject[0];
//                }
//                if (_templateVariables != null) {
//                    _templatevariblesS = _templateVariables[0];
//                }
//                String[] _fromApprove = (String[]) m.get("_fromApprove");
//                if (_fromApprove != null) {
//                    if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                    } else {
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//
//                    }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = TEMPLATE_EDIT_EMAIL;
//                approval.itemid = "Edit Template";
//                approval.makerid = operatorId;
//                approval.template_id = _tid[0];
//                approval.template_name = _templatenameS;
//                approval.template_body = _templatebodyS;
//                approval.template_subject = _templatesubjectS;
//                approval.template_varibles = _templatevariblesS;
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

//            } else {
//                aStatus.iStatus = error;
//                aStatus.strStatus = strError;
//                return aStatus;
//            }
        }
        if (url.contains(navSettings.getProperty("templates.TemplateMobilEdit"))) {
            if (accessObj.editTemplate == true) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                String[] _fromApprove = (String[]) m.get("_fromApprove");
//                if (_fromApprove != null) {
//                    if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                    } else {
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//                    }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//                String[] _tid = (String[]) m.get("idMessageTemplateId");
//                String[] _templatename = (String[]) m.get("_templateM_name");
//                String[] _templatebody = (String[]) m.get("_templateM_body");
//                String[] _templateVariables = (String[]) m.get("_templateM_variable");
//                String _templatenameS = null;
//                String _templatebodyS = null;
//                String _templatesubjectS = null;
//                String _taggedWithS = null;
//                String _templatevariblesS = null;
//                if (_templatename != null) {
//                    _templatenameS = _templatename[0];
//                }
//                if (_templatebody != null) {
//                    _templatebodyS = _templatebody[0];
//                }
//                if (_templateVariables != null) {
//                    _templatevariblesS = _templateVariables[0];
//                }
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = TEMPLATE_EDIT_MOBILE;
//                approval.itemid = "Edit Template";
//                approval.makerid = operatorId;
//                approval.template_id = _tid[0];
//                approval.template_name = _templatenameS;
//                approval.template_body = _templatebodyS;
//                approval.template_varibles = _templatevariblesS;
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

//            } else {
//                aStatus.iStatus = error;
//                aStatus.strStatus = strError;
//                return aStatus;
//            }
        }
        if (url.contains(navSettings.getProperty("templates.changetemplatestatus"))) {
            if (accessObj.editTemplate == true) {
//                if (operatorType == REQUESTER) {

                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                        } else {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//                String[] _tid = (String[]) m.get("_templateid");
//                String[] _status = (String[]) m.get("_status");
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = TEMPLATE_STATUS;
//                approval.itemid = "Edit Template";
//                approval.makerid = operatorId;
//                approval.template_id = _tid[0];
//                approval.template_status = _status[0];
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//
//            }
        }
        if (url.contains(navSettings.getProperty("templates.removetemplate"))) {
            if (accessObj.removeTemplate == true) {
                //if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                        } else {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//                String[] _tid = (String[]) m.get("_tid");
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = TEMPLATE_DELETE;
//                approval.itemid = "Remove Template";
//                approval.makerid = operatorId;
//                approval.template_id = _tid[0];
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        //Users 
        if (url.contains(navSettings.getProperty("users.users"))) {
            if (accessObj.ListUsers == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("users.adduser"))) {
            if (accessObj.addUser == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }
                String[] _user_name = (String[]) m.get("_Name");
                String[] _user_email = (String[]) m.get("_Email");
                String[] _user_phone = (String[]) m.get("_Phone");
                String[] _user_group = (String[]) m.get("_groupid");

                String _user_nameS = null;
                String _user_emailS = null;
                String _user_phoneS = null;
                if (_user_name != null) {
                    _user_nameS = _user_name[0];
                }
                if (_user_email != null) {
                    _user_emailS = _user_email[0];
                }
                if (_user_phone != null) {
                    _user_phoneS = _user_phone[0];
                }
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                Session sTemplate = suTemplate.openSession();
                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                ApprovalSetting approval = new ApprovalSetting();
                approval.action = USER_ADD;
                approval.itemid = "Add User";
                approval.makerid = operatorId;
                approval.userName = _user_nameS;
                approval.userPhone = _user_phoneS;
                approval.userEmail = _user_emailS;
                approval.user_groupId = _user_group[0];
                Calendar pexpiredOn = Calendar.getInstance();
                pexpiredOn.setTime(new Date());
                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                Date dexpiredOn = pexpiredOn.getTime();
                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                if (res == 0) {
                    aStatus.iStatus = pending;
                    aStatus.strStatus = strPending;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }

        if (url.contains(navSettings.getProperty("users.adduser"))) {
            if (accessObj.addUser == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }
                String[] _user_name = (String[]) m.get("_Name");
                String[] _user_email = (String[]) m.get("_Email");
                String[] _user_phone = (String[]) m.get("_Phone");
                String[] _user_group = (String[]) m.get("_groupid");

                String _user_nameS = null;
                String _user_emailS = null;
                String _user_phoneS = null;
                if (_user_name != null) {
                    _user_nameS = _user_name[0];
                }
                if (_user_email != null) {
                    _user_emailS = _user_email[0];
                }
                if (_user_phone != null) {
                    _user_phoneS = _user_phone[0];
                }
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                Session sTemplate = suTemplate.openSession();
                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                ApprovalSetting approval = new ApprovalSetting();
                approval.action = USER_ADD;
                approval.itemid = "Add User";
                approval.makerid = operatorId;
                approval.userName = _user_nameS;
                approval.userPhone = _user_phoneS;
                approval.userEmail = _user_emailS;
                approval.user_groupId = _user_group[0];
                Calendar pexpiredOn = Calendar.getInstance();
                pexpiredOn.setTime(new Date());
                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                Date dexpiredOn = pexpiredOn.getTime();
                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                if (res == 0) {
                    aStatus.iStatus = pending;
                    aStatus.strStatus = strPending;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }

        if (url.contains("edituserdetails")) {
            if (accessObj.editUser == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }
                String[] _userIdE = (String[]) m.get("_userIdD");
                String[] _user_name = (String[]) m.get("_NameD");
                String[] _countryD = (String[]) m.get("_CountryD");
                String[] _LocationD = (String[]) m.get("_LocationD");
                String[] _StreetD = (String[]) m.get("_StreetD");
                String[] _DesignationD = (String[]) m.get("_DesignationD");
                String[] _OrganisationD = (String[]) m.get("_OrganisationD");
                String[] _OrganisationUnitD = (String[]) m.get("_OrganisationUnitD");
                String _user_nameS = null;
                if (_user_name != null) {
                    _user_nameS = _user_name[0];
                }

//                if (operatorType == REQUESTER) {
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                Session sTemplate = suTemplate.openSession();
                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                ApprovalSetting approval = new ApprovalSetting();
                approval.action = USER_CHANGEDETAILS;
                approval.itemid = "Edit User Details";
                approval.makerid = operatorId;
                approval.userId = _userIdE[0];
                approval.userName = _user_nameS;
                approval.userName = _user_name[0];
                approval.user_country = _countryD[0];
                approval.user_location = _LocationD[0];
                approval.user_street = _StreetD[0];
                approval.user_designation = _DesignationD[0];
                approval.user_organization = _OrganisationD[0];
                approval.user_organizationunit = _OrganisationUnitD[0];
                Calendar pexpiredOn = Calendar.getInstance();
                pexpiredOn.setTime(new Date());
                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                Date dexpiredOn = pexpiredOn.getTime();
                Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                   if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.userId.equals(approvalSetting.userId))) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                 int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                if (res == 0) {
                    aStatus.iStatus = pending;
                    aStatus.strStatus = strPending;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }
        if (url.contains(navSettings.getProperty("users.edituser"))) {
            if (accessObj.editUser == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }
                String[] _userIdE = (String[]) m.get("_userIdE");
                String[] _user_name = (String[]) m.get("_NameE");
                String[] _user_email = (String[]) m.get("_EmailE");
                String[] _user_phone = (String[]) m.get("_PhoneE");
                String[] _user_group = (String[]) m.get("_groupidE");
                String _user_nameS = null;
                String _user_emailS = null;
                String _user_phoneS = null;
                String _user_groupS = null;
                if (_user_name != null) {
                    _user_nameS = _user_name[0];
                }
                if (_user_email != null) {
                    _user_emailS = _user_email[0];
                }
                if (_user_phone != null) {
                    _user_phoneS = _user_phone[0];
                }
                if (_user_group != null) {
                    _user_groupS = _user_group[0];
                }
//                if (operatorType == REQUESTER) {
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                Session sTemplate = suTemplate.openSession();
                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                ApprovalSetting approval = new ApprovalSetting();
                approval.action = USER_EDIT;
                approval.itemid = "Edit User";
                approval.makerid = operatorId;
                approval.userId = _userIdE[0];
                approval.userName = _user_nameS;
                approval.userPhone = _user_phoneS;
                approval.userEmail = _user_emailS;
                approval.user_groupId = _user_groupS;
                Calendar pexpiredOn = Calendar.getInstance();
                pexpiredOn.setTime(new Date());
                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                Date dexpiredOn = pexpiredOn.getTime();
                Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.userId.equals(approvalSetting.userId))) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                if (res == 0) {
                    aStatus.iStatus = pending;
                    aStatus.strStatus = strPending;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("users.changeuserstatus"))) {
            if (accessObj.editUser == true) {
//                if (operatorType == REQUESTER) {

                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }
                String[] _userIdE = (String[]) m.get("_userid");
                String[] _status = (String[]) m.get("_status");
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                Session sTemplate = suTemplate.openSession();
                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                ApprovalSetting approval = new ApprovalSetting();
                approval.action = USER_STATUS;
                approval.itemid = "Change User Status";
                approval.makerid = operatorId;
                approval.userId = _userIdE[0];
                approval.userStatus = _status[0];
                Calendar pexpiredOn = Calendar.getInstance();
                pexpiredOn.setTime(new Date());
                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                Date dexpiredOn = pexpiredOn.getTime();
                Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.userId.equals(approvalSetting.userId)) && (approval.userStatus.equals(approvalSetting.userStatus))) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                if (res == 0) {
                    aStatus.iStatus = pending;
                    aStatus.strStatus = strPending;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        if (url.contains(navSettings.getProperty("imagesetting.editImageSettings"))) {
            if (accessObj.editImageConfigSettings == true) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;

            }

        }

        if (url.contains(navSettings.getProperty("imagesetting.editImageSettings"))) {
            if (accessObj.editImageManagement == true) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;

            }

        }

        if (url.contains(navSettings.getProperty("users.deleteuser"))) {
            if (accessObj.removeUser == true) {
//                if (authStatus == MAKERCHECKER_DISABLE) {

                // if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }
                String[] _userIdE = (String[]) m.get("_userid");
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                Session sTemplate = suTemplate.openSession();
                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                ApprovalSetting approval = new ApprovalSetting();
                approval.action = USER_DELETE;
                approval.itemid = "User Delete";
                approval.makerid = operatorId;
                approval.userId = _userIdE[0];
                Calendar pexpiredOn = Calendar.getInstance();
                pexpiredOn.setTime(new Date());
                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                Date dexpiredOn = pexpiredOn.getTime();
                Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                   if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.userId.equals(approvalSetting.userId))) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                
                
                
                
                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                if (res == 0) {
                    aStatus.iStatus = pending;
                    aStatus.strStatus = strPending;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        if (url.contains(navSettings.getProperty("users.unlockuserpassword"))) {
            if (accessObj.editUser == true) {

                //   if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }
                String[] _userIdE = (String[]) m.get("_userid");
                String[] _status = (String[]) m.get("_status");
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                Session sTemplate = suTemplate.openSession();
                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                ApprovalSetting approval = new ApprovalSetting();
                approval.action = USER_UNLOCK_PASSWORD;
                approval.itemid = "User Unlock password";
                approval.makerid = operatorId;
                approval.userId = _userIdE[0];
                approval.userStatus = _status[0];
                Calendar pexpiredOn = Calendar.getInstance();
                pexpiredOn.setTime(new Date());
                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                Date dexpiredOn = pexpiredOn.getTime();
                Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.userId.equals(approvalSetting.userId)) && (approval.userStatus.equals(approvalSetting.userStatus))) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                if (res == 0) {
                    aStatus.iStatus = pending;
                    aStatus.strStatus = strPending;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }
        if (url.contains(navSettings.getProperty("users.resendpassword"))) {
            if (accessObj.editUser == true) {
                //    if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }
                String[] _userIdE = (String[]) m.get("_userid");
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                Session sTemplate = suTemplate.openSession();
                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                ApprovalSetting approval = new ApprovalSetting();
                approval.action = USER_RESEND_PASSWORD;
                approval.itemid = "User Resend password";
                approval.makerid = operatorId;
                approval.userId = _userIdE[0];
                Calendar pexpiredOn = Calendar.getInstance();
                pexpiredOn.setTime(new Date());
                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                Date dexpiredOn = pexpiredOn.getTime();
                Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.userId.equals(approvalSetting.userId))) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                if (res == 0) {
                    aStatus.iStatus = pending;
                    aStatus.strStatus = strPending;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }
        if (url.contains(navSettings.getProperty("users.resetpassword"))) {
            if (accessObj.editUser == true) {
                //  if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }
                String[] _userIdE = (String[]) m.get("_userid");
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                Session sTemplate = suTemplate.openSession();
                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                ApprovalSetting approval = new ApprovalSetting();
                approval.action = USER_RESET_PASSWORD;
                approval.itemid = "User Reset password";
                approval.makerid = operatorId;
                approval.userId = _userIdE[0];
                Calendar pexpiredOn = Calendar.getInstance();
                pexpiredOn.setTime(new Date());
                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                Date dexpiredOn = pexpiredOn.getTime();
                Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.userId.equals(approvalSetting.userId))) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                
                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                if (res == 0) {
                    aStatus.iStatus = pending;
                    aStatus.strStatus = strPending;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }
        if (url.contains(navSettings.getProperty("users.setandresendpassword"))) {
            if (accessObj.editUser == true) {
                // if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }
                String[] _userIdE = (String[]) m.get("_userid");
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                Session sTemplate = suTemplate.openSession();
                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                ApprovalSetting approval = new ApprovalSetting();
                approval.action = USER_SET_SEND_PASSWORD;
                approval.itemid = "User Reset send password";
                approval.makerid = operatorId;
                approval.userId = _userIdE[0];
                Calendar pexpiredOn = Calendar.getInstance();
                pexpiredOn.setTime(new Date());
                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                Date dexpiredOn = pexpiredOn.getTime();
                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                if (res == 0) {
                    aStatus.iStatus = pending;
                    aStatus.strStatus = strPending;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }
            if (url.contains(navSettings.getProperty("users.eraseMobileTrustCredential"))) {
            if (accessObj.editUser == true) {
                //  if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }
                String[] _userIdE = (String[]) m.get("_userid");
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                Session sTemplate = suTemplate.openSession();
                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                ApprovalSetting approval = new ApprovalSetting();
                approval.action = USER_ERASE_MOBILE_TRUST;
                approval.itemid = "Erase Mobile Trust";
                approval.makerid = operatorId;
                approval.userId = _userIdE[0];
                Calendar pexpiredOn = Calendar.getInstance();
                pexpiredOn.setTime(new Date());
                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                Date dexpiredOn = pexpiredOn.getTime();
                Approvalsettings[] arrRequest = null;
                    Approvalsettings[] approvalobj = oUtil.getALLPendingRequest(config.getChannelId(), 2);
                    if(approvalobj != null){
                    for (int i = 0; i < approvalobj.length; i++) {
                        byte[] obje = approvalobj[i].getApprovalSettingEntry();
                        byte[] f = AxiomProtect.AccessDataBytes(obje);
                        ByteArrayInputStream bais = new ByteArrayInputStream(f);
                        Object object = oUtil.deserializeFromObject(bais);
                        ApprovalSetting approvalSetting = null;
                        if (object instanceof ApprovalSetting) {
                            approvalSetting = (ApprovalSetting) object;
                        }
                        if (approvalSetting.action == approvalSetting.action && (approvalSetting.itemid.equals(approval.itemid))
                                && (approval.userId.equals(approvalSetting.userId))) {
                            aStatus.iStatus = duplicate;
                            aStatus.strStatus = strDuplicate;
                            return aStatus;
                        }
                    }}
                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                if (res == 0) {
                    aStatus.iStatus = pending;
                    aStatus.strStatus = strPending;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
            }
        }

        //challenge response
        if (url.contains(navSettings.getProperty("challengeresponse.ChallengeResponse"))) {
            if (accessObj.listChallengeResponse == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("challengeresponse.changeChallengestatus"))) {
            if (accessObj.editChallengeResponse == true) {
                //  if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                String[] _fromApprove = (String[]) m.get("_fromApprove");
//                if (_fromApprove != null) {
//                    if (_fromApprove[0].equals("yes")) {
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//                    }
//                }
//                String[] _userid = (String[]) m.get("_userid");
//                String[] _status = (String[]) m.get("_status");
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = CHALLENGE_RESPONSE_STATUS;
//                approval.itemid = "Challenge Respose Status";
//                approval.makerid = operatorId;
//                approval.userId = _userid[0];
//                approval.userStatus = _status[0];
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }
        if (url.contains(navSettings.getProperty("challengeresponse.removeChallengeuser"))) {
            if (accessObj.removeChallengeResponse == true) {
                //  if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                  aStatus.iStatus = success;
//                    aStatus.strStatus = strSuccess;
//                    return aStatus;
                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }
                String[] _userid = (String[]) m.get("_userid");
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                Session sTemplate = suTemplate.openSession();
                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                ApprovalSetting approval = new ApprovalSetting();
                approval.action = CHALLENGE_RESPONSE_DELETE;
                approval.itemid = "Delete Challenge Respose";
                approval.makerid = operatorId;
                approval.userId = _userid[0];
                Calendar pexpiredOn = Calendar.getInstance();
                pexpiredOn.setTime(new Date());
                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                Date dexpiredOn = pexpiredOn.getTime();
                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                if (res == 0) {
                    aStatus.iStatus = pending;
                    aStatus.strStatus = strPending;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }
        if (url.contains(navSettings.getProperty("challengeresponse.addQuestions"))) {
            if (accessObj.removeChallengeResponse == true) {
                //  if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {

                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }
                String[] _question = (String[]) m.get("_question");
                String[] _ch_weightage = (String[]) m.get("_ch_weightage");
                String[] _ch_status = (String[]) m.get("_ch_status");
                String str_question = null;
                if (_question != null) {
                    str_question = _question[0];
                }
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                Session sTemplate = suTemplate.openSession();
                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                ApprovalSetting approval = new ApprovalSetting();
                approval.action = CHALLENGE_RESPONSE_ADDQUESTION;
                approval.itemid = "Challenge Respose Add Question";
                approval.makerid = operatorId;
                approval.challenge_question = str_question;
                approval.challenge_weightage = _ch_weightage[0];
                approval.status1 = _ch_status[0];
                Calendar pexpiredOn = Calendar.getInstance();
                pexpiredOn.setTime(new Date());
                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                Date dexpiredOn = pexpiredOn.getTime();
                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                if (res == 0) {
                    aStatus.iStatus = pending;
                    aStatus.strStatus = strPending;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }
        if (url.contains(navSettings.getProperty("challengeresponse.editQuestion"))) {
            if (accessObj.removeChallengeResponse == true) {
                //  if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                  aStatus.iStatus = success;
//                    aStatus.strStatus = strSuccess;
//                    return aStatus;
                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }
                String[] _eqid = (String[]) m.get("_eqid");
                String[] _question = (String[]) m.get("_equestion");
                String[] _ch_weightage = (String[]) m.get("_e_weightage");
                String[] _ch_status = (String[]) m.get("_e_status");
                String str_question = null;
                if (_question != null) {
                    str_question = _question[0];
                }
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                Session sTemplate = suTemplate.openSession();
                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                ApprovalSetting approval = new ApprovalSetting();
                approval.action = CHALLENGE_RESPONSE_EDITQUESTION;
                approval.itemid = "Challenge Respose Edit Question";
                approval.makerid = operatorId;
                approval.challenge_question = str_question;
                approval.challenge_weightage = _ch_weightage[0];
                approval.status1 = _ch_status[0];
                approval.challenge_questionId = _eqid[0];
                Calendar pexpiredOn = Calendar.getInstance();
                pexpiredOn.setTime(new Date());
                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                Date dexpiredOn = pexpiredOn.getTime();
                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                if (res == 0) {
                    aStatus.iStatus = pending;
                    aStatus.strStatus = strPending;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }
        //trusted device
        if (url.contains(navSettings.getProperty("trustdevice.trusteddevice"))) {
            if (accessObj.listTrustedDevice == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("trustdevice.changedevicestatus"))) {
            if (accessObj.editTrustedDevice == true) {
                //  if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                  aStatus.iStatus = success;
//                    aStatus.strStatus = strSuccess;
//                    return aStatus;
                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }
                String[] _userid = (String[]) m.get("_userid");
                String[] _status = (String[]) m.get("_status");
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                Session sTemplate = suTemplate.openSession();
                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                ApprovalSetting approval = new ApprovalSetting();
                approval.action = TRUSTED_DEVICE_STATUS;
                approval.itemid = "Trusted Device Status";
                approval.makerid = operatorId;
                approval.userId = _userid[0];
                approval.userStatus = _status[0];
                Calendar pexpiredOn = Calendar.getInstance();
                pexpiredOn.setTime(new Date());
                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                Date dexpiredOn = pexpiredOn.getTime();
                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                if (res == 0) {
                    aStatus.iStatus = pending;
                    aStatus.strStatus = strPending;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }
        if (url.contains(navSettings.getProperty("trustdevice.trusteddevicepasswordattempts"))) {
            if (accessObj.editTrustedDevice == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("trustdevice.removeTrustDevice"))) {
            if (accessObj.removeDestroyClients == true) {
                // if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                  aStatus.iStatus = success;
//                    aStatus.strStatus = strSuccess;
//                    return aStatus;
                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }
                String[] _userid = (String[]) m.get("_userid");
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                Session sTemplate = suTemplate.openSession();
                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                ApprovalSetting approval = new ApprovalSetting();
                approval.action = TRUSTED_DEVICE_DELETE;
                approval.itemid = "Trusted Device Delete";
                approval.makerid = operatorId;
                approval.userId = _userid[0];
                Calendar pexpiredOn = Calendar.getInstance();
                pexpiredOn.setTime(new Date());
                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                Date dexpiredOn = pexpiredOn.getTime();
                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                if (res == 0) {
                    aStatus.iStatus = pending;
                    aStatus.strStatus = strPending;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }

        //Geo managment
        if (url.contains(navSettings.getProperty("geolocation.geotrackingmain"))) {
            if (accessObj.listGeoLocation == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("geolocation.getgeotracking"))
                || url.contains(navSettings.getProperty("geolocation.GeoTracks"))) {
            if (accessObj.editGeoLocation == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("geolocation.editgeotracking"))) {
            if (accessObj.editGeoLocation == true) {
                // if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                  aStatus.iStatus = success;
//                    aStatus.strStatus = strSuccess;
//                    return aStatus;
                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }
                String[] _userID = (String[]) m.get("_userID");
                if (_userID == null) {
                    _userID = (String[]) m.get("_userID1");
                    if (_userID == null) {
                        _userID = (String[]) m.get("_ruserId");
                    }
                }
                String[] _homecountry = (String[]) m.get("_homeCountry");
                String[] _groupid = (String[]) m.get("_groupid");
                String[] _startdate = (String[]) m.get("_startdate");
                String[] _enddate = (String[]) m.get("_enddate");
                String[] _roamingID = (String[]) m.get("_eroaming");

                String _homecountryS = null;
                String _startdateS = null;
                String _enddateS = null;
                if (_homecountry != null) {
                    _homecountryS = _homecountry[0];
                }
                if (_startdate != null) {
                    _startdateS = _startdate[0];
                }
                if (_enddate != null) {
                    _enddateS = _enddate[0];
                }
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                Session sTemplate = suTemplate.openSession();
                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                ApprovalSetting approval = new ApprovalSetting();
                approval.action = GEOTRACKING_EDIT;
                approval.itemid = "Edit Geo Tracking";
                approval.makerid = operatorId;
                approval.userId = _userID[0];
                approval.geolocation_homecountry = _homecountryS;
                approval.geolocation_groupId = _groupid[0];
                approval.geolocation_startdate = _startdateS;
                approval.geolocation_enddate = _enddateS;
                approval.geolocation_roaming = _roamingID[0];
                Calendar pexpiredOn = Calendar.getInstance();
                pexpiredOn.setTime(new Date());
                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                Date dexpiredOn = pexpiredOn.getTime();
                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                if (res == 0) {
                    aStatus.iStatus = pending;
                    aStatus.strStatus = strPending;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

                // } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
            }
        }
        //system message
        if (url.contains(navSettings.getProperty("systemmessage.AddSystemMessage"))) {
            if (accessObj.addSystemMessage == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                  aStatus.iStatus = success;
//                    aStatus.strStatus = strSuccess;
//                    return aStatus;
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] messagebody = (String[]) m.get("_messagebody");
//                    String[] alertto = (String[]) m.get("_alertto");
//                    String[] status = (String[]) m.get("_status");
//                    String messagebodyS = null;
//                    String alerttoS = null;
//
//                    if (messagebody != null) {
//                        messagebodyS = messagebody[0];
//                    }
//                    if (alertto != null) {
//                        alerttoS = alertto[0];
//                    }
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = SYSTEMESSAGE_ADD;
//                    approval.itemid = "Add System Message";
//                    approval.makerid = operatorId;
//                    approval.systemmessage_messagebody = messagebodyS;
//                    approval.systemmessage_alertto = alerttoS;
//                    approval.systemmessage_status = status[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                //  if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
            // }
        }
        if (url.contains(navSettings.getProperty("systemmessage.ListSystemMessage"))) {
            if (accessObj.listSystemMessage == true) {

                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("systemmessage.Deletemessagesettingentry"))) {
            if (accessObj.removeSystemMessage == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                  aStatus.iStatus = success;
//                    aStatus.strStatus = strSuccess;
//                    return aStatus;
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _sid = (String[]) m.get("_sid");
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = SYSTEMESSAGE_DELETE;
//                    approval.itemid = "Delete System Message";
//                    approval.makerid = operatorId;
//                    approval.systemmessage_id = _sid[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("systemmessage.ChangesystemmessageStatus"))) {

            if (accessObj.editSystemMessage == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _sid = (String[]) m.get("_messageno");
//                    String[] _status = (String[]) m.get("_status");
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = SYSTEMESSAGE_STATUS;
//                    approval.itemid = "System Message Status Change";
//                    approval.makerid = operatorId;
//                    approval.systemmessage_id = _sid[0];
//                    approval.systemmessage_status = _status[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("systemmessage.EditsystemmessageSettings"))) {

            if (accessObj.editSystemMessage == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }

        //Image managment
        if (url.contains(navSettings.getProperty("imagesetting.AddImageManagement"))) {
            if (accessObj.addImageManagement == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("imagesetting.ListImageManagement"))) {
            if (accessObj.listImageManagement == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        if (url.contains(navSettings.getProperty("imagesetting.DeleteImageManagement"))) {
            if (accessObj.removeImageManagement == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] image_userid = (String[]) m.get("_userid");
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = IMAGE_DELETE;
//                    approval.itemid = "Remove Image";
//                    approval.makerid = operatorId;
//                    approval.userId = image_userid[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                // if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("imagesetting.ChangeImageManagement"))) {
            if (accessObj.editImageManagement == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                  aStatus.iStatus = success;
//                    aStatus.strStatus = strSuccess;
//                    return aStatus;
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] image_userid = (String[]) m.get("_userid");
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = IMAGE_UNLOCK;
//                    approval.itemid = "Unlock Image";
//                    approval.makerid = operatorId;
//                    approval.userId = image_userid[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        //SNMP managment
        if (url.contains(navSettings.getProperty("snmp.AddSnmpManagement"))) {
            if (accessObj.addSnmpManagement == true) {
                //  if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                  aStatus.iStatus = success;
//                    aStatus.strStatus = strSuccess;
//                    return aStatus;
                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }
                String[] _snmpip = (String[]) m.get("_snmpip");
                String[] _snmpport = (String[]) m.get("_snmpport");
                String[] _level = (String[]) m.get("_level");
                String[] _alive = (String[]) m.get("_alive");
                String[] _error = (String[]) m.get("_error");
                String[] _warning = (String[]) m.get("_warning");
                String[] _status = (String[]) m.get("_status");

                String _snmpipS = null;
                String _snmpportS = null;
                String _levelS = null;
                String _aliveS = null;
                String _errorS = null;
                String _warningS = null;

                if (_snmpip != null) {
                    _snmpipS = _snmpip[0];
                }
                if (_snmpport != null) {
                    _snmpportS = _snmpport[0];
                }
                if (_level != null) {
                    _levelS = _level[0];
                }
                if (_alive != null) {
                    _aliveS = _alive[0];
                }
                if (_error != null) {
                    _errorS = _error[0];
                }
                if (_warning != null) {
                    _warningS = _warning[0];
                }
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                Session sTemplate = suTemplate.openSession();
                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                ApprovalSetting approval = new ApprovalSetting();
                approval.action = SNMP_ADD;
                approval.itemid = "Add SNMP Setting";
                approval.makerid = operatorId;
                approval.snmp_alive = _aliveS;
                approval.snmp_error = _errorS;
                approval.ip = _snmpipS;
                approval.snmp_level = _levelS;
                approval.port = _snmpportS;
                approval.status1 = _status[0];
                approval.snmp_warning = _warningS;
                Calendar pexpiredOn = Calendar.getInstance();
                pexpiredOn.setTime(new Date());
                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                Date dexpiredOn = pexpiredOn.getTime();
                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                if (res == 0) {
                    aStatus.iStatus = pending;
                    aStatus.strStatus = strPending;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }
        if (url.contains(navSettings.getProperty("snmp.ListSnmpManagement"))) {
            if (accessObj.listSnmpManagement == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("snmp.DeleteSnmpManagement"))) {
            if (accessObj.removeSnmpManagement == true) {
                //  if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }
                String[] _sid = (String[]) m.get("_sid");
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                Session sTemplate = suTemplate.openSession();
                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                ApprovalSetting approval = new ApprovalSetting();
                approval.action = SNMP_DELETE;
                approval.itemid = "Remove SNMP Setting";
                approval.makerid = operatorId;
                approval.snmp_id = _sid[0];
                Calendar pexpiredOn = Calendar.getInstance();
                pexpiredOn.setTime(new Date());
                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                Date dexpiredOn = pexpiredOn.getTime();
                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                if (res == 0) {
                    aStatus.iStatus = pending;
                    aStatus.strStatus = strPending;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }

            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }
        if (url.contains(navSettings.getProperty("snmp.ChangeSnmpManagements"))) {
            if (accessObj.editSnmpManagement == true) {
                //  if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }
                String[] _snmpip = (String[]) m.get("_snmpipedit");
                String[] _snmpport = (String[]) m.get("_snmpportedit");
                String[] _level = (String[]) m.get("_leveledit");
                String[] _alive = (String[]) m.get("_aliveedit");
                String[] _error = (String[]) m.get("_erroredit");
                String[] _warning = (String[]) m.get("_warningedit");
                String[] _status = (String[]) m.get("_statusedit");

                String _snmpipS = null;
                String _snmpportS = null;
                String _levelS = null;
                String _aliveS = null;
                String _errorS = null;
                String _warningS = null;

                if (_snmpip != null) {
                    _snmpipS = _snmpip[0];
                }
                if (_snmpport != null) {
                    _snmpportS = _snmpport[0];
                }
                if (_level != null) {
                    _levelS = _level[0];
                }
                if (_alive != null) {
                    _aliveS = _alive[0];
                }
                if (_error != null) {
                    _errorS = _error[0];
                }
                if (_warning != null) {
                    _warningS = _warning[0];
                }
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                Session sTemplate = suTemplate.openSession();
                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                ApprovalSetting approval = new ApprovalSetting();
                approval.action = SNMP_EDIT;
                approval.itemid = "Edit SNMP Setting";
                approval.makerid = operatorId;
                approval.snmp_alive = _aliveS;
                approval.snmp_error = _errorS;
                approval.ip = _snmpipS;
                approval.snmp_level = _levelS;
                approval.port = _snmpportS;
                approval.status1 = _status[0];
                approval.snmp_warning = _warningS;
                Calendar pexpiredOn = Calendar.getInstance();
                pexpiredOn.setTime(new Date());
                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                Date dexpiredOn = pexpiredOn.getTime();
                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                if (res == 0) {
                    aStatus.iStatus = pending;
                    aStatus.strStatus = strPending;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }
        if (url.contains(navSettings.getProperty("snmp.ChangesnmpStatus"))) {
            if (accessObj.editSnmpManagement == true) {
                //    if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
                String[] _fromApprove = (String[]) m.get("_fromApprove");
                if (_fromApprove != null) {
                    if (_fromApprove[0].equals("yes")) {
                        aStatus.iStatus = success;
                        aStatus.strStatus = strSuccess;
                        return aStatus;
                    }
                }
                String[] _status = (String[]) m.get("_status");
                String[] snmp_name = (String[]) m.get("_name");
                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
                Session sTemplate = suTemplate.openSession();
                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
                ApprovalSetting approval = new ApprovalSetting();
                approval.action = SNMP_STATUS;
                approval.itemid = "SNMP Status";
                approval.makerid = operatorId;
                approval.status1 = _status[0];
                approval.snmp_name = snmp_name[0];
                Calendar pexpiredOn = Calendar.getInstance();
                pexpiredOn.setTime(new Date());
                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
                Date dexpiredOn = pexpiredOn.getTime();
                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
                if (res == 0) {
                    aStatus.iStatus = pending;
                    aStatus.strStatus = strPending;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }

        AXIOMStatus aStatus1 = CheckAcces1(m, url, operatorId, operatorType, reqExpiryDuration, authStatus, obj, navSettings, accessObj);
        if (aStatus1 != null) {
            return aStatus1;
        }
        return aStatus;
    }

    public AXIOMStatus CheckAcces1(Map m, String url, String operatorId, int operatorType,
            int reqExpiryDuration, int authStatus, Object obj, Properties navSettings, AccessMatrixSettings accessObj) {
//      AXIOMStatus aStatus = new AXIOMStatus();
        if (url.contains(navSettings.getProperty("webwatch.listwebwatchSettings"))) {
            if (accessObj.listWebWatch == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
//        if (url.contains(navSettings.getProperty("webwatch.addwebwatch"))) {
//            if (accessObj.addWebWatch == true) {
//                aStatus.iStatus = 0;
//                aStatus.strStatus = strSuccess;
//                return aStatus;
//            } else {
//                aStatus.iStatus = error;
//                aStatus.strStatus = strError;
//                return aStatus;
//            }
//        }

        if (url.contains(navSettings.getProperty("webwatch.AddMSettings"))) {
            if (accessObj.addWebWatch == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        if (url.contains(navSettings.getProperty("webwatch.savewebwatch"))) {
            if (accessObj.addWebWatch == true) {

                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            }

///                String[] _fromApprove = (String[]) m.get("_fromApprove");
            //                if (_fromApprove != null) {
            //                    if (_fromApprove[0].equals("yes")) {
            //                        aStatus.iStatus = success;
            //                        aStatus.strStatus = strSuccess;
            //                        return aStatus;
            //                    }
            //                }
            //                String[] name = (String[]) m.get("_name");
            //                String[] status = (String[]) m.get("_status");
            //                String[] edit = (String[]) m.get("_edit");
            //                String[] monitorType = (String[]) m.get("_selectedType");
            //                String[] frequency = (String[]) m.get("_frequency");
            //                String[] responseTime = (String[]) m.get("_timeout");
            //                String[] snmp = (String[]) m.get("_selectedSNMP");
            //                String[] monitors = (String[]) m.get("_servicesalert");
            //                String[] alert = (String[]) m.get("_selectedAlert");
            //                String[] _dropcount = (String[]) m.get("_dropcount");
            //                String[] report = (String[]) m.get("_selectedReoprt");
            //                String[] template = (String[]) m.get("_templateName");
            //                String[] iTemplateId = (String[]) m.get("_templateName");
            //                String[] webadd = (String[]) m.get("_webAddress");
            //                String[] _imagescript = (String[]) m.get("_imagescript");
            //                String[] dnshost = (String[]) m.get("_dnshost");
            //                String[] dnsport = (String[]) m.get("_dnsport");
            //                String[] dnsname = (String[]) m.get("_dnsname");
            //                String[] selectedlookup = (String[]) m.get("_selectedLookup");
            //                String[] linktodns = (String[]) m.get("linktodns");
            //                String[] porthost = (String[]) m.get("_Porthost");
            //                String[] portport = (String[]) m.get("_Portport");
            //                String[] portcommand = (String[]) m.get("_Portcommand");
            //                String[] Pinghost = (String[]) m.get("_Pinghost");
            //                String[] selectedprotocol = (String[]) m.get("_selectedprotocol");
            //                String[] sslHost = (String[]) m.get("_sslHost");
            //                String[] sslPort = (String[]) m.get("_sslPort");
            //                String[] linktossl = (String[]) m.get("linktossl");
            //                String[] notify = (String[]) m.get("_notify");
            //
            //                String nameS = null;
            //                String statusS = null;
            //                String editS = null;
            //                String monitorTypeS = null;
            //                String frequencyS = null;
            //                String responseTimeS = null;
            //                String snmpS = null;
            //                String monitorsS = null;
            //                String alertS = null;
            //                String _dropcountS = null;
            //                String reportS = null;
            //                String templateS = null;
            //                String iTemplateIdS = null;
            //                String webaddS = null;
            //                String _imagescriptS = null;
            //                String dnshostS = null;
            //                String dnsportS = null;
            //                String dnsnameS = null;
            //                String selectedlookupS = null;
            //                String linktodnsS = null;
            //                String porthostS = null;
            //                String portportS = null;
            //                String portcommandS = null;
            //                String PinghostS = null;
            //                String selectedprotocolS = null;
            //                String sslHostS = null;
            //                String sslPortS = null;
            //                String linktosslS = null;
            //                String notifyS = null;
            //
            //                if (name != null) {
            //                    nameS = name[0];
            //                }
            //                if (status != null) {
            //                    statusS = status[0];
            //                }
            //                if (edit != null) {
            //                    editS = edit[0];
            //                }
            //                if (monitorType != null) {
            //                    monitorTypeS = monitorType[0];
            //                }
            //                if (frequency != null) {
            //                    frequencyS = frequency[0];
            //                }
            //                if (responseTime != null) {
            //                    responseTimeS = responseTime[0];
            //                }
            //                if (snmp != null) {
            //                    snmpS = snmp[0];
            //                }
            //                if (monitors != null) {
            //                    monitorsS = monitors[0];
            //                }
            //                if (alert != null) {
            //                    alertS = alert[0];
            //                }
            //                if (_dropcount != null) {
            //                    _dropcountS = _dropcount[0];
            //                }
            //                if (report != null) {
            //                    reportS = report[0];
            //                }
            //                if (template != null) {
            //                    templateS = template[0];
            //                }
            //                if (iTemplateId != null) {
            //                    iTemplateIdS = iTemplateId[0];
            //                }
            //                if (webadd != null) {
            //                    webaddS = webadd[0];
            //                }
            //                if (_imagescript != null) {
            //                    _imagescriptS = _imagescript[0];
            //                }
            //                if (dnshost != null) {
            //                    dnshostS = dnshost[0];
            //                }
            //                if (dnsname != null) {
            //                    dnsnameS = dnsname[0];
            //                }
            //                if (selectedlookup != null) {
            //                    selectedlookupS = selectedlookup[0];
            //                }
            //                if (linktodns != null) {
            //                    linktodnsS = linktodns[0];
            //                }
            //                if (porthost != null) {
            //                    porthostS = porthost[0];
            //                }
            //                if (portport != null) {
            //                    portportS = portport[0];
            //                }
            //                if (portcommand != null) {
            //                    portcommandS = portcommand[0];
            //                }
            //                if (Pinghost != null) {
            //                    PinghostS = Pinghost[0];
            //                }
            //                if (selectedprotocol != null) {
            //                    selectedprotocolS = selectedprotocol[0];
            //                }
            //                if (sslHost != null) {
            //                    sslHostS = sslHost[0];
            //                }
            //                if (sslPort != null) {
            //                    sslPortS = sslPort[0];
            //                }
            //                if (linktossl != null) {
            //                    linktosslS = linktossl[0];
            //                }
            //                if (notify != null) {
            //                    notifyS = notify[0];
            //                }
            //                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
            //                Session sTemplate = suTemplate.openSession();
            //                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
            //                ApprovalSetting approval = new ApprovalSetting();
            //                approval.action = WEBWATCH_ADD;
            //                approval.itemid = "Add Web Watch";
            //                approval.makerid = operatorId;
            //                approval.name = nameS;
            //                approval.status = statusS;
            //                approval.edituser = editS;
            //                approval.type = monitorTypeS;
            //                approval.freequency = frequencyS;
            //                approval.responseTime = responseTimeS;
            //                approval.snmp_name = snmpS;
            //                approval.monitors = monitorsS;
            //                approval.alertmedia = alertS;
            //                approval.dropcount = _dropcountS;
            //                approval.reportType = reportS;
            //                approval.templateName = templateS;
            //                approval.template_id = iTemplateIdS;
            //                approval.webadd = webaddS;
            //                approval.imagescript = _imagescriptS;
            //                approval.dnshost = dnshostS;
            //                approval.port = dnsportS;
            //                approval.dnsname = dnsnameS;
            //                approval.selectedlookup = selectedlookupS;
            //                approval.linktodns = linktodnsS;
            //                approval.linktodns = linktodnsS;
            //                approval.porthost = porthostS;
            //                approval.portport = portportS;
            //                approval.portcommand = portcommandS;
            //                approval.Pinghost = PinghostS;
            //                approval.selectedprotocol = selectedprotocolS;
            //                approval.sslHost = sslHostS;
            //                approval.sslPort = sslPortS;
            //                approval.linktossl = linktosslS;
            //                approval.notify = notifyS;
            //                Calendar pexpiredOn = Calendar.getInstance();
            //                pexpiredOn.setTime(new Date());
            //                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
            //                Date dexpiredOn = pexpiredOn.getTime();
            //                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
            //                if (res == 0) {
            //                    aStatus.iStatus = pending;
            //                    aStatus.strStatus = strPending;
            //                    return aStatus;
            //                } else {
            //                    aStatus.iStatus = error;
            //                    aStatus.strStatus = strError;
            //                    return aStatus;
            //                }
//            else {
//                aStatus.iStatus = error;
//                aStatus.strStatus = strError;
//                return aStatus;
//            }
        }

        if (url.contains(navSettings.getProperty("webwatch.changestatus"))) {
            if (accessObj.editWebWatch == true) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("webwatch.deletesetting"))) {
            if (accessObj.editWebWatch == true) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                String[] _fromApprove = (String[]) m.get("_fromApprove");
//                if (_fromApprove != null) {
//                    if (_fromApprove[0].equals("yes")) {
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//                    }
//                }
//                String[] name = (String[]) m.get("moniotorId");
//                String nameS = null;
//                if (name != null) {
//                    nameS = name[0];
//                }
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = WEBWATCH_DELETE;
//                approval.itemid = "Delete Setting";
//                approval.makerid = operatorId;
//                approval.name = nameS;
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        //config
        //sms gateway
        if (url.contains(navSettings.getProperty("smsgateway.listSmsGateway"))) {
            if (accessObj.listSmsGateway == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        String[] _className = null;
        String[] _logConfirmation1 = null;
        String[] _ip = null;
        String[] _password = null;
        String[] _phoneNo = null;
        String[] _port = null;
        Object[] reserve1 = null;
        Object[] reserve2 = null;
        Object[] reserve3 = null;
        String[] _status1 = null;
        String[] _userId = null;
        String[] _autofailover = null;
        String[] _retrycount = null;
        String[] _retryduration = null;
        String[] _messgeLength = null;
        String _classNameS = null;
        String _ipS = null;
        String _passwordS = null;
        String _phoneNoS = null;
        String _portS = null;
        Object reserve1S = null;
        Object reserve2S = null;
        Object reserve3S = null;
        String _status1S = null;
        String _userIdS = null;
        String _autofailoverS = null;
        String _retrycountS = null;
        String _retrydurationS = null;
        String _messgeLengthS = null;
//        if (url.contains(navSettings.getProperty("smsgateway.addSmsGateway"))) {
//            if (accessObj.editSmsGateway == true) {
//                if (operatorType == REQUESTER) {
//                    if (authStatus == MAKERCHECKER_DISABLE) {
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//                    }
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//
//                    String[] _preference = (String[]) m.get("_perference");
//                    int _iPreference = Integer.parseInt(_preference[0]);
//                    String[] _type1 = (String[]) m.get("_type");
//                    int _type = Integer.parseInt(_type1[0]);
//                    if (_iPreference == PREFERENCE_ONE) {   //primary
//                        _className = (String[]) m.get("_className");
//                        _ip = (String[]) m.get("_ip");
//                        _password = (String[]) m.get("_password");
//                        _phoneNo = (String[]) m.get("_phoneNumber");
//                        _port = (String[]) m.get("_port");
//                        reserve1 = (String[]) m.get("_reserve1");
//                        reserve2 = (String[]) m.get("_reserve2");
//                        reserve3 = (String[]) m.get("_reserve3");
//                        _status1 = (String[]) m.get("_status");
//                        _userId = (String[]) m.get("_userId");
//                        _autofailover = (String[]) m.get("_autofailover");
//                        _retrycount = (String[]) m.get("_retries");
//                        _retryduration = (String[]) m.get("_retryduration");
//                        _messgeLength = (String[]) m.get("_messgeLength");
//
//                    } else if (_iPreference == PREFERENCE_TWO) {   //secondary
//                        //request parameter for mobile secondary
//                        _className = (String[]) m.get("_classNameS");
//                        _ip = (String[]) m.get("_ipS");
//                        _password = (String[]) m.get("_passwordS");
//                        _phoneNo = (String[]) m.get("_phoneNumberS");
//                        _port = (String[]) m.get("_portS");
//                        reserve1 = (String[]) m.get("_reserve1S");
//                        reserve2 = (String[]) m.get("_reserve2S");
//                        reserve3 = (String[]) m.get("_reserve3S");
//                        _status1 = (String[]) m.get("_statusS");
//                        _userId = (String[]) m.get("_userIdS");
//                        _retrycount = (String[]) m.get("_retriesS");
//                        _retryduration = (String[]) m.get("_retrydurationS");
//                        _messgeLength = (String[]) m.get("_messgeLengthS");
//
//                    }
//                    if (_className != null) {
//                        _classNameS = _className[0];
//                    }
//                    if (_ip != null) {
//                        _ipS = _ip[0];
//                    }
//                    if (_password != null) {
//                        _passwordS = _password[0];
//                    }
//                    if (_phoneNo != null) {
//                        _phoneNoS = _phoneNo[0];
//                    }
//                    if (_port != null) {
//                        _portS = _port[0];
//                    }
//                    if (reserve1 != null) {
//                        reserve1S = reserve1[0];
//                    }
//                    if (reserve2 != null) {
//                        reserve2S = reserve2[0];
//                    }
//                    if (reserve3 != null) {
//                        reserve3S = reserve3[0];
//                    }
//                    if (_status1 != null) {
//                        _status1S = _status1[0];
//                    }
//                    if (_userId != null) {
//                        _userIdS = _userId[0];
//                    }
//                    if (_autofailover != null) {
//                        _autofailoverS = _autofailover[0];
//                    }
//                    if (_retrycount != null) {
//                        _retrycountS = _retrycount[0];
//                    }
//                    if (_retryduration != null) {
//                        _retrydurationS = _retryduration[0];
//                    }
//                    if (_messgeLength != null) {
//                        _messgeLengthS = _messgeLength[0];
//                    }
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = SMS_ADD;
//                    approval.itemid = "Add SMS Setting";
//                    approval.makerid = operatorId;
//                    approval.className = _classNameS;
//                    approval.ip = _ipS;
//                    approval.password = _passwordS;
//                    approval.phoneNo = _phoneNoS;
//                    approval.port = _portS;
//                    approval.reserve1 = reserve1S;
//                    approval.reserve2 = reserve2S;
//                    approval.reserve3 = reserve3S;
//                    approval.status1 = _status1S;
//                    approval.userId = _userIdS;
//                    if (_autofailover != null) {
//                        approval.autofailover = _autofailoverS;
//                    }
//                    approval.retrycount = _retrycountS;
//                    approval.retryduration = _retrydurationS;
//                    approval.messgeLength = _messgeLengthS;
//                    approval.type = _type1[0];
//                    approval.prefernce = _iPreference;
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
//                        aStatus.iStatus = pending;
//                        aStatus.strStatus = strPending;
//                        return aStatus;
//                    } else {
//                        aStatus.iStatus = error;
//                        aStatus.strStatus = strError;
//                        return aStatus;
//                    }
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
//        }

        if (url.contains(navSettings.getProperty("smsgateway.removeSmsGateway"))) {
            if (accessObj.editSmsGateway == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("smsgateway.editSmsGateway"))) {
            if (accessObj.editSmsGateway == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("smsgateway.addSmsGateway"))) {
            if (accessObj.addSmsGateway == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        //ussd gateway
        if (url.contains(navSettings.getProperty("ussdgateway.listussdgateway"))) {
            if (accessObj.listUSSDGateway == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("ussdgateway.addussdGateway"))) {
            if (accessObj.editUSSDGateway == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _preference = (String[]) m.get("_perferenceUSSD");
//                    int _iPreference = Integer.parseInt(_preference[0]);
//                    String[] _type1 = (String[]) m.get("_typeUSSD");
//                    int _type = Integer.parseInt(_type1[0]);
//                    if (_iPreference == PREFERENCE_ONE) {   //primary
//                        _className = (String[]) m.get("_classNameUSSD");
//                        _ip = (String[]) m.get("_ipUSSD");
//                        _password = (String[]) m.get("_passwordUSSD");
//                        _phoneNo = (String[]) m.get("_phoneNumberUSSD");
//                        _port = (String[]) m.get("_portUSSD");
//                        reserve1 = (String[]) m.get("_reserve1USSD");
//                        reserve2 = (String[]) m.get("_reserve2USSD");
//                        reserve3 = (String[]) m.get("_reserve3USSD");
//                        _status1 = (String[]) m.get("_statusUSSD");
//                        _userId = (String[]) m.get("_userIdUSSD");
//                        _autofailover = (String[]) m.get("_autofailoverUSSD");
//                        _retrycount = (String[]) m.get("_retriesUSSD");
//                        _retryduration = (String[]) m.get("_retrydurationUSSD");
//                        _messgeLength = (String[]) m.get("_messgeLengthUSSD");
//
//                    } else if (_iPreference == PREFERENCE_TWO) {   //secondary
//                        //request parameter for mobile secondary
//                        _className = (String[]) m.get("_classNameUSSDS");
//                        _ip = (String[]) m.get("_ipUSSDS");
//                        _password = (String[]) m.get("_passwordUSSDS");
//                        _phoneNo = (String[]) m.get("_phoneNumberUSSDS");
//                        _port = (String[]) m.get("_portUSSDS");
//                        reserve1 = (String[]) m.get("_reserve1USSDS");
//                        reserve2 = (String[]) m.get("_reserve2USSDS");
//                        reserve3 = (String[]) m.get("_reserve3USSDS");
//                        _status1 = (String[]) m.get("_statusUSSDS");
//                        _userId = (String[]) m.get("_userIdUSSDS");
//                        _retrycount = (String[]) m.get("_retriesUSSDS");
//                        _retryduration = (String[]) m.get("_retrydurationUSSDS");
//                        _messgeLength = (String[]) m.get("_messgeLengthUSSDS");
//
//                    }
//                    if (_className != null) {
//                        _classNameS = _className[0];
//                    }
//                    if (_ip != null) {
//                        _ipS = _ip[0];
//                    }
//                    if (_password != null) {
//                        _passwordS = _password[0];
//                    }
//                    if (_phoneNo != null) {
//                        _phoneNoS = _phoneNo[0];
//                    }
//                    if (_port != null) {
//                        _portS = _port[0];
//                    }
//                    if (reserve1 != null) {
//                        reserve1S = reserve1[0];
//                    }
//                    if (reserve2 != null) {
//                        reserve2S = reserve2[0];
//                    }
//                    if (reserve3 != null) {
//                        reserve3S = reserve3[0];
//                    }
//                    if (_status1 != null) {
//                        _status1S = _status1[0];
//                    }
//                    if (_userId != null) {
//                        _userIdS = _userId[0];
//                    }
//                    if (_autofailover != null) {
//                        _autofailoverS = _autofailover[0];
//                    }
//                    if (_retrycount != null) {
//                        _retrycountS = _retrycount[0];
//                    }
//                    if (_retryduration != null) {
//                        _retrydurationS = _retryduration[0];
//                    }
//                    if (_messgeLength != null) {
//                        _messgeLengthS = _messgeLength[0];
//                    }
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = USSD_ADD;
//                    approval.itemid = "Add USSD Setting";
//                    approval.makerid = operatorId;
//                    approval.className = _classNameS;
//                    approval.ip = _ipS;
//                    approval.password = _passwordS;
//                    approval.phoneNo = _phoneNoS;
//                    approval.port = _portS;
//                    approval.reserve1 = reserve1S;
//                    approval.reserve2 = reserve2S;
//                    approval.reserve3 = reserve3S;
//                    approval.status1 = _status1S;
//                    approval.userId = _userId[0];
//                    if (_autofailover != null) {
//                        approval.autofailover = _autofailoverS;
//                    }
//                    approval.retrycount = _retrycountS;
//                    approval.retryduration = _retrydurationS;
//                    approval.messgeLength = _messgeLengthS;
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }

        if (url.contains(navSettings.getProperty("ussdgateway.removeussdGateway"))) {
            if (accessObj.removeUSSDGateway == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        //voice gateway
        if (url.contains(navSettings.getProperty("ivrgateway.listivrgateway"))) {
            if (accessObj.listVOICEGateway == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("ivrgateway.addivrGateway"))) {
            if (accessObj.editVOICEGateway == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _preference = (String[]) m.get("_perferenceIVR");
//                    int _iPreference = Integer.parseInt(_preference[0]);
//                    String[] _type1 = (String[]) m.get("_typeIVR");
//                    int _type = Integer.parseInt(_type1[0]);
//                    if (_iPreference == PREFERENCE_ONE) {   //primary
//                        _className = (String[]) m.get("_classNameIVR");
//                        _ip = (String[]) m.get("_ipIVR");
//                        _password = (String[]) m.get("_passwordIVR");
//                        _phoneNo = (String[]) m.get("_phoneNumberIVR");
//                        _port = (String[]) m.get("_portIVR");
//                        reserve1 = (String[]) m.get("_reserve1IVR");
//                        reserve2 = (String[]) m.get("_reserve2IVR");
//                        reserve3 = (String[]) m.get("_reserve3IVR");
//                        _status1 = (String[]) m.get("_statusIVR");
//                        _userId = (String[]) m.get("_userIdIVR");
//                        _autofailover = (String[]) m.get("_autofailoverIVR");
//                        _retrycount = (String[]) m.get("_retriesIVR");
//                        _retryduration = (String[]) m.get("_retrydurationIVR");
//                        _messgeLength = (String[]) m.get("_messgeLengthIVR");
//
//                    } else if (_iPreference == PREFERENCE_TWO) {   //secondary
//                        //request parameter for mobile secondary
//                        _className = (String[]) m.get("_classNameIVRS");
//                        _ip = (String[]) m.get("_ipIVRS");
//                        _password = (String[]) m.get("_passwordIVRS");
//                        _phoneNo = (String[]) m.get("_phoneNumberIVRS");
//                        _port = (String[]) m.get("_portIVRS");
//                        reserve1 = (String[]) m.get("_reserve1IVRS");
//                        reserve2 = (String[]) m.get("_reserve2IVRS");
//                        reserve3 = (String[]) m.get("_reserve3IVRS");
//                        _status1 = (String[]) m.get("_statusIVRS");
//                        _userId = (String[]) m.get("_userIdIVRS");
//                        _retrycount = (String[]) m.get("_retriesIVRS");
//                        _retryduration = (String[]) m.get("_retrydurationIVRS");
//                        _messgeLength = (String[]) m.get("_messgeLengthIVRS");
//
//                    }
//                    if (_className != null) {
//                        _classNameS = _className[0];
//                    }
//                    if (_ip != null) {
//                        _ipS = _ip[0];
//                    }
//                    if (_password != null) {
//                        _passwordS = _password[0];
//                    }
//                    if (_phoneNo != null) {
//                        _phoneNoS = _phoneNo[0];
//                    }
//                    if (_port != null) {
//                        _portS = _port[0];
//                    }
//                    if (reserve1 != null) {
//                        reserve1S = reserve1[0];
//                    }
//                    if (reserve2 != null) {
//                        reserve2S = reserve2[0];
//                    }
//                    if (reserve3 != null) {
//                        reserve3S = reserve3[0];
//                    }
//                    if (_status1 != null) {
//                        _status1S = _status1[0];
//                    }
//                    if (_userId != null) {
//                        _userIdS = _userId[0];
//                    }
//                    if (_autofailover != null) {
//                        _autofailoverS = _autofailover[0];
//                    }
//                    if (_retrycount != null) {
//                        _retrycountS = _retrycount[0];
//                    }
//                    if (_retryduration != null) {
//                        _retrydurationS = _retryduration[0];
//                    }
//                    if (_messgeLength != null) {
//                        _messgeLengthS = _messgeLength[0];
//                    }
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = VOICE_ADD;
//                    approval.itemid = "Add VOICE Setting";
//                    approval.makerid = operatorId;
//                    approval.className = _classNameS;
//                    approval.ip = _ipS;
//                    approval.password = _passwordS;
//                    approval.phoneNo = _phoneNoS;
//                    approval.port = _portS;
//                    approval.reserve1 = reserve1S;
//                    approval.reserve2 = reserve2S;
//                    approval.reserve3 = reserve3S;
//                    approval.status1 = _status1S;
//                    approval.userId = _userIdS;
//                    if (_autofailover != null) {
//                        approval.autofailover = _autofailoverS;
//                    }
//                    approval.retrycount = _retrycountS;
//                    approval.retryduration = _retrydurationS;
//                    approval.messgeLength = _messgeLengthS;
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("ivrgateway.removeivrGateway"))) {
            if (accessObj.removeVOICEGateway == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        //email gateway
        if (url.contains(navSettings.getProperty("emailgateway.listemailgateway"))) {
            if (accessObj.listEMAILGateway == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("emailgateway.addemailGateway"))) {

            if (accessObj.editEMAILGateway == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                  aStatus.iStatus = success;
//                    aStatus.strStatus = strSuccess;
//                    return aStatus;
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _preference = (String[]) m.get("_perferenceEMAIL");
//                    int _iPreference = Integer.parseInt(_preference[0]);
//                    String[] _type1 = (String[]) m.get("_typeEMAIL");
//                    int _type = Integer.parseInt(_type1[0]);
//                    if (_iPreference == PREFERENCE_ONE) {   //primary
//                        _className = (String[]) m.get("_classNameEMAIL");
//                        _ip = (String[]) m.get("_ipEMAIL");
//                        _password = (String[]) m.get("_passwordEMAIL");
//                        _phoneNo = (String[]) m.get("_phoneNumberEMAIL");
//                        _port = (String[]) m.get("_portEMAIL");
//                        reserve1 = (String[]) m.get("_reserve1EMAIL");
//                        reserve2 = (String[]) m.get("_reserve2EMAIL");
//                        reserve3 = (String[]) m.get("_reserve3EMAIL");
//                        _status1 = (String[]) m.get("_statusEMAIL");
//                        _userId = (String[]) m.get("_userIdEMAIL");
//                        _autofailover = (String[]) m.get("_autofailoverEMAIL");
//                        _retrycount = (String[]) m.get("_retriesEMAIL");
//                        _retryduration = (String[]) m.get("_retrydurationEMAIL");
//                        _messgeLength = (String[]) m.get("_messgeLengthEMAIL");
//
//                    } else if (_iPreference == PREFERENCE_TWO) {   //secondary
//                        //request parameter for mobile secondary
//                        _className = (String[]) m.get("_classNameEMAILS");
//                        _ip = (String[]) m.get("_ipEMAILS");
//                        _password = (String[]) m.get("_passwordEMAILS");
//                        _phoneNo = (String[]) m.get("_phoneNumberEMAILS");
//                        _port = (String[]) m.get("_portEMAILS");
//                        reserve1 = (String[]) m.get("_reserve1EMAILS");
//                        reserve2 = (String[]) m.get("_reserve2EMAILS");
//                        reserve3 = (String[]) m.get("_reserve3EMAILS");
//                        _status1 = (String[]) m.get("_statusEMAILS");
//                        _userId = (String[]) m.get("_userIdEMAILS");
//                        _retrycount = (String[]) m.get("_retriesEMAILS");
//                        _retryduration = (String[]) m.get("_retrydurationEMAILS");
//                        _messgeLength = (String[]) m.get("_messgeLengthEMAILS");
//
//                    }
//                    if (_className != null) {
//                        _classNameS = _className[0];
//                    }
//                    if (_ip != null) {
//                        _ipS = _ip[0];
//                    }
//                    if (_password != null) {
//                        _passwordS = _password[0];
//                    }
//                    if (_phoneNo != null) {
//                        _phoneNoS = _phoneNo[0];
//                    }
//                    if (_port != null) {
//                        _portS = _port[0];
//                    }
//                    if (reserve1 != null) {
//                        reserve1S = reserve1[0];
//                    }
//                    if (reserve2 != null) {
//                        reserve2S = reserve2[0];
//                    }
//                    if (reserve3 != null) {
//                        reserve3S = reserve3[0];
//                    }
//                    if (_status1 != null) {
//                        _status1S = _status1[0];
//                    }
//                    if (_userId != null) {
//                        _userIdS = _userId[0];
//                    }
//                    if (_autofailover != null) {
//                        _autofailoverS = _autofailover[0];
//                    }
//                    if (_retrycount != null) {
//                        _retrycountS = _retrycount[0];
//                    }
//                    if (_retryduration != null) {
//                        _retrydurationS = _retryduration[0];
//                    }
//                    if (_messgeLength != null) {
//                        _messgeLengthS = _messgeLength[0];
//                    }
////                if (operatorType == REQUESTER) {
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = EMAIL_ADD;
//                    approval.itemid = "ADD EMAIL Setting";
//                    approval.makerid = operatorId;
//                    approval.className = _classNameS;
//                    approval.ip = _ipS;
//                    approval.password = _passwordS;
//                    approval.phoneNo = _phoneNoS;
//                    approval.port = _portS;
//                    approval.reserve1 = reserve1S;
//                    approval.reserve2 = reserve2S;
//                    approval.reserve3 = reserve3S;
//                    approval.status1 = _status1S;
//                    approval.userId = _userIdS;
//                    if (_autofailover != null) {
//                        approval.autofailover = _autofailoverS;
//                    }
//                    approval.retrycount = _retrycountS;
//                    approval.retryduration = _retrydurationS;
//                    approval.messgeLength = _messgeLengthS;
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }

        if (url.contains(navSettings.getProperty("emailgateway.removeemailGateway"))) {
            if (accessObj.removeEMAILGateway == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        //Billing manager 
        if (url.contains(navSettings.getProperty("billingmanager.listBillingManager"))) {
            if (accessObj.listBillingManager == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("billingmanager.addBillingManager"))) {
            if (accessObj.editBillingManager == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _allowBillingManager = (String[]) m.get("_allowBillingManager");
//                    String[] _allowRepetationCharacterB = (String[]) m.get("_allowRepetationCharacterB");
//                    String[] _gatewayTypeB = (String[]) m.get("_gatewayTypeB");
//                    String[] _alertAttemptB = (String[]) m.get("_alertAttemptB");
//                    String[] _allowBillingManagerSWOTP = (String[]) m.get("_allowBillingManagerSWOTP");
//                    String[] _selectBillTypeSWOTP = (String[]) m.get("_selectBillTypeSWOTP");
//                    String[] _selectBillingDurationSWOTP = (String[]) m.get("_selectBillingDurationSWOTP");
//                    String[] _selectAlertBeforeSWOTP = (String[]) m.get("_selectAlertBeforeSWOTP");
//                    String[] _costSWOTP = (String[]) m.get("_costSWOTP");
//                    String[] _allowBillingManagerHWOTP = (String[]) m.get("_allowBillingManagerHWOTP");
//                    String[] _selectBillTypeHWOTP = (String[]) m.get("_selectBillTypeHWOTP");
//                    String[] _selectBillingDurationHWOTP = (String[]) m.get("_selectBillingDurationHWOTP");
//                    String[] _selectAlertBeforeHWOTP = (String[]) m.get("_selectAlertBeforeHWOTP");
//                    String[] _costHWOTP = (String[]) m.get("_costHWOTP");
//                    String[] _allowBillingManagerSWPKIOTP = (String[]) m.get("_allowBillingManagerSWPKIOTP");
//                    String[] _selectBillTypeSWPKIOTP = (String[]) m.get("_selectBillTypeSWPKIOTP");
//                    String[] _selectBillingDurationSWPKIOTP = (String[]) m.get("_selectBillingDurationSWPKIOTP");
//                    String[] _selectAlertBeforeSWPKIOTP = (String[]) m.get("_selectAlertBeforeSWPKIOTP");
//                    String[] _costSWPKIOTP = (String[]) m.get("_costSWPKIOTP");
//                    String[] _allowBillingManagerHWPKIOTP = (String[]) m.get("_allowBillingManagerHWPKIOTP");
//                    String[] _selectBillTypeHWPKIOTP = (String[]) m.get("_selectBillTypeHWPKIOTP");
//                    String[] _selectBillingDurationHWPKIOTP = (String[]) m.get("_selectBillingDurationHWPKIOTP");
//                    String[] _selectAlertBeforeHWPKIOTP = (String[]) m.get("_selectAlertBeforeHWPKIOTP");
//                    String[] _costHWPKIOTP = (String[]) m.get("_costHWPKIOTP");
//                    String[] _allowBillingManagerCertificate = (String[]) m.get("_allowBillingManagerCertificate");
//                    String[] _selectBillTypeCertificate = (String[]) m.get("_selectBillTypeCertificate");
//                    String[] _selectBillingDurationCertificate = (String[]) m.get("_selectBillingDurationCertificate");
//                    String[] _selectAlertBeforeCertificate = (String[]) m.get("_selectAlertBeforeCertificate");
//                    String[] _costCertificate = (String[]) m.get("_costCertificate");
//                    String[] _allowBillingManagerOOBOTP = (String[]) m.get("_allowBillingManagerOOBOTP");
//                    String[] _selectBillTypeOOBOTP = (String[]) m.get("_selectBillTypeOOBOTP");
//                    String[] _selectBillingDurationOOBOTP = (String[]) m.get("_selectBillingDurationOOBOTP");
//                    String[] _selectAlertBeforeOOBOTP = (String[]) m.get("_selectAlertBeforeOOBOTP");
//                    String[] _txPerDayOOBOTP = (String[]) m.get("_txPerDayOOBOTP");
//                    String[] _costOOBOTP = (String[]) m.get("_costOOBOTP");
//                    String[] _costperTxOOBOTP = (String[]) m.get("_costperTxOOBOTP");
//
//                    String _costSWOTPS = null;
//                    if (_costSWOTP != null) {
//                        _costSWOTPS = _costSWOTP[0];
//                    }
//                    String _costHWOTPS = null;
//                    if (_costHWOTP != null) {
//                        _costHWOTPS = _costHWOTP[0];
//                    }
//                    String _costSWPKIOTPS = null;
//                    if (_costSWPKIOTP != null) {
//                        _costSWPKIOTPS = _costSWPKIOTP[0];
//                    }
//                    String _costHWPKIOTPS = null;
//                    if (_costHWPKIOTP != null) {
//                        _costHWPKIOTPS = _costHWPKIOTP[0];
//                    }
//                    String _costCertificateS = null;
//                    if (_costCertificate != null) {
//                        _costCertificateS = _costCertificate[0];
//                    }
//                    String _costOOBOTPS = null;
//                    if (_costOOBOTP != null) {
//                        _costOOBOTPS = _costOOBOTP[0];
//                    }
//                    String _costperTxOOBOTPS = null;
//                    if (_costperTxOOBOTP != null) {
//                        _costperTxOOBOTPS = _costperTxOOBOTP[0];
//                    }
//
////                if (operatorType == REQUESTER) {
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = BILLING_MANAGER_ADD;
//                    approval.itemid = "Add Billing Manager Setting";
//                    approval.makerid = operatorId;
//                    approval._allowBillingManager = _allowBillingManager[0];
//                    approval._allowRepetationCharacterB = _allowRepetationCharacterB[0];
//                    approval._gatewayTypeB = _gatewayTypeB[0];
//                    approval._alertAttemptB = _alertAttemptB[0];
//                    approval._allowBillingManagerSWOTP = _allowBillingManagerSWOTP[0];
//                    approval._selectBillTypeSWOTP = _selectBillTypeSWOTP[0];
//                    approval._selectBillingDurationSWOTP = _selectBillingDurationSWOTP[0];
//                    approval._selectAlertBeforeSWOTP = _selectAlertBeforeSWOTP[0];
//                    approval._costSWOTP = _costSWOTPS;
//                    approval._allowBillingManagerHWOTP = _allowBillingManagerHWOTP[0];
//                    approval._selectBillTypeHWOTP = _selectBillTypeHWOTP[0];
//                    approval._selectBillingDurationHWOTP = _selectBillingDurationHWOTP[0];
//                    approval._selectAlertBeforeHWOTP = _selectAlertBeforeHWOTP[0];
//                    approval._costHWOTP = _costHWOTPS;
//                    approval._allowBillingManagerSWPKIOTP = _allowBillingManagerSWPKIOTP[0];
//                    approval._selectBillTypeSWPKIOTP = _selectBillTypeSWPKIOTP[0];
//                    approval._selectBillingDurationSWPKIOTP = _selectBillingDurationSWPKIOTP[0];
//                    approval._selectAlertBeforeSWPKIOTP = _selectAlertBeforeSWPKIOTP[0];
//                    approval._costSWPKIOTP = _costSWPKIOTPS;
//                    approval._allowBillingManagerHWPKIOTP = _allowBillingManagerHWPKIOTP[0];
//                    approval._selectBillTypeHWPKIOTP = _selectBillTypeHWPKIOTP[0];
//                    approval._selectBillingDurationHWPKIOTP = _selectBillingDurationHWPKIOTP[0];
//                    approval._selectAlertBeforeHWPKIOTP = _selectAlertBeforeHWPKIOTP[0];
//                    approval._costHWPKIOTP = _costHWPKIOTPS;
//                    approval._allowBillingManagerCertificate = _allowBillingManagerCertificate[0];
//                    approval._selectBillTypeCertificate = _selectBillTypeCertificate[0];
//                    approval._selectBillingDurationCertificate = _selectBillingDurationCertificate[0];
//                    approval._selectAlertBeforeCertificate = _selectAlertBeforeCertificate[0];
//                    approval._costCertificate = _costCertificateS;
//                    approval._allowBillingManagerOOBOTP = _allowBillingManagerOOBOTP[0];
//                    approval._selectBillTypeOOBOTP = _selectBillTypeOOBOTP[0];
//                    approval._selectBillingDurationOOBOTP = _selectBillingDurationOOBOTP[0];
//                    approval._selectAlertBeforeOOBOTP = _selectAlertBeforeOOBOTP[0];
//                    approval._txPerDayOOBOTP = _txPerDayOOBOTP[0];
//                    approval._costOOBOTP = _costOOBOTPS;
//                    approval._costperTxOOBOTP = _costperTxOOBOTPS;
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("billingmanager.editBillingManager"))) {
            if (accessObj.editBillingManager == true) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                String[] _fromApprove = (String[]) m.get("_fromApprove");
//                if (_fromApprove != null) {
//                    if (_fromApprove[0].equals("yes")) {
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//                    }
//                }
//                String[] _allowBillingManager = (String[]) m.get("_allowBillingManager");
//                String[] _allowRepetationCharacterB = (String[]) m.get("_allowRepetationCharacterB");
//                String[] _gatewayTypeB = (String[]) m.get("_gatewayTypeB");
//                String[] _alertAttemptB = (String[]) m.get("_alertAttemptB");
//                String[] _allowBillingManagerSWOTP = (String[]) m.get("_allowBillingManagerSWOTP");
//                String[] _selectBillTypeSWOTP = (String[]) m.get("_selectBillTypeSWOTP");
//                String[] _selectBillingDurationSWOTP = (String[]) m.get("_selectBillingDurationSWOTP");
//                String[] _selectAlertBeforeSWOTP = (String[]) m.get("_selectAlertBeforeSWOTP");
//                String[] _costSWOTP = (String[]) m.get("_costSWOTP");
//                String[] _allowBillingManagerHWOTP = (String[]) m.get("_allowBillingManagerHWOTP");
//                String[] _selectBillTypeHWOTP = (String[]) m.get("_selectBillTypeHWOTP");
//                String[] _selectBillingDurationHWOTP = (String[]) m.get("_selectBillingDurationHWOTP");
//                String[] _selectAlertBeforeHWOTP = (String[]) m.get("_selectAlertBeforeHWOTP");
//                String[] _costHWOTP = (String[]) m.get("_costHWOTP");
//                String[] _allowBillingManagerSWPKIOTP = (String[]) m.get("_allowBillingManagerSWPKIOTP");
//                String[] _selectBillTypeSWPKIOTP = (String[]) m.get("_selectBillTypeSWPKIOTP");
//                String[] _selectBillingDurationSWPKIOTP = (String[]) m.get("_selectBillingDurationSWPKIOTP");
//                String[] _selectAlertBeforeSWPKIOTP = (String[]) m.get("_selectAlertBeforeSWPKIOTP");
//                String[] _costSWPKIOTP = (String[]) m.get("_costSWPKIOTP");
//                String[] _allowBillingManagerHWPKIOTP = (String[]) m.get("_allowBillingManagerHWPKIOTP");
//                String[] _selectBillTypeHWPKIOTP = (String[]) m.get("_selectBillTypeHWPKIOTP");
//                String[] _selectBillingDurationHWPKIOTP = (String[]) m.get("_selectBillingDurationHWPKIOTP");
//                String[] _selectAlertBeforeHWPKIOTP = (String[]) m.get("_selectAlertBeforeHWPKIOTP");
//                String[] _costHWPKIOTP = (String[]) m.get("_costHWPKIOTP");
//                String[] _allowBillingManagerCertificate = (String[]) m.get("_allowBillingManagerCertificate");
//                String[] _selectBillTypeCertificate = (String[]) m.get("_selectBillTypeCertificate");
//                String[] _selectBillingDurationCertificate = (String[]) m.get("_selectBillingDurationCertificate");
//                String[] _selectAlertBeforeCertificate = (String[]) m.get("_selectAlertBeforeCertificate");
//                String[] _costCertificate = (String[]) m.get("_costCertificate");
//                String[] _allowBillingManagerOOBOTP = (String[]) m.get("_allowBillingManagerOOBOTP");
//                String[] _selectBillTypeOOBOTP = (String[]) m.get("_selectBillTypeOOBOTP");
//                String[] _selectBillingDurationOOBOTP = (String[]) m.get("_selectBillingDurationOOBOTP");
//                String[] _selectAlertBeforeOOBOTP = (String[]) m.get("_selectAlertBeforeOOBOTP");
//                String[] _txPerDayOOBOTP = (String[]) m.get("_txPerDayOOBOTP");
//                String[] _costOOBOTP = (String[]) m.get("_costOOBOTP");
//                String[] _costperTxOOBOTP = (String[]) m.get("_costperTxOOBOTP");
//
////                if (operatorType == REQUESTER) {
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = EDIT_EMAIL;
//                approval.itemid = "EDIT Billing Manager Setting";
//                approval.makerid = operatorId;
//                approval._allowBillingManager = _allowBillingManager[0];
//                approval._allowRepetationCharacterB = _allowRepetationCharacterB[0];
//                approval._gatewayTypeB = _gatewayTypeB[0];
//                approval._alertAttemptB = _alertAttemptB[0];
//                approval._allowBillingManagerSWOTP = _allowBillingManagerSWOTP[0];
//                approval._selectBillTypeSWOTP = _selectBillTypeSWOTP[0];
//                approval._selectBillingDurationSWOTP = _selectBillingDurationSWOTP[0];
//                approval._selectAlertBeforeSWOTP = _selectAlertBeforeSWOTP[0];
//                approval._costSWOTP = _costSWOTP[0];
//                approval._allowBillingManagerHWOTP = _allowBillingManagerHWOTP[0];
//                approval._selectBillTypeHWOTP = _selectBillTypeHWOTP[0];
//                approval._selectBillingDurationHWOTP = _selectBillingDurationHWOTP[0];
//                approval._selectAlertBeforeHWOTP = _selectAlertBeforeHWOTP[0];
//                approval._costHWOTP = _costHWOTP[0];
//                approval._allowBillingManagerSWPKIOTP = _allowBillingManagerSWPKIOTP[0];
//                approval._selectBillTypeSWPKIOTP = _selectBillTypeSWPKIOTP[0];
//                approval._selectBillingDurationSWPKIOTP = _selectBillingDurationSWPKIOTP[0];
//                approval._selectAlertBeforeSWPKIOTP = _selectAlertBeforeSWPKIOTP[0];
//                approval._costSWPKIOTP = _costSWPKIOTP[0];
//                approval._allowBillingManagerHWPKIOTP = _allowBillingManagerHWPKIOTP[0];
//                approval._selectBillTypeHWPKIOTP = _selectBillTypeHWPKIOTP[0];
//                approval._selectBillingDurationHWPKIOTP = _selectBillingDurationHWPKIOTP[0];
//                approval._selectAlertBeforeHWPKIOTP = _selectAlertBeforeHWPKIOTP[0];
//                approval._costHWPKIOTP = _costHWPKIOTP[0];
//                approval._allowBillingManagerCertificate = _allowBillingManagerCertificate[0];
//                approval._selectBillTypeCertificate = _selectBillTypeCertificate[0];
//                approval._selectBillingDurationCertificate = _selectBillingDurationCertificate[0];
//                approval._selectAlertBeforeCertificate = _selectAlertBeforeCertificate[0];
//                approval._costCertificate = _costCertificate[0];
//                approval._allowBillingManagerOOBOTP = _allowBillingManagerOOBOTP[0];
//                approval._selectBillTypeOOBOTP = _selectBillTypeOOBOTP[0];
//                approval._selectBillingDurationOOBOTP = _selectBillingDurationOOBOTP[0];
//                approval._selectAlertBeforeOOBOTP = _selectAlertBeforeOOBOTP[0];
//                approval._txPerDayOOBOTP = _txPerDayOOBOTP[0];
//                approval._costOOBOTP = _costOOBOTP[0];
//                approval._costperTxOOBOTP = _costperTxOOBOTP[0];
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("billingmanager.removeBillingManager"))) {
            if (accessObj.removeBillingManager == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        //Push Notification 
        if (url.contains(navSettings.getProperty("pushgateway.listpushgateway"))) {
            if (accessObj.listPushGateway == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        if (url.contains(navSettings.getProperty("pushgateway.addandroidpushgateway"))) {
            if (accessObj.editPushGateway == true) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                String[] _fromApprove = (String[]) m.get("_fromApprove");
//                if (_fromApprove != null) {
//                    if (_fromApprove[0].equals("yes")) {
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//                    }
//                }
//                _className = (String[]) m.get("_className");
//                _ip = (String[]) m.get("_ip");
//                _logConfirmation1 = (String[]) m.get("_logConfirmation");
//                _port = (String[]) m.get("_port");
//                reserve1 = (String[]) m.get("_reserve1");
//                reserve2 = (String[]) m.get("_reserve2");
//                reserve3 = (String[]) m.get("_reserve3");
//                _status1 = (String[]) m.get("_status");
//                _autofailover = (String[]) m.get("_autofailover");
//                _retrycount = (String[]) m.get("_retries");
//                _retryduration = (String[]) m.get("_retryduration");
//                String[] _retriesFromGoogle = (String[]) m.get("_retriesFromGoogle");
//                String[] _timetolive = (String[]) m.get("_timetolive");
//                String[] _delayWhileIdle = (String[]) m.get("_delayWhileIdle");
//                String[] _gcmurl = (String[]) m.get("_gcmurl");
//                String[] _apikey = (String[]) m.get("_apikey");
//                String[] _googleSenderKey = (String[]) m.get("_googleSenderKey");
//
//                if (_className != null) {
//                    _classNameS = _className[0];
//                }
//                if (_ip != null) {
//                    _ipS = _ip[0];
//                }
//                if (_port != null) {
//                    _portS = _port[0];
//                }
//                if (reserve1 != null) {
//                    reserve1S = reserve1[0];
//                }
//                if (reserve2 != null) {
//                    reserve2S = reserve2[0];
//                }
//                if (reserve3 != null) {
//                    reserve3S = reserve3[0];
//                }
//                if (_status1 != null) {
//                    _status1S = _status1[0];
//                }
//                if (_autofailover != null) {
//                    _autofailoverS = _autofailover[0];
//                }
//                if (_retrycount != null) {
//                    _retrycountS = _retrycount[0];
//                }
//                if (_retryduration != null) {
//                    _retrydurationS = _retryduration[0];
//                }
//                String _logConfirmation1S = null;
//                if (_logConfirmation1 != null) {
//                    _logConfirmation1S = _logConfirmation1[0];
//                }
//                String _retriesFromGoogleS = null;
//                if (_retriesFromGoogle != null) {
//                    _retriesFromGoogleS = _retriesFromGoogle[0];
//                }
//                String _timetoliveS = null;
//                if (_timetolive != null) {
//                    _timetoliveS = _timetolive[0];
//                }
//                String _delayWhileIdleS = null;
//                if (_delayWhileIdle != null) {
//                    _delayWhileIdleS = _delayWhileIdle[0];
//                }
//                String _gcmurlS = null;
//                if (_gcmurl != null) {
//                    _gcmurlS = _gcmurl[0];
//                }
//                String _apikeyS = null;
//                if (_apikey != null) {
//                    _apikeyS = _apikey[0];
//                }
//                String _googleSenderKeyS = null;
//                if (_googleSenderKey != null) {
//                    _googleSenderKeyS = _googleSenderKey[0];
//                }
////                if (operatorType == REQUESTER) {
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = ADD_ANDROID_PUSH_SETTING;
//                approval.itemid = "ADD Android Push Setting";
//                approval.makerid = operatorId;
//                approval.className = _classNameS;
//                approval.ip = _ipS;
//                approval.logConfirmation1 = _logConfirmation1S;
//                approval.port = _portS;
//                approval.reserve1 = reserve1S;
//                approval.reserve2 = reserve2S;
//                approval.reserve3 = reserve3S;
//                approval.status1 = _status1S;
//                approval.autofailover = _autofailoverS;
//                approval.retrycount = _retrycountS;
//                approval.retryduration = _retrydurationS;
//                approval.retriesFromGoogle = _retriesFromGoogleS;
//                approval.timetolive = _timetoliveS;
//                approval.delayWhileIdle = _delayWhileIdleS;
//                approval.gcmurl = _gcmurlS;
//                approval.apikey = _apikeyS;
//                approval.googleSenderKey = _googleSenderKeyS;
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("pushgateway.addiphonepushgateway"))) {

            if (accessObj.editPushGateway == true) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                String[] _fromApprove = (String[]) m.get("_fromApprove");
//                if (_fromApprove != null) {
//                    if (_fromApprove[0].equals("yes")) {
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//                    }
//                }
//                _className = (String[]) m.get("_classNameS");
//                _ip = (String[]) m.get("_ipS");
//                _logConfirmation1 = (String[]) m.get("_logConfirmationS");
//                _port = (String[]) m.get("_portS");
//                reserve1 = (String[]) m.get("_reserve1S");
//                reserve2 = (String[]) m.get("_reserve2S");
//                reserve3 = (String[]) m.get("_reserve3S");
//                _status1 = (String[]) m.get("_statusS");
//                _retrycount = (String[]) m.get("_retriesS");
//                _retryduration = (String[]) m.get("_retrydurationS");
//                String[] _bundleID = (String[]) m.get("_bundleID");
//                String[] _certpassowrd = (String[]) m.get("_password");
//                String[] _applicationType = (String[]) m.get("_delayWhileIdleS");
//                if (_className != null) {
//                    _classNameS = _className[0];
//                }
//                if (_ip != null) {
//                    _ipS = _ip[0];
//                }
//                if (_port != null) {
//                    _portS = _port[0];
//                }
//                if (reserve1 != null) {
//                    reserve1S = reserve1[0];
//                }
//                if (reserve2 != null) {
//                    reserve2S = reserve2[0];
//                }
//                if (reserve3 != null) {
//                    reserve3S = reserve3[0];
//                }
//                if (_status1 != null) {
//                    _status1S = _status1[0];
//                }
//                if (_retrycount != null) {
//                    _retrycountS = _retrycount[0];
//                }
//                if (_retryduration != null) {
//                    _retrydurationS = _retryduration[0];
//                }
//                String _logConfirmation1S = null;
//                if (_logConfirmation1 != null) {
//                    _logConfirmation1S = _logConfirmation1[0];
//                }
//                String _bundleIDS = null;
//                if (_bundleID != null) {
//                    _bundleIDS = _bundleID[0];
//                }
//                String _certpassowrdS = null;
//                if (_certpassowrd != null) {
//                    _certpassowrdS = _certpassowrd[0];
//                }
//                String _applicationTypeS = null;
//                if (_applicationType != null) {
//                    _applicationTypeS = _applicationType[0];
//                }
//
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = ADD_IPHONE_PUSH_SETTING;
//                approval.itemid = "ADD IPHONe Push Setting";
//                approval.makerid = operatorId;
//                approval.className = _classNameS;
//                approval.ip = _ipS;
//                approval.logConfirmation1 = _logConfirmation1S;
//                approval.port = _portS;
//                approval.reserve1 = reserve1S;
//                approval.reserve2 = reserve2S;
//                approval.reserve3 = reserve3S;
//                approval.status1 = _status1S;
//                approval.retrycount = _retrycountS;
//                approval.retryduration = _retrydurationS;
//                approval.bundleID = _bundleIDS;
//                approval.certpassowrd = _certpassowrdS;
//                approval.applicationType = _applicationTypeS;
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }

            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        if (url.contains(navSettings.getProperty("pushgateway.removeandroidpushgateway"))
                || url.contains(navSettings.getProperty("pushgateway.removeiphonepushgateway"))) {
            if (accessObj.removePushGateway == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        //User Source
        if (url.contains(navSettings.getProperty("usersource.listusersource"))) {
            if (accessObj.listUserSourceSettings == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("usersource.addusersource"))) {

            if (accessObj.addUserSourceSettings == true) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;

//                aStatus.iStatus = success;
//                aStatus.strStatus = strSuccess;
//                return aStatus;
//                String[] _fromApprove = (String[]) m.get("_fromApprove");
//                if (_fromApprove != null) {
//                    if (_fromApprove[0].equals("yes")) {
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//                    }
//                }
//                String[] _statusUS = (String[]) m.get("_statusUS");
//                String[] _ipUS = (String[]) m.get("_ipUS");
//                String[] _portUS = (String[]) m.get("_portUS");
//                String[] _userIdUS = (String[]) m.get("_userIdUS");
//                String[] _passwordUS = (String[]) m.get("_passwordUS");
//                String[] _classNameUS = (String[]) m.get("_classNameUS");
//                String[] _sslUS = (String[]) m.get("_sslUS");
//                String[] _reserve1US = (String[]) m.get("_reserve1US");
//                String[] _reserve2US = (String[]) m.get("_reserve2US");
//                String[] _reserve3US = (String[]) m.get("_reserve3US");
//                String[] _databaseNameUS = (String[]) m.get("_databaseNameUS");
//                String[] _tableNameUS = (String[]) m.get("_tableNameUS");
//                String[] _algoTypeUS = (String[]) m.get("_algoTypeUS");
//                String[] _passwordTypeUS = (String[]) m.get("_passwordTypeUS");
//                String[] _algoAttrReserve1US = (String[]) m.get("_algoAttrReserve1US");
//                String[] _algoAttrReserve2US = (String[]) m.get("_algoAttrReserve2US");
//                String[] _algoAttrReserve3US = (String[]) m.get("_algoAttrReserve3US");
//                String[] _algoAttrReserve4US = (String[]) m.get("_algoAttrReserve4US");
//                String[] _userValidityDays = (String[]) m.get("_userValidityDays");
//
//                String _statusUSS = null;
//                if (_statusUS != null) {
//                    _statusUSS = _statusUS[0];
//                }
//                String _ipUSS = null;
//                if (_ipUS != null) {
//                    _ipUSS = _ipUS[0];
//                }
//                String _portUSS = null;
//                if (_portUS != null) {
//                    _portUSS = _portUS[0];
//                }
//                String _userIdUSS = null;
//                if (_userIdUS != null) {
//                    _userIdUSS = _userIdUS[0];
//                }
//                String _passwordUSS = null;
//                if (_passwordUS != null) {
//                    _passwordUSS = _passwordUS[0];
//                }
//                String _classNameUSS = null;
//                if (_classNameUS != null) {
//                    _classNameUSS = _classNameUS[0];
//                }
//                String _sslUSS = null;
//                if (_sslUS != null) {
//                    _sslUSS = _sslUS[0];
//                }
//                String _reserve1USS = null;
//                if (_reserve1US != null) {
//                    _reserve1USS = _reserve1US[0];
//                }
//                String _reserve2USS = null;
//                if (_reserve2US != null) {
//                    _reserve2USS = _reserve2US[0];
//                }
//                String _reserve3USS = null;
//                if (_reserve3US != null) {
//                    _reserve3USS = _reserve3US[0];
//                }
//                String _databaseNameUSS = null;
//                if (_databaseNameUS != null) {
//                    _databaseNameUSS = _databaseNameUS[0];
//                }
//                String _tableNameUSS = null;
//                if (_tableNameUS != null) {
//                    _tableNameUSS = _tableNameUS[0];
//                }
//                String _algoTypeUSS = null;
//                if (_algoTypeUS != null) {
//                    _algoTypeUSS = _algoTypeUS[0];
//                }
//                String _passwordTypeUSS = null;
//                if (_passwordTypeUS != null) {
//                    _passwordTypeUSS = _passwordTypeUS[0];
//                }
//                String _algoAttrReserve1USS = null;
//                if (_algoAttrReserve1US != null) {
//                    _algoAttrReserve1USS = _algoAttrReserve1US[0];
//                }
//                String _algoAttrReserve2USS = null;
//                if (_algoAttrReserve2US != null) {
//                    _algoAttrReserve2USS = _algoAttrReserve2US[0];
//                }
//                String _algoAttrReserve3USS = null;
//                if (_algoAttrReserve3US != null) {
//                    _algoAttrReserve3USS = _algoAttrReserve3US[0];
//                }
//                String _algoAttrReserve4USS = null;
//                if (_algoAttrReserve4US != null) {
//                    _algoAttrReserve4USS = _algoAttrReserve4US[0];
//                }
//                String _userValidityDaysS = null;
//                if (_userValidityDays != null) {
//                    _userValidityDaysS = _userValidityDays[0];
//                }
//
////                if (operatorType == REQUESTER) {
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = ADD_USER_SOURCE_SETTING;
//                approval.itemid = "Add user source Setting";
//                approval.makerid = operatorId;
//                approval.ip = _ipUSS;
//                approval.status1 = _statusUSS;
//                approval.port = _portUSS;
//                approval.userId = _userIdUSS;
//                approval.className = _classNameUSS;
//                approval.ssl = _sslUSS;
//                approval.reserve1 = _reserve1USS;
//                approval.reserve2 = _reserve2USS;
//                approval.reserve3 = _reserve3USS;
//                approval.database_name = _databaseNameUSS;
//                approval.table_name = _tableNameUSS;
//                approval.algoTypeUS = _algoTypeUSS;
//                approval.passwordTypeUS = _passwordTypeUSS;
//                approval.password = _passwordUSS;
//                approval.algoAttrReserve1US = _algoAttrReserve1USS;
//                approval.algoAttrReserve2US = _algoAttrReserve2USS;
//                approval.algoAttrReserve3US = _algoAttrReserve3USS;
//                approval.algoAttrReserve4US = _algoAttrReserve4USS;
//                approval.userValidityDays = _userValidityDaysS;
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }
        if (url.contains(navSettings.getProperty("usersource.editusersource"))) {

            if (accessObj.editUserSourceSettings == true) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;

//                String[] _fromApprove = (String[]) m.get("_fromApprove");
//                if (_fromApprove != null) {
//                    if (_fromApprove[0].equals("yes")) {
//                        aStatus.iStatus = success;
//                        aStatus.strStatus = strSuccess;
//                        return aStatus;
//                    }
//                }
//                String[] _statusUS = (String[]) m.get("_statusUS");
//                String[] _ipUS = (String[]) m.get("_ipUS");
//                String[] _portUS = (String[]) m.get("_portUS");
//                String[] _userIdUS = (String[]) m.get("_userIdUS");
//                String[] _passwordUS = (String[]) m.get("_passwordUS");
//                String[] _classNameUS = (String[]) m.get("_classNameUS");
//                String[] _sslUS = (String[]) m.get("_sslUS");
//                String[] _reserve1US = (String[]) m.get("_reserve1US");
//                String[] _reserve2US = (String[]) m.get("_reserve2US");
//                String[] _reserve3US = (String[]) m.get("_reserve3US");
//                String[] _databaseNameUS = (String[]) m.get("_databaseNameUS");
//                String[] _tableNameUS = (String[]) m.get("_tableNameUS");
//                String[] _algoTypeUS = (String[]) m.get("_algoTypeUS");
//                String[] _passwordTypeUS = (String[]) m.get("_passwordTypeUS");
//                String[] _algoAttrReserve1US = (String[]) m.get("_algoAttrReserve1US");
//                String[] _algoAttrReserve2US = (String[]) m.get("_algoAttrReserve2US");
//                String[] _algoAttrReserve3US = (String[]) m.get("_algoAttrReserve3US");
//                String[] _algoAttrReserve4US = (String[]) m.get("_algoAttrReserve4US");
//                String[] _userValidityDays = (String[]) m.get("_userValidityDays");
//
////                if (operatorType == REQUESTER) {
//                SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                Session sTemplate = suTemplate.openSession();
//                AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                ApprovalSetting approval = new ApprovalSetting();
//                approval.action = EDIT_USER_SOURCE_SETTING;
//                approval.itemid = "EDIT user source Setting";
//                approval.makerid = operatorId;
//                approval.ip = _ipUS[0];
//                approval.status1 = _statusUS[0];
//                approval.port = _portUS[0];
//                approval.userId = _userIdUS[0];
//                approval.className = _classNameUS[0];
//                approval.ssl = _sslUS[0];
//                approval.reserve1 = _reserve1US[0];
//                approval.reserve2 = _reserve2US[0];
//                approval.reserve3 = _reserve3US[0];
//                approval.database_name = _databaseNameUS[0];
//                approval.table_name = _tableNameUS[0];
//                approval.algoTypeUS = _algoTypeUS[0];
//                approval.passwordTypeUS = _passwordTypeUS[0];
//                approval.algoAttrReserve1US = _algoAttrReserve1US[0];
//                approval.algoAttrReserve2US = _algoAttrReserve2US[0];
//                approval.algoAttrReserve3US = _algoAttrReserve3US[0];
//                approval.algoAttrReserve4US = _algoAttrReserve4US[0];
//                approval.userValidityDays = _userValidityDays[0];
//                Calendar pexpiredOn = Calendar.getInstance();
//                pexpiredOn.setTime(new Date());
//                pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                Date dexpiredOn = pexpiredOn.getTime();
//                int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                if (res == 0) {
//                    aStatus.iStatus = pending;
//                    aStatus.strStatus = strPending;
//                    return aStatus;
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("usersource.removeusersource"))) {
            if (accessObj.removeUserSourceSettings == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("usersource.removeusersource"))) {
            if (accessObj.editUserSourceSettings == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        //Password Policy settings
        if (url.contains(navSettings.getProperty("passwordpolicy.listpasswordpolicy"))) {
            if (accessObj.listPasswordPolicySettings == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("passwordpolicy.addpasswordpolicy"))) {

            if (accessObj.editPasswordPolicySettings == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _passwordLength = (String[]) m.get("_passwordLength");
//                    String[] _passwordExpiryTime = (String[]) m.get("_passwordExpiryTime");
//                    String[] _passwordIssueLimit = (String[]) m.get("_passwordIssueLimit");
//                    String[] _enforceDormancyTime = (String[]) m.get("_enforceDormancyTime");
//                    String[] _passwordInvalidAttempts = (String[]) m.get("_passwordInvalidAttempts");
//                    String[] _oldPasswordReuseTime = (String[]) m.get("_oldPasswordReuseTime");
//                    String[] _changePasswordAfterLogin = (String[]) m.get("_changePasswordAfterLogin");
//                    String[] _epinResetReissue = (String[]) m.get("_epinResetReissue");
//                    String[] _crResetReissue = (String[]) m.get("_crResetReissue");
//                    String[] _passwordType = (String[]) m.get("_passwordType");
//                    String[] _allowRepetationCharacter = (String[]) m.get("_allowRepetationCharacter");
//                    String[] _genearationPassword = (String[]) m.get("_genearationPassword");
//                    String[] _alertUserPassword = (String[]) m.get("_alertUserPassword");
//                    String[] _blockCommonWords = (String[]) m.get("_blockCommonWords");
//                    String[] _allowAlert = (String[]) m.get("_allowAlert");
//                    String[] _gatewayType = (String[]) m.get("_gatewayType");
//                    String[] _alertAttempt = (String[]) m.get("_alertAttempt");
//                    String[] _templateName = (String[]) m.get("_templateName");
//                    String[] _arrCommonPassword = (String[]) m.get("_arrCommonPassword");
//                    String[] _allowAlertFor = (String[]) m.get("_allowAlertFor");
//                    String[] _userValidityDays = (String[]) m.get("_userValidityDays");
//
//                    String _passwordLengthS = null;
//                    if (_passwordLength != null) {
//                        _passwordLengthS = _passwordLength[0];
//                    }
//                    String _passwordExpiryTimeS = null;
//                    if (_passwordExpiryTime != null) {
//                        _passwordExpiryTimeS = _passwordExpiryTime[0];
//                    }
//                    String _passwordIssueLimitS = null;
//                    if (_passwordIssueLimit != null) {
//                        _passwordIssueLimitS = _passwordIssueLimit[0];
//                    }
//                    String _enforceDormancyTimeS = null;
//                    if (_enforceDormancyTime != null) {
//                        _enforceDormancyTimeS = _enforceDormancyTime[0];
//                    }
//                    String _passwordInvalidAttemptsS = null;
//                    if (_passwordInvalidAttempts != null) {
//                        _passwordInvalidAttemptsS = _passwordInvalidAttempts[0];
//                    }
//                    String _oldPasswordReuseTimeS = null;
//                    if (_oldPasswordReuseTime != null) {
//                        _oldPasswordReuseTimeS = _oldPasswordReuseTime[0];
//                    }
//                    String _changePasswordAfterLoginS = null;
//                    if (_changePasswordAfterLogin != null) {
//                        _changePasswordAfterLoginS = _changePasswordAfterLogin[0];
//                    }
//                    String _epinResetReissueS = null;
//                    if (_epinResetReissue != null) {
//                        _epinResetReissueS = _epinResetReissue[0];
//                    }
//                    String _crResetReissueS = null;
//                    if (_crResetReissue != null) {
//                        _crResetReissueS = _crResetReissue[0];
//                    }
//                    String _passwordTypeS = null;
//                    if (_passwordType != null) {
//                        _passwordTypeS = _passwordType[0];
//                    }
//                    String _allowRepetationCharacterS = null;
//                    if (_allowRepetationCharacter != null) {
//                        _allowRepetationCharacterS = _allowRepetationCharacter[0];
//                    }
//                    String _genearationPasswordS = null;
//                    if (_genearationPassword != null) {
//                        _genearationPasswordS = _genearationPassword[0];
//                    }
//                    String _alertUserPasswordS = null;
//                    if (_alertUserPassword != null) {
//                        _alertUserPasswordS = _alertUserPassword[0];
//                    }
//                    String _blockCommonWordsS = null;
//                    if (_blockCommonWords != null) {
//                        _blockCommonWordsS = _blockCommonWords[0];
//                    }
//                    String _allowAlertS = null;
//                    if (_allowAlert != null) {
//                        _allowAlertS = _allowAlert[0];
//                    }
//                    String _gatewayTypeS = null;
//                    if (_gatewayType != null) {
//                        _gatewayTypeS = _gatewayType[0];
//                    }
//                    String _alertAttemptS = null;
//                    if (_alertAttempt != null) {
//                        _alertAttemptS = _alertAttempt[0];
//                    }
//                    String _templateNameS = null;
//                    if (_templateName != null) {
//                        _templateNameS = _templateName[0];
//                    }
//                    String _arrCommonPasswordS = null;
//                    if (_arrCommonPassword != null) {
//                        _arrCommonPasswordS = _arrCommonPassword[0];
//                    }
//                    String _allowAlertForS = null;
//                    if (_allowAlertFor != null) {
//                        _allowAlertForS = _allowAlertFor[0];
//                    }
//                    String _userValidityDaysS = null;
//                    if (_userValidityDays != null) {
//                        _userValidityDaysS = _userValidityDays[0];
//                    }
////                if (operatorType == REQUESTER) {
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = ADD_PASSWORD_POLICY_SETTING;
//                    approval.itemid = "Add password policy Setting";
//                    approval.makerid = operatorId;
//                    approval.passwordLength = _passwordLengthS;
//                    approval.passwordExpiryTime = _passwordExpiryTimeS;
//                    approval.passwordIssueLimit = _passwordIssueLimitS;
//                    approval.enforceDormancyTime = _enforceDormancyTimeS;
//                    approval.passwordInvalidAttempts = _passwordInvalidAttemptsS;
//                    approval.oldPasswordReuseTime = _oldPasswordReuseTimeS;
//                    approval.changePasswordAfterLogin = _changePasswordAfterLoginS;
//                    approval.epinResetReissue = _epinResetReissueS;
//                    approval.crResetReissue = _crResetReissueS;
//                    approval.passwordType = _passwordTypeS;
//                    approval.allowRepetationCharacter = _allowRepetationCharacterS;
//                    approval.genearationPassword = _genearationPasswordS;
//                    approval.alertUserPassword = _alertUserPasswordS;
//                    approval.blockCommonWords = _blockCommonWordsS;
//                    approval.allowAlert = _allowAlertS;
//                    approval.gatewayType = _gatewayTypeS;
//                    approval.alertAttempt = _alertAttemptS;
//                    approval.templateName = _templateNameS;
//                    approval.arrCommonPassword = _arrCommonPasswordS;
//                    approval.allowAlertFor = _allowAlertForS;
//                    approval.userValidityDays = _userValidityDaysS;
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("passwordpolicy.editpasswordpolicy"))) {

            if (accessObj.editPasswordPolicySettings == true) {
//                if (operatorType == REQUESTER) {

                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _passwordLength = (String[]) m.get("_passwordLength");
//                    String[] _passwordExpiryTime = (String[]) m.get("_passwordExpiryTime");
//                    String[] _passwordIssueLimit = (String[]) m.get("_passwordIssueLimit");
//                    String[] _enforceDormancyTime = (String[]) m.get("_enforceDormancyTime");
//                    String[] _passwordInvalidAttempts = (String[]) m.get("_passwordInvalidAttempts");
//                    String[] _oldPasswordReuseTime = (String[]) m.get("_oldPasswordReuseTime");
//                    String[] _changePasswordAfterLogin = (String[]) m.get("_changePasswordAfterLogin");
//                    String[] _epinResetReissue = (String[]) m.get("_epinResetReissue");
//                    String[] _crResetReissue = (String[]) m.get("_crResetReissue");
//                    String[] _passwordType = (String[]) m.get("_passwordType");
//                    String[] _allowRepetationCharacter = (String[]) m.get("_allowRepetationCharacter");
//                    String[] _genearationPassword = (String[]) m.get("_genearationPassword");
//                    String[] _alertUserPassword = (String[]) m.get("_alertUserPassword");
//                    String[] _blockCommonWords = (String[]) m.get("_blockCommonWords");
//                    String[] _allowAlert = (String[]) m.get("_allowAlert");
//                    String[] _gatewayType = (String[]) m.get("_gatewayType");
//                    String[] _alertAttempt = (String[]) m.get("_alertAttempt");
//                    String[] _templateName = (String[]) m.get("_templateName");
//                    String[] _arrCommonPassword = (String[]) m.get("_arrCommonPassword");
//                    String[] _allowAlertFor = (String[]) m.get("_allowAlertFor");
//                    String[] _userValidityDays = (String[]) m.get("_userValidityDays");
//
////                if (operatorType == REQUESTER) {
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = EDIT_PASSWORD_POLICY_SETTING;
//                    approval.itemid = "Edit password policy Setting";
//                    approval.makerid = operatorId;
//                    approval.passwordLength = _passwordLength[0];
//                    approval.passwordExpiryTime = _passwordExpiryTime[0];
//                    approval.passwordIssueLimit = _passwordIssueLimit[0];
//                    approval.enforceDormancyTime = _enforceDormancyTime[0];
//                    approval.passwordInvalidAttempts = _passwordInvalidAttempts[0];
//                    approval.oldPasswordReuseTime = _oldPasswordReuseTime[0];
//                    approval.changePasswordAfterLogin = _changePasswordAfterLogin[0];
//                    approval.epinResetReissue = _epinResetReissue[0];
//                    approval.crResetReissue = _crResetReissue[0];
//                    approval.passwordType = _passwordType[0];
//                    approval.allowRepetationCharacter = _allowRepetationCharacter[0];
//                    approval.genearationPassword = _genearationPassword[0];
//                    approval.alertUserPassword = _alertUserPassword[0];
//                    approval.blockCommonWords = _blockCommonWords[0];
//                    approval.allowAlert = _allowAlert[0];
//                    approval.gatewayType = _gatewayType[0];
//                    approval.alertAttempt = _alertAttempt[0];
//                    approval.templateName = _templateName[0];
//                    approval.arrCommonPassword = _arrCommonPassword[0];
//                    approval.allowAlertFor = _allowAlertFor[0];
//                    approval.userValidityDays = _userValidityDays[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
            // }
        }
        if (url.contains(navSettings.getProperty("passwordpolicy.removepasswordpolicy"))) {
            if (accessObj.removePasswordPolicySettings == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        //Channel Profile settings
        if (url.contains(navSettings.getProperty("channelprofile.listchannelprofile"))) {
            if (accessObj.listChannelProfileSettings == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("channelprofile.addchannelprofile"))) {

            if (accessObj.editChannelProfileSettings == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _checkuser = (String[]) m.get("_checkuser");
//                    String[] _alertuser = (String[]) m.get("_alertuser");
//                    String[] _edituser = (String[]) m.get("_edituser");
//                    String[] _deleteuser = (String[]) m.get("_deleteuser");
//                    String[] _tokenload = (String[]) m.get("_tokenload");
//                    String[] _alertmedia = (String[]) m.get("_alertmedia");
//                    String[] _softwaretype = (String[]) m.get("_softwaretype");
//                    String[] _cleanuppath = (String[]) m.get("_cleanuppath");
//                    String[] _rssarchive = (String[]) m.get("_rssarchive");
//                    String[] _uploads = (String[]) m.get("_uploads");
//                    String[] _authorizer = (String[]) m.get("_authorizer");
//                    String[] _Authorizer_PeningDuration = (String[]) m.get("_Authorizer_PeningDuration");
//                    String[] _resetuser = (String[]) m.get("_resetuser");
//                    String[] _connectorstatus = (String[]) m.get("_connectorstatus");
//                    String[] _cleanupdays = (String[]) m.get("_cleanupdays");
//                    String[] _otpspecification = (String[]) m.get("_otpspecification");
//                    String[] _certspecification = (String[]) m.get("_certspecification");
//                    String[] _ocraspecification = (String[]) m.get("_ocraspecification");
//                    String[] _locationclassName = (String[]) m.get("_locationclassName");
//                    String[] _Authorizer_Units = (String[]) m.get("_Authorizer_Units");
//                    String[] _multipleSession = (String[]) m.get("_multipleSession");
//                    String[] _serverRestriction = (String[]) m.get("_serverRestriction");
//                    String[] _dayrestriction = (String[]) m.get("_dayrestriction");
//                    String[] _timerange = (String[]) m.get("_timerange");
//                    String[] _timefromampm = (String[]) m.get("_timefromampm");
//                    String[] _totimerange = (String[]) m.get("_totimerange");
//                    String[] _timetoampm = (String[]) m.get("_timetoampm");
//                    String[] _sweetSpotDeviation = (String[]) m.get("_sweetSpotDeviation");
//                    String[] _xDeviation = (String[]) m.get("_xDeviation");
//                    String[] _yDeviation = (String[]) m.get("_yDeviation");
////                if (operatorType == REQUESTER) {
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = ADD_CHANNLE_PROFILE_SETTING;
//                    approval.itemid = "Add channel profile Setting";
//                    approval.makerid = operatorId;
//                    approval.checkuser = _checkuser[0];
//                    approval.alertuser = _alertuser[0];
//                    approval.edituser = _edituser[0];
//                    approval.deleteuser = _deleteuser[0];
//                    approval.tokenload = _tokenload[0];
//                    approval.alertmedia = _alertmedia[0];
//                    approval.softwaretype = _softwaretype[0];
//                    approval.cleanuppath = _cleanuppath[0];
//                    approval.rssarchive = _rssarchive[0];
//                    approval.uploads = _uploads[0];
//                    approval.authorizer = _authorizer[0];
//                    approval.Authorizer_PeningDuration = _Authorizer_PeningDuration[0];
//                    approval.resetuser = _resetuser[0];
//                    approval.connectorstatus = _connectorstatus[0];
//                    approval.cleanupdays = _cleanupdays[0];
//                    approval.otpspecification = _otpspecification[0];
//                    approval.certspecification = _certspecification[0];
//                    approval.ocraspecification = _ocraspecification[0];
//                    approval.locationclassName = _locationclassName[0];
//                    approval.Authorizer_Units = _Authorizer_Units[0];
//                    approval.multipleSession = _multipleSession[0];
//                    approval.serverRestriction = _serverRestriction[0];
//                    approval.dayrestriction = _dayrestriction[0];
//                    approval.timerange = _timerange[0];
//                    approval.timefromampm = _timefromampm[0];
//                    approval.totimerange = _totimerange[0];
//                    approval.timetoampm = _timetoampm[0];
//                    approval.sweetSpotDeviation = _sweetSpotDeviation[0];
//                    approval.xDeviation = _xDeviation[0];
//                    approval.yDeviation = _yDeviation[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("channelprofile.editchannelprofile"))) {

            if (accessObj.editChannelProfileSettings == true) {
                // if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _checkuser = (String[]) m.get("_checkuser");
//                    String[] _alertuser = (String[]) m.get("_alertuser");
//                    String[] _edituser = (String[]) m.get("_edituser");
//                    String[] _deleteuser = (String[]) m.get("_deleteuser");
//                    String[] _tokenload = (String[]) m.get("_tokenload");
//                    String[] _alertmedia = (String[]) m.get("_alertmedia");
//                    String[] _softwaretype = (String[]) m.get("_softwaretype");
//                    String[] _cleanuppath = (String[]) m.get("_cleanuppath");
//                    String[] _rssarchive = (String[]) m.get("_rssarchive");
//                    String[] _uploads = (String[]) m.get("_uploads");
//                    String[] _authorizer = (String[]) m.get("_authorizer");
//                    String[] _Authorizer_PeningDuration = (String[]) m.get("_Authorizer_PeningDuration");
//                    String[] _resetuser = (String[]) m.get("_resetuser");
//                    String[] _connectorstatus = (String[]) m.get("_connectorstatus");
//                    String[] _cleanupdays = (String[]) m.get("_cleanupdays");
//                    String[] _otpspecification = (String[]) m.get("_otpspecification");
//                    String[] _certspecification = (String[]) m.get("_certspecification");
//                    String[] _ocraspecification = (String[]) m.get("_ocraspecification");
//                    String[] _locationclassName = (String[]) m.get("_locationclassName");
//                    String[] _Authorizer_Units = (String[]) m.get("_Authorizer_Units");
//                    String[] _multipleSession = (String[]) m.get("_multipleSession");
//                    String[] _serverRestriction = (String[]) m.get("_serverRestriction");
//                    String[] _dayrestriction = (String[]) m.get("_dayrestriction");
//                    String[] _timerange = (String[]) m.get("_timerange");
//                    String[] _timefromampm = (String[]) m.get("_timefromampm");
//                    String[] _totimerange = (String[]) m.get("_totimerange");
//                    String[] _timetoampm = (String[]) m.get("_timetoampm");
//                    String[] _sweetSpotDeviation = (String[]) m.get("_sweetSpotDeviation");
//                    String[] _xDeviation = (String[]) m.get("_xDeviation");
//                    String[] _yDeviation = (String[]) m.get("_yDeviation");
////                if (operatorType == REQUESTER) {
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = EDIT_CHANNLE_PROFILE_SETTING;
//                    approval.itemid = "Edit Channel Profile Setting";
//                    approval.makerid = operatorId;
//                    approval.checkuser = _checkuser[0];
//                    approval.alertuser = _alertuser[0];
//                    approval.edituser = _edituser[0];
//                    approval.deleteuser = _deleteuser[0];
//                    approval.tokenload = _tokenload[0];
//                    approval.alertmedia = _alertmedia[0];
//                    approval.softwaretype = _softwaretype[0];
//                    approval.cleanuppath = _cleanuppath[0];
//                    approval.rssarchive = _rssarchive[0];
//                    approval.uploads = _uploads[0];
//                    approval.authorizer = _authorizer[0];
//                    approval.Authorizer_PeningDuration = _Authorizer_PeningDuration[0];
//                    approval.resetuser = _resetuser[0];
//                    approval.connectorstatus = _connectorstatus[0];
//                    approval.cleanupdays = _cleanupdays[0];
//                    approval.otpspecification = _otpspecification[0];
//                    approval.certspecification = _certspecification[0];
//                    approval.ocraspecification = _ocraspecification[0];
//                    approval.locationclassName = _locationclassName[0];
//                    approval.Authorizer_Units = _Authorizer_Units[0];
//                    approval.multipleSession = _multipleSession[0];
//                    approval.serverRestriction = _serverRestriction[0];
//                    approval.dayrestriction = _dayrestriction[0];
//                    approval.timerange = _timerange[0];
//                    approval.timefromampm = _timefromampm[0];
//                    approval.totimerange = _totimerange[0];
//                    approval.timetoampm = _timetoampm[0];
//                    approval.sweetSpotDeviation = _sweetSpotDeviation[0];
//                    approval.xDeviation = _xDeviation[0];
//                    approval.yDeviation = _yDeviation[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("channelprofile.removechannelprofile"))) {
            if (accessObj.removeChannelProfileSettings == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        //otp token settings
        if (url.contains(navSettings.getProperty("tokensetting.listtokensettings"))) {
            if (accessObj.listOtpTokensSettings == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("tokensetting.addtokensettings"))) {

            if (accessObj.editOtpTokensSettings == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _OTPLength = (String[]) m.get("_OTPLength");
//                    String[] _SOTPLength = (String[]) m.get("_SOTPLength");
//                    String[] _OATHAlgoType = (String[]) m.get("_OATHAlgoType");
//                    String[] _SWOTPLength = (String[]) m.get("_SWOTPLength");
//                    String[] _SWOATHAlgoType = (String[]) m.get("_SWOATHAlgoType");
//                    String[] _SWWebTokenExpiryTime = (String[]) m.get("_SWWebTokenExpiryTime");
//                    String[] _OTPAttempts = (String[]) m.get("_OTPAttempts");
//                    String[] _ValidationSteps = (String[]) m.get("_ValidationSteps");
//                    String[] _duration = (String[]) m.get("_duration");
//                    String[] _MultipleToken = (String[]) m.get("_MultipleToken");
//                    String[] _SWWebTokenPinAttempt = (String[]) m.get("_SWWebTokenPinAttempt");
//                    String[] _EnforceMasking = (String[]) m.get("_EnforceMasking");
//                    String[] _autoUnlockAfter = (String[]) m.get("_autoUnlockAfter");
//                    String[] _RegistrationExpiryTime = (String[]) m.get("_RegistrationExpiryTime");
//                    String[] _allowAlertT = (String[]) m.get("_allowAlertT");
//                    String[] _gatewayType = (String[]) m.get("_gatewayType");
//                    String[] _templateNameT = (String[]) m.get("_templateNameT");
//                    String[] _otpExpiryAfterSession = (String[]) m.get("_otpExpiryAfterSession");
//                    String[] _otpLockedAfter = (String[]) m.get("_otpLockedAfter");
//
////                if (operatorType == REQUESTER) {
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = ADD_TOKEN_SETTING;
//                    approval.itemid = "Add Token Setting";
//                    approval.makerid = operatorId;
//                    approval.OTPLength = _OTPLength[0];
//                    approval.SOTPLength = _SOTPLength[0];
//                    approval.OATHAlgoType = _OATHAlgoType[0];
//                    approval.SWOTPLength = _SWOTPLength[0];
//                    approval.SWOTPLength = _SWOTPLength[0];
//                    approval.SWWebTokenExpiryTime = _SWWebTokenExpiryTime[0];
//                    approval.OTPAttempts = _OTPAttempts[0];
//                    approval.ValidationSteps = _ValidationSteps[0];
//                    approval.duration = _duration[0];
//                    approval.MultipleToken = _MultipleToken[0];
//                    approval.SWWebTokenPinAttempt = _SWWebTokenPinAttempt[0];
//                    approval.EnforceMasking = _EnforceMasking[0];
//                    approval.autoUnlockAfter = _autoUnlockAfter[0];
//                    approval.RegistrationExpiryTime = _RegistrationExpiryTime[0];
//                    approval.allowAlertT = _allowAlertT[0];
//                    approval.gatewayType = _gatewayType[0];
//                    approval.templateNameT = _templateNameT[0];
//                    approval.otpExpiryAfterSession = _otpExpiryAfterSession[0];
//                    approval.otpLockedAfter = _otpLockedAfter[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
//                    } else {
//                        aStatus.iStatus = error;
//                        aStatus.strStatus = strError;
//                        return aStatus;
//                    }
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
            }
        }
        if (url.contains(navSettings.getProperty("tokensetting.edittokensettings"))) {

            if (accessObj.editOtpTokensSettings == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _OTPLength = (String[]) m.get("_OTPLength");
//                    String[] _SOTPLength = (String[]) m.get("_SOTPLength");
//                    String[] _OATHAlgoType = (String[]) m.get("_OATHAlgoType");
//                    String[] _SWOTPLength = (String[]) m.get("_SWOTPLength");
//                    String[] _SWOATHAlgoType = (String[]) m.get("_SWOATHAlgoType");
//                    String[] _SWWebTokenExpiryTime = (String[]) m.get("_SWWebTokenExpiryTime");
//                    String[] _OTPAttempts = (String[]) m.get("_OTPAttempts");
//                    String[] _ValidationSteps = (String[]) m.get("_ValidationSteps");
//                    String[] _duration = (String[]) m.get("_duration");
//                    String[] _MultipleToken = (String[]) m.get("_MultipleToken");
//                    String[] _SWWebTokenPinAttempt = (String[]) m.get("_SWWebTokenPinAttempt");
//                    String[] _EnforceMasking = (String[]) m.get("_EnforceMasking");
//                    String[] _autoUnlockAfter = (String[]) m.get("_autoUnlockAfter");
//                    String[] _RegistrationExpiryTime = (String[]) m.get("_RegistrationExpiryTime");
//                    String[] _allowAlertT = (String[]) m.get("_allowAlertT");
//                    String[] _gatewayType = (String[]) m.get("_gatewayType");
//                    String[] _templateNameT = (String[]) m.get("_templateNameT");
//                    String[] _otpExpiryAfterSession = (String[]) m.get("_otpExpiryAfterSession");
//                    String[] _otpLockedAfter = (String[]) m.get("_otpLockedAfter");
//
////                if (operatorType == REQUESTER) {
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = ADD_TOKEN_SETTING;
//                    approval.itemid = "Add Token Setting";
//                    approval.makerid = operatorId;
//                    approval.OTPLength = _OTPLength[0];
//                    approval.SOTPLength = _SOTPLength[0];
//                    approval.OATHAlgoType = _OATHAlgoType[0];
//                    approval.SWOTPLength = _SWOTPLength[0];
//                    approval.SWOTPLength = _SWOTPLength[0];
//                    approval.SWWebTokenExpiryTime = _SWWebTokenExpiryTime[0];
//                    approval.OTPAttempts = _OTPAttempts[0];
//                    approval.ValidationSteps = _ValidationSteps[0];
//                    approval.duration = _duration[0];
//                    approval.MultipleToken = _MultipleToken[0];
//                    approval.SWWebTokenPinAttempt = _SWWebTokenPinAttempt[0];
//                    approval.EnforceMasking = _EnforceMasking[0];
//                    approval.autoUnlockAfter = _autoUnlockAfter[0];
//                    approval.RegistrationExpiryTime = _RegistrationExpiryTime[0];
//                    approval.allowAlertT = _allowAlertT[0];
//                    approval.gatewayType = _gatewayType[0];
//                    approval.templateNameT = _templateNameT[0];
//                    approval.otpExpiryAfterSession = _otpExpiryAfterSession[0];
//                    approval.otpLockedAfter = _otpLockedAfter[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("tokensetting.removetokensettings"))) {
            if (accessObj.removeOtpTokensSettings == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        //certificate settings
        if (url.contains(navSettings.getProperty("certificatesetting.listcertificatesetting"))) {
            if (accessObj.listCertificateSettings == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("certificatesetting.addcertificatesetting"))) {

            if (accessObj.editCertificateSettings == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    _className = (String[]) m.get("_className");
//                    _ip = (String[]) m.get("_ip");
//                    _password = (String[]) m.get("_password");
//                    _port = (String[]) m.get("_port");
//                    String[] _reserve1 = (String[]) m.get("_reserve1");
//                    String[] _reserve2 = (String[]) m.get("_reserve2");
//                    String[] _reserve3 = (String[]) m.get("_reserve3");
//                    _userId = (String[]) m.get("_userId");
//                    String[] _validityDays = (String[]) m.get("_validityDays");
//                    String[] _keyLength = (String[]) m.get("_keyLength");
//                    String[] _crllink = (String[]) m.get("_crllink");
//                    String[] _crlusername = (String[]) m.get("_crlusername");
//                    String[] _crlpassword = (String[]) m.get("_crlpassword");
//                    String[] ocspurl = (String[]) m.get("ocspurl");
//                    String[] _tagsList = (String[]) m.get("_tagsList");
//                    String[] _pulledtime = (String[]) m.get("_pulledtime");
//                    String[] _statuskyc = (String[]) m.get("_statuskyc");
//                    String[] _alertsource = (String[]) m.get("_alertsource");
////                if (operatorType == REQUESTER) {
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = ADD_CERTIFICATE_SETTING;
//                    approval.itemid = "Add Certificate Setting";
//                    approval.makerid = operatorId;
//                    approval.className = _className[0];
//                    approval.ip = _ip[0];
//                    approval.password = _password[0];
//                    approval.port = _port[0];
//                    approval.reserve1 = _reserve1[0];
//                    approval.reserve2 = _reserve2[0];
//                    approval.reserve3 = _reserve3[0];
//                    approval.userId = _userId[0];
//                    approval.status1 = _statuskyc[0];
//                    approval.validityDays = _validityDays[0];
//                    approval.keyLength = _keyLength[0];
//                    approval.crllink = _crllink[0];
//                    approval.crlusername = _crlusername[0];
//                    approval.crlpassword = _crlpassword[0];
//                    approval.ocspurl = ocspurl[0];
//                    approval.tagsList = _tagsList[0];
//                    approval.pulledtime = _pulledtime[0];
//                    approval.alertsource = _alertsource[0];
//
//                    Calendar pexpiredOn = Calendar.getInstance();
//
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = pending;
                aStatus.strStatus = strPending;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("certificatesetting.editcertificatesetting"))) {

            if (accessObj.editCertificateSettings == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                
//                  aStatus.iStatus = success;
//                    aStatus.strStatus = strSuccess;
//                    return aStatus;
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } //}
            //                    _className = (String[]) m.get("_className");
            //                    _ip = (String[]) m.get("_ip");
            //                    _password = (String[]) m.get("_password");
            //                    _port = (String[]) m.get("_port");
            //                    String[] _reserve1 = (String[]) m.get("_reserve1");
            //                    String[] _reserve2 = (String[]) m.get("_reserve2");
            //                    String[] _reserve3 = (String[]) m.get("_reserve3");
            //                    _userId = (String[]) m.get("_userId");
            //                    String[] _validityDays = (String[]) m.get("_validityDays");
            //                    String[] _keyLength = (String[]) m.get("_keyLength");
            //                    String[] _crllink = (String[]) m.get("_crllink");
            //                    String[] _crlusername = (String[]) m.get("_crlusername");
            //                    String[] _crlpassword = (String[]) m.get("_crlpassword");
            //                    String[] ocspurl = (String[]) m.get("ocspurl");
            //                    String[] _tagsList = (String[]) m.get("_tagsList");
            //                    String[] _pulledtime = (String[]) m.get("_pulledtime");
            //                    String[] _statuskyc = (String[]) m.get("_statuskyc");
            //                    String[] _alertsource = (String[]) m.get("_alertsource");
            ////                if (operatorType == REQUESTER) {
            //                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
            //                    Session sTemplate = suTemplate.openSession();
            //                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
            //                    ApprovalSetting approval = new ApprovalSetting();
            //                    approval.action = EDIT_CERTIFICATE_SETTING;
            //                    approval.itemid = "Edit Certificate Setting";
            //                    approval.makerid = operatorId;
            //                    approval.className = _className[0];
            //                    approval.ip = _ip[0];
            //                    approval.password = _password[0];
            //                    approval.port = _port[0];
            //                    approval.reserve1 = _reserve1[0];
            //                    approval.reserve2 = _reserve2[0];
            //                    approval.reserve3 = _reserve3[0];
            //                    approval.userId = _userId[0];
            //                    approval.status1 = _statuskyc[0];
            //                    approval.validityDays = _validityDays[0];
            //                    approval.keyLength = _keyLength[0];
            //                    approval.crllink = _crllink[0];
            //                    approval.crlusername = _crlusername[0];
            //                    approval.crlpassword = _crlpassword[0];
            //                    approval.ocspurl = ocspurl[0];
            //                    approval.tagsList = _tagsList[0];
            //                    approval.pulledtime = _pulledtime[0];
            //                    approval.alertsource = _alertsource[0];
            //
            //                    Calendar pexpiredOn = Calendar.getInstance();
            //
            //                    pexpiredOn.setTime(new Date());
            //                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
            //                    Date dexpiredOn = pexpiredOn.getTime();
            //                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
            //                    if (res == 0) {
            //                        aStatus.iStatus = pending;
            //                        aStatus.strStatus = strPending;
            //                        return aStatus;
            //                    } else {
            //                        aStatus.iStatus = error;
            //                        aStatus.strStatus = strError;
            //                        return aStatus;
            //                    }
            else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        //  }
        if (url.contains(navSettings.getProperty("certificatesetting.removecertificatesetting"))) {
            if (accessObj.removeCertificateSettings == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        //mobile trust settings
        if (url.contains(navSettings.getProperty("mobiletrustsetting.listmobiletrustsetting"))) {
            if (accessObj.listMobileTrustSettings == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("mobiletrustsetting.addmobiletrustsetting"))) {

            if (accessObj.editMobileTrustSettings == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                  aStatus.iStatus = success;
//                    aStatus.strStatus = strSuccess;
//                    return aStatus;
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _CountryName = (String[]) m.get("_CountryName");
//                    String[] _ExpiryMin = (String[]) m.get("_ExpiryMin");
//                    String[] _Backup = (String[]) m.get("_Backup");
//                    String[] _SilentCall = (String[]) m.get("_SilentCall");
//                    String[] _SelfDestructEnable = (String[]) m.get("_SelfDestructEnable");
//                    String[] _SelfDestructAttempts = (String[]) m.get("_SelfDestructAttempts");
//                    String[] _selfDestructURl = (String[]) m.get("_selfDestructURl");
//                    String[] _timeStamp = (String[]) m.get("_timeStamp");
//                    String[] _deviceTracking = (String[]) m.get("_deviceTracking");
//                    String[] _geoFencing = (String[]) m.get("_geoFencing");
//                    String[] _allowAlertM = (String[]) m.get("_allowAlertM");
//                    String[] _gatewayTypeM = (String[]) m.get("_gatewayTypeM");
//                    String[] _alertAttemptM = (String[]) m.get("_alertAttemptM");
//                    String[] _templateName = (String[]) m.get("_templateName");
//                    String[] _allowAlertForM = (String[]) m.get("_allowAlertForM");
//                    String[] _selfDestructURlM = (String[]) m.get("_selfDestructURlM");
//                    String[] _type = (String[]) m.get("_type");
//                    String[] _isBlackListed = (String[]) m.get("_isBlackListed");
//                    String[] _blackListedCountries = (String[]) m.get("_blackListedCountries");
//
////                if (operatorType == REQUESTER) {
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = ADD_MOBILE_TRUST_SETTING;
//                    approval.itemid = "Add Mobile Trust Setting";
//                    approval.makerid = operatorId;
//
//                    approval.CountryName = _CountryName[0];
//                    approval.ExpiryMin = _ExpiryMin[0];
//                    approval.Backup = _Backup[0];
//                    approval.SilentCall = _SilentCall[0];
//                    approval.SelfDestructEnable = _SelfDestructEnable[0];
//                    approval.SelfDestructAttempts = _SelfDestructAttempts[0];
//                    approval.selfDestructURl = _selfDestructURl[0];
//                    approval.timeStamp = _timeStamp[0];
//                    approval.deviceTracking = _deviceTracking[0];
//                    approval.geoFencing = _geoFencing[0];
//                    approval.allowAlertM = _allowAlertM[0];
//                    approval.gatewayTypeM = _gatewayTypeM[0];
//                    approval.alertAttemptM = _alertAttemptM[0];
//                    approval.templateName = _templateName[0];
//                    approval.allowAlertForM = _allowAlertForM[0];
//                    approval.selfDestructURlM = _selfDestructURlM[0];
//                    approval.type = _type[0];
//                    approval.isBlackListed = _isBlackListed[0];
//                    approval.blackListedCountries = _blackListedCountries[0];
//
//                    Calendar pexpiredOn = Calendar.getInstance();
//
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("mobiletrustsetting.editmobiletrustsetting"))) {

            if (accessObj.editMobileTrustSettings == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                  aStatus.iStatus = success;
//                    aStatus.strStatus = strSuccess;
//                    return aStatus;
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _CountryName = (String[]) m.get("_CountryName");
//                    String[] _ExpiryMin = (String[]) m.get("_ExpiryMin");
//                    String[] _Backup = (String[]) m.get("_Backup");
//                    String[] _SilentCall = (String[]) m.get("_SilentCall");
//                    String[] _SelfDestructEnable = (String[]) m.get("_SelfDestructEnable");
//                    String[] _SelfDestructAttempts = (String[]) m.get("_SelfDestructAttempts");
//                    String[] _selfDestructURl = (String[]) m.get("_selfDestructURl");
//                    String[] _timeStamp = (String[]) m.get("_timeStamp");
//                    String[] _deviceTracking = (String[]) m.get("_deviceTracking");
//                    String[] _geoFencing = (String[]) m.get("_geoFencing");
//                    String[] _allowAlertM = (String[]) m.get("_allowAlertM");
//                    String[] _gatewayTypeM = (String[]) m.get("_gatewayTypeM");
//                    String[] _alertAttemptM = (String[]) m.get("_alertAttemptM");
//                    String[] _templateName = (String[]) m.get("_templateName");
//                    String[] _allowAlertForM = (String[]) m.get("_allowAlertForM");
//                    String[] _selfDestructURlM = (String[]) m.get("_selfDestructURlM");
//                    String[] _type = (String[]) m.get("_type");
//                    String[] _isBlackListed = (String[]) m.get("_isBlackListed");
//                    String[] _blackListedCountries = (String[]) m.get("_blackListedCountries");
//
////                if (operatorType == REQUESTER) {
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = ADD_MOBILE_TRUST_SETTING;
//                    approval.itemid = "Add Mobile Trust Setting";
//                    approval.makerid = operatorId;
//                    approval.CountryName = _CountryName[0];
//                    approval.ExpiryMin = _ExpiryMin[0];
//                    approval.Backup = _Backup[0];
//                    approval.SilentCall = _SilentCall[0];
//                    approval.SelfDestructEnable = _SelfDestructEnable[0];
//                    approval.SelfDestructAttempts = _SelfDestructAttempts[0];
//                    approval.selfDestructURl = _selfDestructURl[0];
//                    approval.timeStamp = _timeStamp[0];
//                    approval.deviceTracking = _deviceTracking[0];
//                    approval.geoFencing = _geoFencing[0];
//                    approval.allowAlertM = _allowAlertM[0];
//                    approval.gatewayTypeM = _gatewayTypeM[0];
//                    approval.alertAttemptM = _alertAttemptM[0];
//                    approval.templateName = _templateName[0];
//                    approval.allowAlertForM = _allowAlertForM[0];
//                    approval.selfDestructURlM = _selfDestructURlM[0];
//                    approval.type = _type[0];
//                    approval.isBlackListed = _isBlackListed[0];
//                    approval.blackListedCountries = _blackListedCountries[0];
//
//                    Calendar pexpiredOn = Calendar.getInstance();
//
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("mobiletrustsetting.removemobiletrustsetting"))) {
            if (accessObj.removeMobileTrustSettings == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        //radius server settings
        if (url.contains(navSettings.getProperty("radiussetting.listradiussetting"))) {
            if (accessObj.listRadiusConfigSettings == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("radiussetting.addradiussetting"))) {

            if (accessObj.editRadiusConfigSettings == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _displayname = (String[]) m.get("_displayname");
//                    String[] authenticationtype = (String[]) m.get("authenticationtype");
//                    String[] _rClientIP = (String[]) m.get("_rClientIP");
//                    String[] _radiusClientSecretkey = (String[]) m.get("_radiusClientSecretkey");
//                    String[] _dayrestriction = (String[]) m.get("_dayrestriction");
//                    String[] _timerange = (String[]) m.get("_timerange");
//                    String[] _timefromampm = (String[]) m.get("_timefromampm");
//                    String[] _totimerange = (String[]) m.get("_totimerange");
//                    String[] _timetoampm = (String[]) m.get("_timetoampm");
//
////                if (operatorType == REQUESTER) {
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = ADD_RADIUS_SETTING;
//                    approval.itemid = "Add Radius Setting";
//                    approval.makerid = operatorId;
//                    approval.displayname = _displayname[0];
//                    approval.authenticationtype = authenticationtype[0];
//                    approval.rClientIP = _rClientIP[0];
//                    approval.radiusClientSecretkey = _radiusClientSecretkey[0];
//                    approval.dayrestriction = _dayrestriction[0];
//                    approval.timerange = _timerange[0];
//                    approval.timefromampm = _timefromampm[0];
//                    approval.totimerange = _totimerange[0];
//                    approval.timetoampm = _timetoampm[0];
//
//                    Calendar pexpiredOn = Calendar.getInstance();
//
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }

        if (url.contains(navSettings.getProperty("radiussetting.editradiussetting1"))) {
            if (accessObj.editRadiusConfigSettings == true) {

                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;

            }
        }

        if (url.contains(navSettings.getProperty("radiussetting.changeClientStatus"))) {
            if (accessObj.editRadiusConfigSettings == true) {

                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;

            }
        }

        if (url.contains(navSettings.getProperty("radiussetting.editradiussetting"))) {
            if (accessObj.editRadiusConfigSettings == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }
//                  aStatus.iStatus = success;
//                    aStatus.strStatus = strSuccess;
//                    return aStatus;
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _displayname = (String[]) m.get("_displayname");
//                    String[] authenticationtype = (String[]) m.get("authenticationtype");
//                    String[] _rClientIP = (String[]) m.get("_rClientIP");
//                    String[] _radiusClientSecretkey = (String[]) m.get("_radiusClientSecretkey");
//                    String[] _dayrestriction = (String[]) m.get("_dayrestriction");
//                    String[] _timerange = (String[]) m.get("_timerange");
//                    String[] _timefromampm = (String[]) m.get("_timefromampm");
//                    String[] _totimerange = (String[]) m.get("_totimerange");
//                    String[] _timetoampm = (String[]) m.get("_timetoampm");
//
////                if (operatorType == REQUESTER) {
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = EDIT_RADIUS_SETTING;
//                    approval.itemid = "EDIT Radius Setting";
//                    approval.makerid = operatorId;
//                    approval.displayname = _displayname[0];
//                    approval.authenticationtype = authenticationtype[0];
//                    approval.rClientIP = _rClientIP[0];
//                    approval.radiusClientSecretkey = _radiusClientSecretkey[0];
//                    approval.dayrestriction = _dayrestriction[0];
//                    approval.timerange = _timerange[0];
//                    approval.timefromampm = _timefromampm[0];
//                    approval.totimerange = _totimerange[0];
//                    approval.timetoampm = _timetoampm[0];
//
//                    Calendar pexpiredOn = Calendar.getInstance();
//
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("radiussetting.removeradiussetting"))) {
            if (accessObj.removeRadiusConfigSettings == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        //global settings
        if (url.contains(navSettings.getProperty("globalsetting.listglobalsetting"))) {
            if (accessObj.listGlobalSettings == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("globalsetting.addglobalsetting"))) {
            if (accessObj.editGlobalSettings == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("globalsetting.editglobalsetting"))) {
            if (accessObj.editGlobalSettings == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("globalsetting.removeglobalsetting"))) {
            if (accessObj.removeGlobalSettings == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        //epin settings
        if (url.contains(navSettings.getProperty("epinsetting.listepinsetting"))) {
            if (accessObj.listEpinConfigSettings == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("epinsetting.addeditepinsettings"))) {

            if (accessObj.editEpinConfigSettings == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] pinDeliveryType = (String[]) m.get("_pinDeliveryType");
//                    String[] channelType = (String[]) m.get("_channelType");
//                    String[] channelType2 = (String[]) m.get("_channelType2");
//                    String[] duration = (String[]) m.get("_splitduration");
//                    String[] dayRestriction = (String[]) m.get("_dayRestriction");
//                    String[] timeFromInHour = (String[]) m.get("_timeFromRestriction");
//                    String[] timeToInHour = (String[]) m.get("_timeToRestriction");
//                    String[] pinRequestCountDuration = (String[]) m.get("_pinRequestCountDuration");
//                    String[] pinRequestCount = (String[]) m.get("_pinRequestCount");
//                    String[] operatorController = (String[]) m.get("_operatorController");
//                    String[] expiryTime = (String[]) m.get("_expiryTime");
//                    String[] isAlertOperatorOnFailure = (String[]) m.get("_isAlertOperatorOnFailure");
//                    String[] smstext = (String[]) m.get("_smstext");
//                    String[] PINSource = (String[]) m.get("_PINSource");
//                    String[] PinSourceClassNameclassname = (String[]) m.get("_PinSourceClassNameclassname");
//                    String[] ChallengeResponse = (String[]) m.get("_ChallengeResponse");
//                    String[] ChallengeResponseclassname = (String[]) m.get("_ChallengeResponseclassname");
//                    String[] smsurlStatus = (String[]) m.get("_smsurlStatus");
//                    String[] smsurl = (String[]) m.get("_smsurl");
//                    String[] voiceurlStatus = (String[]) m.get("_voiceurlStatus");
//                    String[] voiceurl = (String[]) m.get("_voiceurl");
//                    String[] ussdurlStatus = (String[]) m.get("_ussdurlStatus");
//                    String[] ussdurl = (String[]) m.get("_ussdurl");
//                    String[] weburlStatus = (String[]) m.get("_weburlStatus");
//                    String[] webeurl = (String[]) m.get("_weburl");
//                    String[] _displayname = (String[]) m.get("_displayname");
//
////                if (operatorType == REQUESTER) {
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = ADD_EPIN_SETTING;
//                    approval.itemid = "Add Epin Setting";
//                    approval.makerid = operatorId;
//                    approval.displayname = _displayname[0];
//                    approval.pinDeliveryType = pinDeliveryType[0];
//                    approval.channelType = channelType[0];
//                    approval.channelType2 = channelType2[0];
//                    approval.dayRestriction = dayRestriction[0];
//                    approval.timeFromInHour = timeFromInHour[0];
//                    approval.timeToInHour = timeToInHour[0];
//                    approval.pinRequestCountDuration = pinRequestCountDuration[0];
//                    approval.pinRequestCount = pinRequestCount[0];
//                    approval.operatorController = operatorController[0];
//                    approval.expiryTime = expiryTime[0];
//                    approval.isAlertOperatorOnFailure = isAlertOperatorOnFailure[0];
//                    approval.smstext = smstext[0];
//                    approval.PINSource = PINSource[0];
//                    approval.PinSourceClassNameclassname = PinSourceClassNameclassname[0];
//                    approval.ChallengeResponse = ChallengeResponse[0];
//                    approval.ChallengeResponseclassname = ChallengeResponseclassname[0];
//                    approval.smsurlStatus = smsurlStatus[0];
//                    approval.smsurl = smsurl[0];
//                    approval.voiceurlStatus = voiceurlStatus[0];
//                    approval.voiceurl = voiceurl[0];
//                    approval.ussdurlStatus = ussdurlStatus[0];
//                    approval.ussdurl = ussdurl[0];
//                    approval.weburlStatus = weburlStatus[0];
//                    approval.webeurl = webeurl[0];
//                    approval.duration = duration[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

//                }
//            } else {
//                aStatus.iStatus = error;
//                aStatus.strStatus = strError;
//                return aStatus;
//            }
        }
        if (url.contains(navSettings.getProperty("system.changeoprpassword"))) {
            if (accessObj.editEpinConfigSettings == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _oldPassword = (String[]) m.get("_oldPassword");
//                    String[] _newPassword = (String[]) m.get("_newPassword");
//                    String[] _confirmPassword = (String[]) m.get("_confirmPassword");
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = SYSTEM_CHANGEPASSWORD;
//                    approval.itemid = "Change Password";
//                    approval.makerid = operatorId;
//                    approval.oldvalue = _oldPassword[0];
//                    approval.password = _newPassword[0];
//                    approval.newValue = _confirmPassword[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
//                    }
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
            }
        }
        if (url.contains(navSettings.getProperty("system.addscheduler"))) {
            if (accessObj.addReportSchedulerManagement == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _gatewayType = (String[]) m.get("_gatewayType");
//                    String[] _schedulerStatus = (String[]) m.get("_schedulerStatus");
//                    String[] _gatewayPreference = (String[]) m.get("_gatewayPreference");
//                    String[] _gatewayStatus = (String[]) m.get("_gatewayStatus");
//                    String[] _operatorRoles = (String[]) m.get("_operatorRoles");
//                    String[] _schedulerDuration = (String[]) m.get("_schedulerDuration");
//                    String[] _schedulername = (String[]) m.get("_schedulername");
//                    String[] _email = (String[]) m.get("_tagsList");
//                    String[] _reportType = (String[]) m.get("_reportType");
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = SYSTEM_ADD_SCHEDULER;
//                    approval.itemid = "Add Report Scheduler";
//                    approval.makerid = operatorId;
//                    approval.type = _gatewayType[0];
//                    approval.status = _schedulerStatus[0];
//                    approval.status1 = _gatewayStatus[0];
//                    approval.prefernce = Integer.parseInt(_gatewayPreference[0]);
//                    approval.operator_role = _operatorRoles[0];
//                    approval.duration = _schedulerDuration[0];
//                    approval.name = _schedulername[0];
//                    approval.email = _email[0];
//                    approval.reportType = _reportType[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("system.changeschedularstatus"))) {
            if (accessObj.editReportSchedulerManagement == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _schedulerStatus = (String[]) m.get("_status");
//                    String[] _schedulername = (String[]) m.get("_schedulerName");
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = SYSTEM_STATUS_SCHEDULER;
//                    approval.itemid = "Schedular Status";
//                    approval.makerid = operatorId;
//                    approval.status = _schedulerStatus[0];
//                    approval.name = _schedulername[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("system.editscheduler"))) {
            if (accessObj.editReportSchedulerManagement == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _gatewayType = (String[]) m.get("_gatewayType");
//                    String[] _gatewayPreference = (String[]) m.get("_gatewayPreference");
//                    String[] _gatewayStatus = (String[]) m.get("_gatewayStatus");
//                    String[] _operatorRoles = (String[]) m.get("_operatorRoles");
//                    String[] _schedulerDuration = (String[]) m.get("_schedulerDuration");
//                    String[] _schedulername = (String[]) m.get("_schedulername");
//                    String[] _email = (String[]) m.get("_EditSchedulerContact0");
//                    String[] _reportType = (String[]) m.get("_reportType");
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = SYSTEM_EDIT_SCHEDULER;
//                    approval.itemid = "Schedular Status";
//                    approval.makerid = operatorId;
//                    approval.type = _gatewayType[0];
//                    approval.status1 = _gatewayStatus[0];
//                    approval.prefernce = Integer.parseInt(_gatewayPreference[0]);
//                    approval.operator_role = _operatorRoles[0];
//                    approval.duration = _schedulerDuration[0];
//                    approval.name = _schedulername[0];
//                    approval.email = _email[0];
//                    approval.reportType = _reportType[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("system.removescheduler"))) {
            if (accessObj.removeReportSchedulerManagement == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _schedulername = (String[]) m.get("_schedulerName");
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = SYSTEM_DELETE_SCHEDULER;
//                    approval.itemid = "Schedular Delete";
//                    approval.makerid = operatorId;
//                    approval.name = _schedulername[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("system.auditintegrity"))) {
            if (accessObj.removeReportSchedulerManagement == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _IntegrityID = (String[]) m.get("_IntegrityID");
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = SYSTEM_AUDIT_INTEGRITY;
//                    approval.itemid = "Audit Integrity";
//                    approval.makerid = operatorId;
//                    approval.template_id = _IntegrityID[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("epinsetting.editeditepinsettings"))) {
            if (accessObj.editEpinConfigSettings == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("epinsetting.removeeditepinsettings"))) {
            if (accessObj.removeEpinConfigSettings == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        //  //two way auth
        if (url.contains(navSettings.getProperty("twowayauth.list"))) {
            if (accessObj.listTwoWayAuthManagement == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("twowayauth.status"))) {
            if (accessObj.editTwoWayAuthManagement == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                  aStatus.iStatus = success;
//                    aStatus.strStatus = strSuccess;
//                    return aStatus;
//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] type = (String[]) m.get("type");
//                    String[] status = (String[]) m.get("status");
//                    String[] userid = (String[]) m.get("userid");
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = TWO_WAY_AUTH_STATUS;
//                    approval.itemid = "Two way Auth status";
//                    approval.makerid = operatorId;
//                    approval.type = type[0];
//                    approval.status1 = status[0];
//                    approval.userId = userid[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("twowayauth.sendregcode"))) {
            if (accessObj.editTwoWayAuthManagement == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] userid = (String[]) m.get("userid");
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = TWO_WAY_AUTH_REGCODE;
//                    approval.itemid = "Two way Auth Send Regcode";
//                    approval.makerid = operatorId;
//                    approval.userId = userid[0];
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }

        //web resource
        if (url.contains(navSettings.getProperty("webresource.listwebResource"))) {
            if (accessObj.listWebResource == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("webresource.addwebResource"))) {
            if (accessObj.addWebResource == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _resource_name = (String[]) m.get("_Name");
//                    String[] _weburl = (String[]) m.get("_URL");
//                    String[] saveto = (String[]) m.get("_savepath");
//                    String[] _status = (String[]) m.get("_Status");
//                    String[] _group = (String[]) m.get("_groupName");
//                    String _resource_nameS = null;
//                    String _weburlS = null;
//                    String savetoS = null;
//                    String _statusS = null;
//                    String _groupS = null;
//                    if (_resource_name != null) {
//                        _resource_nameS = _resource_name[0];
//                    }
//                    if (_weburl != null) {
//                        _weburlS = _weburl[0];
//                    }
//                    if (saveto != null) {
//                        savetoS = saveto[0];
//                    }
//                    if (_status != null) {
//                        _statusS = _status[0];
//                    }
//                    if (_group != null) {
//                        _groupS = _group[0];
//                    }
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = WEBRESOURCE_ADD;
//                    approval.itemid = "Add Web Resource";
//                    approval.makerid = operatorId;
//                    approval.name = _resource_nameS;
//                    approval.webeurl = _weburlS;
//                    approval.xmlfilepath = savetoS;
//                    approval.status = _statusS;
//                    approval.user_groupId = _groupS;
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }
        if (url.contains(navSettings.getProperty("webresource.changestatus"))) {
            if (accessObj.addWebResource == true) {
//                if (operatorType == REQUESTER) {
                if (authStatus == MAKERCHECKER_DISABLE) {
                    aStatus.iStatus = success;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                }

//                    String[] _fromApprove = (String[]) m.get("_fromApprove");
//                    if (_fromApprove != null) {
//                        if (_fromApprove[0].equals("yes")) {
//                            aStatus.iStatus = success;
//                            aStatus.strStatus = strSuccess;
//                            return aStatus;
//                        }
//                    }
//                    String[] _resS = (String[]) m.get("_resourceid");
//                    String[] _statusS = (String[]) m.get("_status");
//
//                    SessionFactoryUtil suTemplate = new SessionFactoryUtil(SessionFactoryUtil.approvalsettings);
//                    Session sTemplate = suTemplate.openSession();
//                    AuthorizationUtils oUtil = new AuthorizationUtils(suTemplate, sTemplate);
//                    ApprovalSetting approval = new ApprovalSetting();
//                    approval.action = WEBRESOURCE_CHANGESTATUS;
//                    approval.itemid = "Web Resource Change status";
//                    approval.makerid = operatorId;
//                    if (_resS != null) {
//                        approval.name = _resS[0];
//                    }
//                    if (_statusS != null) {
//                        approval.status = _statusS[0];
//                    }
//                    Calendar pexpiredOn = Calendar.getInstance();
//                    pexpiredOn.setTime(new Date());
//                    pexpiredOn.add(Calendar.DAY_OF_YEAR, reqExpiryDuration);
//                    Date dexpiredOn = pexpiredOn.getTime();
//                    int res = oUtil.addSetting(config.getChannelId(), operatorId, AUTORIZATION_PENDING_STATUS, dexpiredOn, approval);
//                    if (res == 0) {
                aStatus.iStatus = success;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

//                } else {
//                    aStatus.iStatus = error;
//                    aStatus.strStatus = strError;
//                    return aStatus;
//                }
//            }
        }

        //user report
        if (url.contains(navSettings.getProperty("userreport.list"))) {
            if (accessObj.listUserReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("userreport.view"))) {
            String[] _requestID = (String[]) m.get("_requestID");
            if (_requestID[0].equals("userreport")) {
                if (accessObj.viewUserReport == true) {
                    aStatus.iStatus = 0;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            } else if (_requestID[0].equals("userreportDownload")) {
                if (accessObj.downloadUserReport == true) {
                    aStatus.iStatus = 0;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }
        }
        //msgreport report
        if (url.contains(navSettings.getProperty("msgreport.list"))) {
            if (accessObj.listMessageReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("msgreport.view"))) {
            String[] _requestID = (String[]) m.get("_requestID");
            if (_requestID[0].equals("msgreport")) {
                if (accessObj.viewMessageReport == true) {
                    aStatus.iStatus = 0;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            } else if (_requestID[0].equals("msgreportdownload")) {
                if (accessObj.downloadMessageReport == true) {
                    aStatus.iStatus = 0;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }
        }
        //honey trap report
        if (url.contains(navSettings.getProperty("honeytrapreport.list"))) {
            if (accessObj.listMessageReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("honeytrapreport.view"))) {
            String[] _requestID = (String[]) m.get("_requestID");
            if (_requestID[0].equals("honeytrapreport")) {
                if (accessObj.viewHoneyTrapReport == true) {
                    aStatus.iStatus = 0;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            } else if (_requestID[0].equals("honeyTrapDownload")) {
                if (accessObj.downloadHoneyTrapReport == true) {
                    aStatus.iStatus = 0;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }
        }

        //otp report
        if (url.contains(navSettings.getProperty("otpreport.list"))) {
            if (accessObj.listOtpReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("otpreport.view"))) {
            String[] _requestID = (String[]) m.get("_requestID");
            if (_requestID[0].equals("otpreport")) {
                if (accessObj.viewOtpReport == true) {
                    aStatus.iStatus = 0;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            } else if (_requestID[0].equals("otpreportdownload")) {
                if (accessObj.downloadOtpReport == true) {
                    aStatus.iStatus = 0;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

        }
        //bill subscription report
        if (url.contains(navSettings.getProperty("billreport.list"))) {
            if (accessObj.listsubscriptionBaseBillingReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("billreport.view"))) {
            String[] _requestID = (String[]) m.get("_requestID");
            if (_requestID[0].equals("billreportsubscription")) {
                if (accessObj.viewsubscriptionBaseBillingReport == true) {
                    aStatus.iStatus = 0;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            } else if (_requestID[0].equals("subbillreportdownload")) {
                if (accessObj.downloadsubscriptionBaseBillingReport == true) {
                    aStatus.iStatus = 0;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

        }
        //bill transaction report
        if (url.contains(navSettings.getProperty("billreportTx.list"))) {
            if (accessObj.listsubscriptionBaseBillingReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("billreportTx.view"))) {
            String[] _requestID = (String[]) m.get("_requestID");
            if (_requestID[0].equals("billreporttx")) {
                if (accessObj.viewtranscationBaseBillingReport == true) {
                    aStatus.iStatus = 0;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            } else if (_requestID[0].equals("txbillreportdownload")) {
                if (accessObj.downloadtranscationBaseBillingReport == true) {
                    aStatus.iStatus = 0;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }

        }
        if (url.contains(navSettings.getProperty("otpfailurereport.list"))) {
            if (accessObj.listOtpFailureReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("otpfailurereport.view"))) {
            String[] _requestID = (String[]) m.get("_requestID");
            if (_requestID[0].equals("otpfailurereport")) {
                if (accessObj.viewOtpFailureReport == true) {
                    aStatus.iStatus = 0;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            } else if (_requestID[0].equals("otpfailurereportdownload")) {
                if (accessObj.downloadOtpFailureReport == true) {
                    aStatus.iStatus = 0;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }
        }

        //Pki report
        if (url.contains(navSettings.getProperty("pkireport.list"))) {
            if (accessObj.listPkiReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("pkireport.view"))) {
            String[] _requestID = (String[]) m.get("_requestID");
            if (_requestID[0].equals("pkireport")) {
                if (accessObj.viewPkiReport == true) {
                    aStatus.iStatus = 0;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            } else if (_requestID[0].equals("pkireportdownload")) {
                if (accessObj.downloadPkiReport == true) {
                    aStatus.iStatus = 0;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }
        }
        //cert report
        //Pki report
        if (url.contains(navSettings.getProperty("pkireport.list"))) {
            if (accessObj.listPkiReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("certReport.list"))) {
            String[] _requestID = (String[]) m.get("_requestID");
            if (_requestID[0].equals("certreport")) {
                if (accessObj.viewCertificateReport == true) {
                    aStatus.iStatus = 0;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            } else if (_requestID[0].equals("certreportdownload")) {
                if (accessObj.downloadCertificateReport == true) {
                    aStatus.iStatus = 0;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }
        }

        //new change
        if (url.contains("certreport")) {

            if (accessObj.downloadCertificateReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        if (url.contains("editusersrcsettings")) {
            if (accessObj.editUserSourceSettings == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        //epin report
        if (url.contains(navSettings.getProperty("epinreport.list"))) {

            if (accessObj.listEpinSystemReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        //new change
        if (url.contains("DownloadCAAudit")) {

            if (accessObj.downloadUserReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        //new change
        if (url.contains("DownloadAuthByDate")) {

            if (accessObj.downloadTwoAuthRports == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        if (url.contains("msgcostreport")) {

            if (accessObj.downloadMessageReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        if (url.contains("sessionaudit")) {

            if (accessObj.downloadSessionAuditTrail == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        //msgcostreport   
        if (url.contains(navSettings.getProperty("epinreport.view"))) {
            String[] _requestID = (String[]) m.get("_requestID");
            if (_requestID[0].equals("epinreport")) {
                if (accessObj.viewEpinSystemReport == true) {
                    aStatus.iStatus = 0;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            } else if (_requestID[0].equals("epinreportdownload")) {
                if (accessObj.downloadEpinSystemReport == true) {
                    aStatus.iStatus = 0;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }
        }

        //listEpinUserSpecific report
        if (url.contains(navSettings.getProperty("epinuserreport.list"))) {
            if (accessObj.listEpinUserSpecificReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("epinuserreport.view"))) {
            if (accessObj.viewEpinUserSpecificReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("epinuserreport.download"))) {
            if (accessObj.downloadEpinUserSpecificReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        //RemoteSigning report
        if (url.contains(navSettings.getProperty("remotesigningreport.list"))) {
            if (accessObj.listRemoteSigningReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("remotesigningreport.view"))) {
            String[] _requestID = (String[]) m.get("_requestID");
            if (_requestID[0].equals("remotesigningreport")) {
                if (accessObj.viewRemoteSigningReport == true) {
                    aStatus.iStatus = 0;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            } else if (_requestID[0].equals("remotesigningreportdownload")) {
                if (accessObj.downloadRemoteSigningReport == true) {
                    aStatus.iStatus = 0;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }
        }
        if (url.contains(navSettings.getProperty("transactionreport.view"))) {
            String[] _requestID = (String[]) m.get("_requestID");
            if (_requestID[0].equals("transactionreport")) {
                if (accessObj.viewRemoteSigningReport == true) {
                    aStatus.iStatus = 0;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            } else if (_requestID[0].equals("transactionreportdownload")) {
                if (accessObj.downloadRemoteSigningReport == true) {
                    aStatus.iStatus = 0;
                    aStatus.strStatus = strSuccess;
                    return aStatus;
                } else {
                    aStatus.iStatus = error;
                    aStatus.strStatus = strError;
                    return aStatus;
                }
            }
        }

        //user specific
        if (url.contains(navSettings.getProperty("listlicenseDetails.list"))) {
            if (accessObj.listlicenseDetails == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("listhwOTPTokenUpload.list"))) {
            if (accessObj.listhwOTPTokenUpload == true || accessObj.listhwPKITokenUpload == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
//        if (url.contains(navSettings.getProperty("listhwPKITokenUpload.list"))) {
//            if (accessObj.listhwPKITokenUpload == true) {
//                aStatus.iStatus = 0;
//                aStatus.strStatus = strSuccess;
//                return aStatus;
//            } else {
//                aStatus.iStatus = error;
//                aStatus.strStatus = strError;
//                return aStatus;
//            }
//        }
        if (url.contains(navSettings.getProperty("signPDFDOC.list"))) {
            if (accessObj.signPDFDOC == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("changeSecurityCredentials.list"))) {
            if (accessObj.changeSecurityCredentials == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("downloadAuditTrail.list"))) {
            if (accessObj.downloadAuditTrail == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("downloadSessionAuditTrail.list"))) {
            if (accessObj.downloadSessionAuditTrail == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("killSessionID.list"))) {
            if (accessObj.killSessionID == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("downloadLogs.list"))) {
            if (accessObj.downloadLogs == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("downloadCleanupLogs.list"))) {
            if (accessObj.downloadCleanupLogs == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("connectorStatus.list"))) {
            if (accessObj.connectorStatus == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("auditIntegrity.list"))) {
            if (accessObj.auditIntegrity == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("changeDBPassword.list"))) {
            if (accessObj.changeDBPassword == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("requesterArchiveRequest.list"))) {
            if (accessObj.requesterArchiveRequest == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("requesterPendingRequest.list"))) {
            if (accessObj.requesterPendingRequest == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("authorizerArchiveRequest.list"))) {
            if (accessObj.authorizerArchiveRequest == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("authorizerPendingRequest.list"))) {
            if (accessObj.authorizerPendingRequest == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("PendingListForApproval.list"))) {
            if (accessObj.authorizerPendingRequest == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains(navSettings.getProperty("requesterOperatorList.list"))) {
            if (accessObj.requesterOperatorList == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        if (url.contains("ELDonutChart")) {
            if (accessObj.viewEasyCheckinReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains("ELBarChart")) {
            if (accessObj.viewEasyCheckinReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        if (url.contains("easyloginReport.jsp")) {
            if (accessObj.viewEasyCheckinReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        if (url.contains("twowayauthreporttable.jsp")) {
            if (accessObj.viewTwoAuthRports == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        if (url.contains("easyloginReport.jsp")) {
            if (accessObj.viewEasyCheckinReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        if (url.contains("certificatereport.jsp")) {
            if (accessObj.viewCertificateReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        if (url.contains("editemessage")) {
            if (accessObj.editErrorMessages == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        if (url.contains("TxReportDownload")) {
            if (accessObj.downloadtranscationReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        if (url.contains("DownloadEasyCheckInReport") || url.contains("DownloadEasySessionReport")) {
            if (accessObj.downloadasyCheckinReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }

        }

        if (url.contains("DownloadRemoteSigningReport")) {
            if (accessObj.downloadRemoteSigningReport == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }

        if (url.contains("unitreports.jsp")) {
            if (accessObj.listUnitRports == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains("report.jsp")) {
            if (accessObj.listUnitRports == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains("report.jsp")) {
            if (accessObj.listUnitRports == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains("userrolereportdown")) {
            if (accessObj.viewRoleRports == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        if (url.contains("unitrolereportdown")) {
            if (accessObj.viewUnitRports == true) {
                aStatus.iStatus = 0;
                aStatus.strStatus = strSuccess;
                return aStatus;
            } else {
                aStatus.iStatus = error;
                aStatus.strStatus = strError;
                return aStatus;
            }
        }
        return null;

    }
}
